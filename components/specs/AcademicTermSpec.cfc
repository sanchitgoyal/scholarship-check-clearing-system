<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods
	
		1. IllegalTermCodeException
		2. IllegalTermSemesterException
		3. IllegalAcademicYearException
		4. IllegalTermBeginDateException
		5. IllegalTermEndDateException

---------------------------------------------------------------------------------------------------->

<cfcomponent displayname="AcademicTermSpec" bindingname="AcademicTermSpec" output="no" hint="Validates Academic Term">
	<!--- Term Code Validator --->
	<cffunction name="isValidTermCode" access="public" returntype="boolean" output="no" hint="Validates Term Code" description="Validates Term Code">
		<cfargument name="termCode" required="yes" hint="Academic Term Code" />
	    <!-------------------- Test Cases --------------------
			1. Term Code is a Non Numeric Argument 
			2. Term Code is a Non Positive Numeric Argument 
		------------------------------------------------------>   
		<!--- Case 1 & Case 2 --->      
		<cfif (NOT IsNumeric(arguments.termCode)) or (NOT arguments.termCode gt 0)> 
	    	<!--- Throw Specific Exception --->
	    	<cfthrow type="IllegalTermCodeException" message="Bad Formatting for Term Code" 
	        		detail="Term Code Should be a 4 Digit Positive Number. Eg: 0750" extendedinfo="#arguments.termCode#" />
		<cfelse>
	    	<!--- Default Return True --->
			<cfreturn true />    	
	    </cfif>    	
	</cffunction>
    
	<!--- Semester Validator --->
	<cffunction name="isValidSemester" access="public" returntype="boolean" output="no" hint="Validates Academic Term Semester" description="Validates Academic Term Semester">
        <cfargument name="semester" type="string" required="yes" hint="fall/summer/spring" />
        <!---------------- Test Cases ------------------
			1. Semester is not in {spring,summer,fall}
		------------------------------------------------> 
        <!--- Case 1 --->
        <cfif arguments.semester neq 'spring' and arguments.semester neq 'fall' and arguments.semester neq 'summer'>
			<!--- Throw Specific Exception --->
            <cfthrow type="IllegalTermSemesterException" message="Unknown Value for Term Semester" 
                detail="Term semester needs to be either one of - spring, fall, summer" extendedinfo="#arguments.semester#"/>
		<cfelse>
	    	<!--- Default Return True --->
			<cfreturn true />    	
	    </cfif>    	
	</cffunction>
    
	<!--- Academic Year Validator --->
	<cffunction name="isValidAcademicYear" access="public" returntype="boolean" output="no" hint="Validates Academic Year" description="Validates Academic Year">
        <cfargument name="academicYear" type="string" required="yes" />       
    	<!------------- Test Cases --------------
			1. Academic Year is a Blank String 
		---------------------------------------->
        <!--- Case 1 --->
		<cfif Trim(arguments.academicYear) eq ''>
        	<!--- Throw Specific Exception --->
        	<cfthrow type="IllegalAcademicYearException" message="No Value Set For Academic Year"
            		detail="Term Academic Year value cannot be set as blank" extendedinfo="#arguments.academicYear#"/>
		<cfelse>
	    	<!--- Default Return True --->
			<cfreturn true />    	
	    </cfif>    	
	</cffunction>
    
    <!--- Begin Date Validator --->
	<cffunction name="isValidBeginDate" access="public" returntype="boolean" output="no" hint="Validate Academic Term Begin Date" description="Validates Academic Term's Begin Date">
        <cfargument name="beginDate" required="yes" />       
    	<!------------- Test Cases --------------
			1. Begin Date is not a valid date 
		---------------------------------------->
        <!--- Case 1 --->
		<cfif NOT IsDate(arguments.beginDate)>
        	<!--- Throw Specific Exception --->
        	<cfthrow type="IllegalTermBeginDateException" message="Invalid Begin Date"
            		detail="Begin date for an academic term has to be a valid calendar date" extendedinfo="#arguments.beginDate#"/>
		<cfelse>
	    	<!--- Default Return True --->
			<cfreturn true />    	
	    </cfif>    	
	</cffunction>
    
    <!--- End Date Validator --->
	<cffunction name="isValidEndDate" access="public" returntype="boolean" output="no" hint="Validate Academic Term End Date" description="Validates Academic Term's End Date">
        <cfargument name="endDate" required="yes" />       
    	<!------------- Test Cases --------------
			1. End Date is not a valid date 
		---------------------------------------->
        <!--- Case 1 --->
		<cfif NOT IsDate(arguments.endDate)>
        	<!--- Throw Specific Exception --->
        	<cfthrow type="IllegalTermEndDateException" message="Invalid End Date"
            		detail="End date for an academic term has to be a valid calendar date" extendedinfo="#arguments.endDate#"/>
		<cfelse>
	    	<!--- Default Return True --->
			<cfreturn true />    	
	    </cfif>    	
	</cffunction>
    
</cfcomponent>