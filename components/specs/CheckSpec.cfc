<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalDepositNumberFormatException
		2. IllegalUserIDFormatException
		3. IllegalItemTypeCodeException
		4. IllegalTagTextFormatException
		5. IllegalCheckNumberFormatException
		6. IllegalCheckTotalFormatException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent bindingname="CheckSpec" displayname="CheckSpec" hint="Validates Check" output="no" >
	<!--- Deposit Number Validator --->
    <cffunction name="isValidDepositNumber" access="public" returntype="boolean" output="no" hint="Validates Deposit Number" description="Validates Deposit Number">
		<cfargument name="depositNumber" required="yes" hint="Check Deposit Number" />        
		<!-------------------------------------- Test Cases -------------------------------------
			1. Deposit Number Blank is Okay 
			2. Deposit Number is something other than a blank and is Not Formatted SFAXXX...
		----------------------------------------------------------------------------------------->
                
        <!--- Handle Case 1 & 2. --->
        <cfif (NOT isSimpleValue(arguments.depositNumber)) or (Trim(arguments.depositNumber) neq '' and Trim(REReplace(arguments.depositNumber,'SFA\d+','')) neq '')>
        	<!--- Throw Exception --->
			<cfthrow type="IllegalDepositNumberFormatException" message="Illegal Deposit Number Format" 
            		detail="Deposit Number needs to be in SFAXXX... Format" extendedinfo="#depositNumber#" />
		</cfif>
        
        <!--- Default Return True --->
        <cfreturn true />
	</cffunction>

	<!--- Associated User ID Specification --->
	<cffunction name="isValidUser" access="public" returntype="boolean" output="no" hint="Validates User ID" description="Validates User ID">
		<cfargument name="userID" type="numeric" required="yes" hint="Gears User ID" />
		<!-------- Test Cases -----------
			1. Is not valid Gears User 
		---------------------------------->
		<!--- Create Validator Object for Gears --->
		<cfset var externalValidator = createObject("component","gears.studentfinancialaid.specs.ExternalSpec") />
		<!--- Validate Gears User ID --->
		<cftry>
			<!--- Cover Case 1 --->
			<cfreturn externalValidator.isValidGearsUserID(arguments.userID) />
			<!--- Catch & Process Exceptions --->
			<cfcatch type="IllegalGearsUserIDFormatException">
	        	<cfthrow type="IllegalUserIDFormatException" message="Incorrect Format for User ID" 
	            		detail="User ID is supposed to be a positive non zero integer" />
			</cfcatch>			
		</cftry>		
		<!--- Default Return False --->	
		<cfreturn false />
	</cffunction>

	<!--- Validates Check Creation Date --->
	<cffunction name="isValidCreationDate" access="public" returntype="boolean" output="no" hint="Validates Creation Date" description="Validates Creation Date">
		<cfargument name="createdOn" type="Date" required="true" hint="Check Creation Date" />
		<!----- Test Cases -------
			1. None 
		------------------------->	
		<cfreturn true />			
	</cffunction>

	<!--- Validates Check Last Modification Date --->
	<cffunction name="isValidLastModificationDate" access="public" returntype="boolean" output="no"  hint="Validates Modification Date" description="Validates Modification Date">
		<cfargument name="lastModifiedOn" type="Date" required="true" hint="Check Last Modification Date" />
		<!----- Test Cases -------
			1. None 
		------------------------->	
		<cfreturn true />			
	</cffunction>	
    
	<!--- Validates Check Deposit Date --->
	<cffunction name="isValidDepositDate" access="public" returntype="boolean" output="no" hint="Validates Deposit Date" description="Validates Deposit Date">
		<cfargument name="depositedOn" type="Date" required="true" hint="Check Deposit Date" />
		<!----- Test Cases -------
			1. None 
		------------------------->	
		<cfreturn true />			
	</cffunction>	
    
	<!--- Validates Check Receipt Date --->
	<cffunction name="isValidReceiptDate" access="public" returntype="boolean" output="no" hint="Validates Receipt Date" description="Validates Receipt Date">
		<cfargument name="receivedOn" type="Date" required="true" hint="Check Receipt Date" />
		<!----- Test Cases -------
			1. None 
		------------------------->	
		<cfreturn true />			
	</cffunction>	

	<!--- Validate Check Item Type --->
	<cffunction name="isValidItemTypeCode" access="public" returntype="boolean" output="no" hint="Validates Check Item Type" description="Validates Check Item Type">
		<cfargument name="itemTypeCode" type="numeric" required="true" hint="Check Item Type" />
		<!-------------- Test Cases --------------------
			1. Item Type Code doesn't match spec  
		------------------------------------------------>	
		
		<!--- Instantiate Item Type Spec Object --->
		<cfset var itemTypeValidator = createObject("component","gears.studentfinancialaid.specs.ItemTypeSpec") />
		
		<!--- Cover Case 1 : Throws IllegalItemTypeCodeException --->
        <cfreturn itemTypeValidator.isValidItemTypeCode(arguments.itemTypeCode) />
        
	</cffunction>

	<!--- Validate Tag List --->
	<cffunction name="isValidTagList" access="public" returntype="boolean" output="false" hint="Validates Check Tag List" description="Validates Check Tag List">
		<cfargument name="tagList" type="string" required="true" hint="Tag List" />
		<!--------------------- Test Cases ----------------------
			1. Any Tag in the list is not in proper format 		
		-------------------------------------------------------->
        
		<!--- Instantiate Local Variables --->
		<cfset var listPassedValidation = true />

		<!--- Instantiate Check Tag Spec Object --->
		<cfset var checkTagValidator = createObject("component","gears.studentfinancialaid.specs.CheckTagSpec") />							
		<!--- Loop through Tags in List --->
		<cfset tagArray = listToArray(tagList,",") />
        <cfloop index="i" from="1" to="#arrayLen(tagArray)#">
			<!--- Validate Tag: Throws IllegalTagTextFormatException --->
            <cfset listPassedValidation = listPassedValidation and checkTagValidator.isValidTagText(tagArray[i]) />
        </cfloop>		

		<!--- Return Validation Result --->
		<cfreturn listPassedValidation />
	</cffunction>

	<!--- Validate Check Number --->
	<cffunction name="isValidCheckNumber" access="public" returntype="boolean" output="false" hint="Validates Check Number" description="Validates Check Number">					
		<cfargument name="checkNumber" type="numeric" required="true" hint="Check Number" />				
		<!--------------------- Test Cases -------------------------
			1. Check Number is Non-Integer 									
			2. Check Number is Non-Positive Integer 			
		------------------------------------------------------------>	
        
		<!--- Handle Test Cases 1 & 2 --->
        <cfif NOT (Int(arguments.checkNumber) eq arguments.checkNumber and arguments.checkNumber gt 0)>
        	<cfthrow type="IllegalCheckNumberFormatException" message="Illegal Check Number Format Exception" 
            		detail="Check Number Should be a Non-Zero, Positive Integer" extendedinfo="#arguments.checkNumber#" />
		</cfif>        	
						
		<!--- Default to True --->						
		<cfreturn true />
	</cffunction>

	<!--- Validate Check Total --->
	<cffunction name="isValidCheckTotal" access="public" returntype="boolean" output="false" hint="Validates Check Total" description="Validates Check Total">					
		<cfargument name="checkTotal" type="numeric" required="true" hint="Check Total" />				
		<!--------------------- Test Cases -------------------------
			1. Check Total is not a number 
			2. Check Total Non-Positive 			
		------------------------------------------------------------>	
        
		<!--- Handle Test Cases 1 & 2 --->
        <cfif  NOT (IsNumeric(arguments.checkTotal) and arguments.checkTotal gt 0)>
        	<cfthrow type="IllegalCheckTotalFormatException" message="Illegal Check Total Format Exception" 
            		detail="Check Total Should be a Non-Zero, Positive Number" extendedinfo="#arguments.checkTotal#" />
		</cfif>        	
						
		<!--- Default to True --->						
		<cfreturn true />
	</cffunction>

	<!--- Validate Disbursements JSON --->
	<cffunction name="isValidDisbursementsJSON" access="public" returntype="boolean" output="false" hint="Validates Disbursements JSON" description="Validates Disbursements JSON">					
		<cfargument name="disbursementsJSON" type="string" required="true" hint="Check Disbursements" />				
		<!--------------------- Test Cases -------------------------
			1. Disbursements JSON is empty string 
			2. Disbursements JSON is not JSON format
			3. JSON contains no disbursements
		------------------------------------------------------------>	

		<!--- Handle Case 1 --->
		<cfif Trim(arguments.disbursementsJSON) eq "">
        	<cfthrow type="EmptyDisbursementJSONException" message="No Disbursement associated with Check" 
            		detail="A check needs to be related with atleast one disbursement." extendedinfo="#disbursementsJSON#" /> 
        </cfif>
        
        <!--- Handle Case 2 --->
        <cfif NOT isJSON(arguments.disbursementsJSON)>
			<cfthrow type="IllegalDisbursementJSONFormatException" message="Disbursement JSON not in format" 
            		detail="Disbursements JSON was not found to be a valid JSON String" extendedinfo="#disbursementsJSON#" />
        </cfif>        
        
        <!--- Handle Case 3 --->
        <cfif ArrayLen(DeserializeJSON(arguments.disbursementsJSON)) eq 0>
        	<cfthrow type="EmptyDisbursementJSONException" message="No Disbursement associated with Check" 
            		detail="A check needs to be related with atleast one disbursement." extendedinfo="#disbursementsJSON#" /> 
        </cfif>

		<!--- Default to True --->						
		<cfreturn true />
	</cffunction>

	<!--- Validate Comments JSON --->
	<cffunction name="isValidCommentsJSON" access="public" returntype="boolean" output="false" hint="Validates Comments JSON" description="Validates Comments JSON">					
		<cfargument name="commentsJSON" type="string" required="true" hint="Check Comments" />				
		<!--------------------- Test Cases -------------------------
			1. Comments JSON is empty string 
			2. Comments is not JSON format
		------------------------------------------------------------>	

		<!--- Handle Case 1 --->
		<cfif Trim(arguments.commentsJSON) eq "">
        	<cfreturn true />
        </cfif>
        
        <!--- Handle Case 2 --->
        <cfif NOT isJSON(arguments.commentsJSON)>
			<cfthrow type="IllegalCommentsJSONFormatException" message="Comments JSON not in format" 
            		detail="Comments JSON was not found to be a valid JSON String" extendedinfo="#arguments.commentsJSON#" />
        </cfif>        

		<!--- Default to True --->						
		<cfreturn true />
	</cffunction>
</cfcomponent> 


<!--------------  Workshop -----------------

	<!--- Validate Creator --->
	<cffunction name="isValidCreator" access="public" returntype="boolean" output="no" hint="Validates Check Creator" description="Validates Check Creator">
		<cfargument name="userID" required="yes" hint="Gears User ID" />
		<!-------- Test Cases -----------
			1. Is not valid Gears User 
		---------------------------------->
        
		<!--- Instantiate External Factors Validator --->
		<cfset var externalValidator = createObject("component","gears.studentfinancialaid.specs.ExternalSpec") />
		<cftry>
			<!--- Cover Case 1 --->
			<cfreturn externalValidator.isValidGearsUserID(arguments.userID) />
            
			<!--- Catch & Process Exceptions --->
            
            <!--- Illegal Gears User --->
			<cfcatch type="IllegalGearsUserIDFormatException">
	        	<cfthrow type="IllegalUserIDFormatException" message="Incorrect Format for User ID" detail="User ID is supposed to be a positive non zero integer" />
			</cfcatch>			
		</cftry>		
		<!--- Default Return False --->	
		<cfreturn false />
	</cffunction>
    
	<!--- Validate Modifier --->
	<cffunction name="isValidModifier" access="public" returntype="boolean" output="no" hint="Validates Check Modifier" description="Validates Check Modifier">
		<cfargument name="userID" required="yes" hint="Gears User ID" />
		<!-------- Test Cases -----------
			1. Is not valid Gears User 
		---------------------------------->
        
		<!--- Instantiate External Factors Validator --->
		<cfset var externalValidator = createObject("component","gears.studentfinancialaid.specs.ExternalSpec") />
		<cftry>
			<!--- Cover Case 1 --->
			<cfreturn externalValidator.isValidGearsUserID(arguments.userID) />
            
			<!--- Catch & Process Exceptions --->
            
            <!--- Illegal Gears User --->
			<cfcatch type="IllegalGearsUserIDFormatException">
	        	<cfthrow type="IllegalUserIDFormatException" message="Incorrect Format for User ID" detail="User ID is supposed to be a positive non zero integer" />
			</cfcatch>			
		</cftry>		
		<!--- Default Return False --->	
		<cfreturn false />
	</cffunction>
	
--------------------------------------------------------->	