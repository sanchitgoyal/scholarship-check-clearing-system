<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalGearsUserIDFormatException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent bindingname="ExternalSpec" displayname="ExternalSpec" hint="Validates External Factors" output="no">
	<!--- Gears User ID Specification --->
	<cffunction name="isValidGearsUserID"  access="public" returntype="boolean" output="no" 
    			hint="Validates for Gears User ID Format" description="Validates for Gears User ID Format">
		<cfargument name="userID" type="numeric" required="yes" hint="Gears User ID" />
    	<!------------- Test Cases ---------------
			1. User ID is Not An integer
			2. User ID is Non Positive Integer 
		------------------------------------------>			
        <cfif NOT Int(arguments.userID) eq arguments.userID or NOT arguments.userID gt 0>
        	<cfthrow type="IllegalGearsUserIDFormatException" message="Incorrect Format for Gears User ID" 
            		detail="Gears User ID is supposed to be a positive non zero integer" />
        </cfif> 
        
        <!--- Default True --->
        <cfreturn true />
	</cffunction>   
</cfcomponent>