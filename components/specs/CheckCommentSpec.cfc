<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalUserIDFormatException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent bindingname="CheckCommentSpec" displayname="CheckCommentSpec" hint="Check Comment Specification" output="no">
	<!--- User ID Specification --->
	<cffunction name="isValidUserID"  access="public" returntype="boolean" output="no" hint="Validates Commenting User's ID" description="Validates Commenting User's ID">
		<cfargument name="userID" type="numeric" required="yes" hint="Gears User ID" />
    	<!----------------- Test Cases -------------------
			1. User ID is Not in Gears User ID Format
		------------------------------------------------->	
        		
		<!--- Instantiate External Factors Validator Object --->
        <cfset externalSpecValidator = CreateObject("component","gears.studentfinancialaid.specs.ExternalSpec") />
                    
        <!--- Validate Gears' User ID Format : Throws IllegalGearsUserIDFormatException--->
        <cfreturn externalSpecValidator.isValidGearsUserID(arguments.userID) />
        
	</cffunction>   
    
    <!--- Comment Text Specification --->
    <cffunction name="isValidText" access="public" returntype="boolean" output="no" hint="Validates Comment Text" description="Validates Comment Text">
		<cfargument name="text" type="string" required="yes" hint="Comment Text" />
        <!--- Test Cases ---
			1. None 
		-------------------->
        
        <!--- Default True --->			        
        <cfreturn true />
	</cffunction>     
    
    <!--- Comment Date/Time Specification --->
    <cffunction name="isValidDate" access="public" returntype="boolean" output="no" hint="Validates Comment Date/Time" description="Validates Comment Date/Time">
		<cfargument name="date" type="date" required="yes" hint="Comment Date/Time" />
        <!--- Test Cases ---
			1. None 
		-------------------->
        
        <!--- Default True --->			        
        <cfreturn true />
	</cffunction>         
            
</cfcomponent>