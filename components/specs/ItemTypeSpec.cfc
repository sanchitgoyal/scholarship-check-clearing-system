<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalItemTypeCodeException
		2. IllegalItemTypeDescriptionException
		3. IllegalItemTypeFundException
		4. IllegalItemTypeDepartmentException
		5. IllegalItemTypeAccountException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent bindingname="ItemTypeSpec" displayname="ItemTypeSpec" hint="Validates Item Type" output="no">
	<!--- Item Type Code Validator --->
	<cffunction name="isValidItemTypeCode"  access="public" returntype="boolean" output="no" hint="Validates Item Type Code" description="Validates Item Type Code">
		<cfargument name="itemTypeCode" required="yes" hint="Item Type Code" />
        <!-------------------------------- Test Cases -----------------------------
			1. itemTypeCode is Non-Integer 									
			2. itemTypeCode is Non-Positive Integer 			
			3. itemTypeCode is not 12 digits  
		--------------------------------------------------------------------------->
                
        <!--- Handle Test Cases 1, 2 & 3 --->
        <cfif NOT (IsNumeric(arguments.itemTypeCode) and (Int(arguments.itemTypeCode) eq arguments.itemTypeCode) and (arguments.itemTypeCode gt 0) and (Len(Trim(arguments.itemTypeCode)) eq 12))>
        	<cfthrow type="IllegalItemTypeCodeException" message="Illegal Item Type Code" 
            		detail="Item Type Code should be A 12 digit Non-Zero, Non-Negative Integer" extendedinfo="#arguments.itemTypeCode#" />
		</cfif>        	

        <!--- Default True --->
        <cfreturn true />
	</cffunction>   
    
    <!--- Item Type Description Validator --->
    <cffunction name="isValidItemTypeDescription" access="public" returntype="boolean" output="no" hint="Validates Item Type Description" description="Validates Item Type Description">
    	<cfargument name="itemTypeDescription" required="yes" hint="Item Type Description" />
        <!------------------ Test Cases -------------------
			1. itemTypeDescription is an empty string 
		-------------------------------------------------->	
        
        <!--- Cover Case 1 --->
        <cfif (NOT IsSimpleValue(arguments.itemTypeDescription)) or (Trim(arguments.itemTypeDescription) eq '')>
        	<cfthrow type="IllegalItemTypeDescriptionException" message="Illegal Item Type Description" 
            		detail="Item Type Description Cannot be Blank" extendedinfo="#arguments.itemTypeDescription#" />
        </cfif>
        
        <!--- Default True --->
        <cfreturn true />
    </cffunction>        
    
    <!--- Item Type Fund Validator --->
    <cffunction name="isValidItemTypeFund" access="public" returntype="boolean" output="no" hint="Validates Item Type Fund" description="Validates Item Type Fund">
    	<cfargument name="itemTypeFund" required="yes" hint="Item Type Fund" />
		<!-------------------------------------- Test Cases -------------------------------
			1. itemTypeFund is Not a Non-Negative, Non Zero Integer --> Throw Exception
		----------------------------------------------------------------------------------->
        
		<!--- Cover Case 1 --->
		<cfif NOT (IsNumeric(arguments.itemTypeFund) and (Int(arguments.itemTypeFund) eq arguments.itemTypeFund) and (arguments.itemTypeFund gt 0))>
        	<cfthrow type="IllegalItemTypeFundException" message="Illegal Item Type Fund" 
            		detail="Item Type Fund should be A Non-Zero, Non-Negative Integer Value" extendedinfo="#arguments.itemTypeFund#" />
		</cfif>        	
        
        <!--- Default True --->
        <cfreturn true />
    </cffunction>  
    
    <!--- Item Type Department Validator --->
    <cffunction name="isValidItemTypeDepartment" access="public" returntype="boolean" output="no" hint="Validates Item Type Department" description="Validates Item Type Department">
    	<cfargument name="itemTypeDepartment" required="yes" hint="Item Type Department" />
		<!---------------------------------------- Test Cases ---------------------------------
			1. itemTypeDepartment is Not a Non-Negative, Non Zero Integer --> Throw Exception
		--------------------------------------------------------------------------------------->
        <cfif NOT (IsNumeric(arguments.itemTypeDepartment) and (Int(arguments.itemTypeDepartment) eq arguments.itemTypeDepartment) and (arguments.itemTypeDepartment gt 0))>
        	<cfthrow type="IllegalItemTypeDepartmentException" message="Illegal Item Type Department" 
            		detail="Item Type Department should be A Non-Zero, Non-Negative Integer Value" extendedinfo="#arguments.itemTypeDepartment#" />
		</cfif>        	
        <!--- Default True --->
        <cfreturn true />
    </cffunction>              
    
    <!--- Item Type Account Validator --->
    <cffunction name="isValidItemTypeAccount" access="public" returntype="boolean" output="no" hint="Validates Item Type Account" description="Validates Item Type Account">
    	<cfargument name="itemTypeAccount" required="yes" hint="Item Type Account" />
		<!---------------------------------------- Test Cases ---------------------------------
			1. itemTypeAccount is Not a Non-Negative, Non Zero Integer --> Throw Exception
		--------------------------------------------------------------------------------------->
        <cfif NOT (IsNumeric(arguments.itemTypeAccount) and (Int(arguments.itemTypeAccount) eq arguments.itemTypeAccount) and (arguments.itemTypeAccount gt 0))>
        	<cfthrow type="IllegalItemTypeAccountException" message="Illegal Item Type Account" 
            		detail="Item Type Account should be A Non-Zero, Non-Negative Integer Value" extendedinfo="#arguments.itemTypeAccount#" />
		</cfif>        	
        <!--- Default True --->
        <cfreturn true />
    </cffunction>              
</cfcomponent>