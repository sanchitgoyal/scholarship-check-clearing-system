<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalTagTextException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent bindingname="CheckTagSpec" displayname="CheckTagSpec" output="no" hint="Validates Check Tag">
	<!--- Tag Text Validator --->
	<cffunction name="isValidTagText"  access="public" returntype="boolean" output="no" hint="Validates Check Tag Text" description="Validates Check Tag Text">
		<cfargument name="tagText" type="string" required="yes" hint="Check Tag Text" />
		<!----------------------- Test Cases ---------------------------
			1. Tag text is Empty 
			2. Tag text contains characters other than [a-z][A-Z][0-9]
		--------------------------------------------------------------->

        <!--- Tag Text is Empty --->
        <cfif Trim(arguments.tagText) eq ''>
        	<cfthrow type="IllegalTagTextException" message="Tag Text is Blank" 
            		detail="Tag Text Needs To be a [A-Z][a-z][0-9] Character Formatted Word, Space is Allowed" extendedinfo="#arguments.tagText#" />        	        
        </cfif>
        
        <!--- Tag Text Contains Characters Other Than [A-Z][a-z][0-9] --->
        <cfif REReplace(arguments.tagText, '[A-Za-z0-9\s]*','') neq ''>
			<cfthrow type="IllegalTagTextException" message="Illegal Characters in Tag Text" 
            	detail="Tag Text Needs To be a [A-Z][a-z][0-9] Character Formatted Word, Space is Allowed" extendedinfo="#arguments.tagText#" />        	        
        </cfif>
        
        <!--- Default True --->
        <cfreturn true />
	</cffunction>   
</cfcomponent>
