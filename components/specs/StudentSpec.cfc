<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods
	
	1. IllegalStudentRecordIDFormatException
	2. IllegalEmplIDFormatException
	3. IllegalStudentFirstNameFormatException
	4. IllegalStudentMiddleNameFormatException
	5. IllegalStudentLastNameFormatException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent bindingname="StudentSpec" displayname="StudentSpec" hint="Student Record Validator" output="no" >
	<!--- Student Record ID Validator --->
    <cffunction name="isValidStudentID" access="public" returntype="boolean" hint="Validates Student Record ID" description="Validates Student Record ID">
    	<cfargument name="studentID" required="yes" hint="Student Record ID" />
        <!------ Test Cases --------
			1. Should Be Numeric 
		---------------------------->	
        <cfif NOT IsNumeric(arguments.studentID)>
            <cfthrow type="IllegalStudentRecordIDFormatException" message="Illegal Student Record ID Format"
                    detail="Student Record ID should be a Positive Integer e.g. 123" extendedinfo="#arguments.emplID#" />        
        </cfif>
        <!--- Default Return True --->
        <cfreturn true />
    </cffunction>
    
	<!--- Empl ID Validator --->
    <cffunction name="isValidEmplID" access="public" returntype="boolean" hint="Validates UND Empl ID" description="Validates UND Empl ID">
    	<cfargument name="emplID" required="yes" hint="UND Empl ID" />
        <!----------------------- Test Cases ---------------------------
			1. If EmplID is not Empty it should be Numeric 
			2. If EmplID is not Empty it should be a Positive number 
		---------------------------------------------------------------->	 
        
        <cfif NOT IsSimpleValue(arguments.emplID)>
			<!--- Throw Exception --->
            <cfthrow type="IllegalEmplIDFormatException" message="Illegal Student Empl ID Format"
                    detail="Student Empl ID needs to be a Positive Integer e.g. 012345" extendedinfo="#arguments.emplID#" />        
        <cfelseif Len(Trim(arguments.emplID)) neq 0>
			<!--- Cover Cases 1 & 2 --->
			<cfif NOT IsNumeric(arguments.emplID) or arguments.emplID lte 0>
                <!--- Throw Exception --->
                <cfthrow type="IllegalEmplIDFormatException" message="Illegal Student Empl ID Format"
                        detail="Student Empl ID needs to be a Positive Integer e.g. 012345" extendedinfo="#arguments.emplID#" />
            </cfif>
		</cfif>            
        <!--- Default Return True --->
        <cfreturn true />
    </cffunction>
    
    <!--- First Name Validator --->
	<cffunction name="isValidFirstName" access="public" returntype="boolean" hint="Validates Student's First Name" description="Validates Student's First Name">
    	<cfargument name="firstName" required="yes" hint="Student's First Name" />
        <!---------- Test Cases ------------
			1. Is an Empty String 
		----------------------------------->
        
        <!--- Cover Case 1 --->
        <cfif (NOT IsSimpleValue(arguments.firstName)) or Len(Trim(arguments.firstName)) eq 0>
        	<!--- Throw Exception --->
        	<cfthrow type="IllegalStudentFirstNameFormatException" message="Illegal Student First Name Format"
            		detail="Student's First name Cannot be Blank" extendedinfo="#arguments.firstName#" />
        </cfif>
        <!--- Default Return True --->
        <cfreturn true />
    </cffunction>
    
    <!--- Middle Name Validator --->
	<cffunction name="isValidMiddleName" access="public" returntype="boolean" hint="Validates Student's Middle Name" description="Validates Student's Middle Name">
    	<cfargument name="middleName" required="yes" hint="Student's Middle Name" />
        <!----------------------- Test Cases -----------------------
			1. Should be just a string value 
		------------------------------------------------------------>
        
		<!--- Cover Case 1 --->
		<cfif NOT IsSimpleValue(arguments.middleName)>
        	<!--- Throw Exception --->
        	<cfthrow type="IllegalStudentMiddleNameFormatException" message="Illegal Student Middle Name Format"
            		detail="Student's Middle Name should be a text value" extendedinfo="#arguments.middleName#" />
        </cfif>  	
        <!--- Default Return True --->		
        <cfreturn true />
    </cffunction>
    
    <!--- Last Name Validator --->
	<cffunction name="isValidLastName" access="public" returntype="boolean" hint="Validates Student's Last Name" description="Validates Student's Last Name">
    	<cfargument name="lastName" required="yes" hint="Student's Last Name" />
        <!---------- Test Cases ------------
			1. Is an Empty String 
		----------------------------------->

        <!--- Cover Case 1 --->
        <cfif (NOT IsSimpleValue(arguments.lastName)) or (Len(Trim(arguments.lastName)) eq 0)>
        	<!--- Throw Exception --->
        	<cfthrow type="IllegalStudentLastNameFormatException" message="Illegal Student Last Name Format"
            		detail="Student's Last Name Cannot be Blank" extendedinfo="#arguments.lastName#" />
        </cfif>
        <!--- Default Return True --->
        <cfreturn true />
    </cffunction>
</cfcomponent>
