<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalDepositNumberFormatException
		2. IllegalUserIDFormatException
		3. IllegalItemTypeCodeFormatException
		4. IllegalTagTextFormatException
		5. IllegalCheckNumberFormatException
		6. IllegalCheckTotalFormatException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent bindingname="Check" displayname="Check" hint="CFC To Represent Academic Term" output="no" >
	<cffunction name="init" access="public" returntype="Check" hint="CFC Initializer" description="CFC Initializer" output="no">
    	<cfargument name="depositNumber" type="string" default="" required="no" hint="Check Record Deposit Number" />
        <cfargument name="createdBy" type="numeric" required="no" hint="Gears User ID" />
        <cfargument name="lastModifiedBy" type="numeric" required="no" hint="Gears User ID" />
        <cfargument name="createdOn" type="date" required="no" hint="Date Created" />
        <cfargument name="lastModifiedOn" type="date" required="no" hint="Date Last Modified" />        
       	<cfargument name="depositedOn" type="date" required="yes" hint="Date Check was Deposited" /> 
       	<cfargument name="receivedOn" type="date" required="yes" hint="Date Check was Received" />                
        <cfargument name="itemTypeCode" type="numeric" required="yes" hint="Check Item Type" />
        <cfargument name="tagList" type="string" default="" required="no" hint="Check Tags" />
        <cfargument name="checkNumber" type="numeric" required="yes" hint="Check Number" />
        <cfargument name="checkTotal" type="numeric" required="yes" hint="Check Total" />
        <cfargument name="disbursementsJSON" type="string" required="yes" hint="Disbursements JSON" />
        <cfargument name="commentsJSON" type="string" required="no" default="" hint="Comments JSON" />        
    
    	<cfset variables.specValidator = CreateObject("component","gears.studentfinancialaid.specs.CheckSpec") />
    
    	<cfset setDepositNumber(arguments.depositNumber) />
    	<cfif StructKeyExists(arguments,'createdBy')>
			<cfset setCreatedBy(arguments.createdBy) />
    	</cfif>
        <cfif StructKeyExists(arguments,'lastModifiedBy')>
	    	<cfset setlastModifiedBy(arguments.lastModifiedBy) />
    	</cfif>
        <cfif StructKeyExists(arguments,'createdOn')>
			<cfset setCreatedOn(arguments.createdOn) /> 
		</cfif>
        <cfif StructKeyExists(arguments,'lastModifiedOn')> 
			<cfset setLastModifiedOn(arguments.lastModifiedOn) />
		</cfif>
        
        <cfset setDepositedOn(arguments.depositedOn) />
        <cfset setReceivedOn(arguments.receivedOn) />
        
		<cfset setItemTypeCode(arguments.itemTypeCode) />        
		<cfset setTagList(arguments.tagList) />                                
        <cfset setCheckNumber(arguments.checkNumber) />                        
        <cfset setCheckTotal(arguments.checkTotal) />                                
        <cfset setDisbursementsJSON(arguments.disbursementsJSON) />        
        <cfset setCommentsJSON(arguments.commentsJSON) />      
        
        <cfreturn this />          
    </cffunction>

	<!--- Getters --->
    <cffunction name="getDepositNumber" access="public" hint="" description="" output="no" returntype="string">    	
		<cfreturn variables.depositNumber />
    </cffunction> 
    
    <cffunction name="getCreatedBy" access="public" hint="" description="" output="no" returntype="numeric">    	
		<cfreturn variables.createdBy />            
    </cffunction> 
    
    <cffunction name="getLastModifiedBy" access="public" hint="" description="" output="no" returntype="numeric">    	
		<cfreturn variables.lastModifiedBy />
    </cffunction> 
    
    <cffunction name="getCreatedOn" access="public" hint="" description="" output="no" returntype="date">   
		<cfreturn variables.createdOn />
    </cffunction> 
    
    <cffunction name="getLastModifiedOn" access="public" hint="" description="" output="no" returntype="date"> 
		<cfreturn variables.lastModifiedOn />
    </cffunction>                
    
    <cffunction name="getDepositedOn" access="public" hint="" description="" output="no" returntype="date"> 
		<cfreturn variables.depositedOn />
    </cffunction>                 
     
    <cffunction name="getReceivedOn" access="public" hint="" description="" output="no" returntype="date"> 
		<cfreturn variables.receivedOn />
    </cffunction>                 
    
    <cffunction name="getItemTypeCode" access="public" hint="" description="" output="no" returntype="numeric">   
		<cfreturn variables.itemTypeCode />
    </cffunction> 
    
    <cffunction name="getTagList" access="public" hint="" description="" output="no" returntype="string">    	
		<cfreturn variables.tagList />
    </cffunction> 
    
    <cffunction name="getCheckNumber" access="public" hint="" description="" output="no" returntype="numeric">  
		<cfreturn variables.checkNumber />
    </cffunction> 
    
    <cffunction name="getCheckTotal" access="public" hint="" description="" output="no" returntype="numeric">    	
		<cfreturn variables.checkTotal />
    </cffunction>        
    
    <cffunction name="getDisbursementsJSON" access="public" hint="" description="" output="no" returntype="string">    	
		<cfreturn variables.disbursementsJSON />
    </cffunction>                 

    <cffunction name="getCommentsJSON" access="public" hint="" description="" output="no" returntype="string">    	
		<cfreturn variables.commentsJSON />
    </cffunction>                 
    
    <cffunction name="toStruct" access="public" hint="" description="" output="no" returntype="struct">
    	<cfset var snapshotStruct = {} />
        <cfset snapShotStruct.depositNumber = getDepositNumber() /> 
        <cfset snapShotStruct.createdBy = getCreatedBy() />
        <cfset snapShotStruct.lastModifiedBy = getLastModifiedBy() />
        <cfset snapShotStruct.createdOn = getCreatedOn() />
        <cfset snapShotStruct.lastModifiedOn = getLastModifiedOn() />
        <cfset snapShotStruct.depositedOn = getDepositedOn() />
        <cfset snapShotStruct.receivedOn = getReceivedOn() />
        <cfset snapShotStruct.itemTypeCode = getItemTypeCode() />
        <cfset snapShotStruct.tagList = getTagList() />
        <cfset snapShotStruct.checkNumber = getCheckNumber() />
        <cfset snapShotStruct.checkTotal = getCheckTotal() />
        <cfset snapShotStruct.disbursementsJSON = getDisbursementsJSON() />
        <cfset snapShotStruct.commentsJSON = getCommentsJSON() />
        
        <cfreturn snapshotStruct />
    </cffunction>
       

    <!--- Setters --->
    
    <cffunction name="setDepositNumber" access="public" hint="" description="" output="no" returntype="void">    	
    	<cfargument name="depositNumber" type="string" required="yes" hint="" />
        <cfif variables.specValidator.isValidDepositNumber(arguments.depositNumber)>
        	<cfset variables.depositNumber = arguments.depositNumber />
        </cfif>
    </cffunction> 
    
    <cffunction name="setCreatedBy" access="public" hint="" description="" output="no" returntype="void">    	
    	<cfargument name="createdBy" type="numeric" required="yes" hint="" />
        <cfif variables.specValidator.isValidUser(arguments.createdBy)>
        	<cfset variables.createdBy = arguments.createdBy />
        </cfif>    
    </cffunction> 
    
    <cffunction name="setLastModifiedBy" access="public" hint="" description="" output="no" returntype="void">    	
    	<cfargument name="lastModifiedBy" type="numeric" required="yes" hint="" />
        <cfif variables.specValidator.isValidUser(arguments.lastModifiedBy)>
        	<cfset variables.lastModifiedBy = arguments.lastModifiedBy />
        </cfif>        
    </cffunction> 
    
    <cffunction name="setCreatedOn" access="public" hint="" description="" output="no" returntype="void">   
    	<cfargument name="createdOn" type="date" required="yes" hint="" />
        <cfif variables.specValidator.isValidCreationDate(arguments.createdOn)>
        	<cfset variables.createdOn = arguments.createdOn />
        </cfif>             	
    </cffunction> 
    
    <cffunction name="setLastModifiedOn" access="public" hint="" description="" output="no" returntype="void"> 
    	<cfargument name="lastModifiedOn" type="date" required="yes" hint="" />
        <cfif variables.specValidator.isValidLastModificationDate(arguments.lastModifiedOn)>
        	<cfset variables.lastModifiedOn = arguments.lastModifiedOn />
        </cfif>             	       	
    </cffunction>                 
    
    <cffunction name="setDepositedOn" access="public" hint="" description="" output="no" returntype="void"> 
    	<cfargument name="depositedOn" type="date" required="yes" hint="" />
        <cfif variables.specValidator.isValidDepositDate(arguments.depositedOn)>
        	<cfset variables.depositedOn = arguments.depositedOn />
        </cfif>             	       	
    </cffunction>                 
     
    <cffunction name="setReceivedOn" access="public" hint="" description="" output="no" returntype="void"> 
    	<cfargument name="receivedOn" type="date" required="yes" hint="" />
        <cfif variables.specValidator.isValidReceiptDate(arguments.receivedOn)>
        	<cfset variables.receivedOn = arguments.receivedOn />
        </cfif>             	       	
    </cffunction>                     
    
    <cffunction name="setItemTypeCode" access="public" hint="" description="" output="no" returntype="void">   
    	<cfargument name="itemTypeCode" type="numeric" required="yes" hint="" />
        <cfif variables.specValidator.isValidItemTypeCode(arguments.itemTypeCode)>
        	<cfset variables.itemTypeCode = arguments.itemTypeCode />
        </cfif>             	       	
    </cffunction> 
    
    <cffunction name="setTagList" access="public" hint="" description="" output="no" returntype="void">    	
    	<cfargument name="tagList" type="string" required="yes" hint="" />
        <cfif variables.specValidator.isValidTagList(arguments.tagList)>
        	<cfset variables.tagList = arguments.tagList />
        </cfif>             	       	
    </cffunction> 
    
    <cffunction name="setCheckNumber" access="public" hint="" description="" output="no" returntype="void">  
    	<cfargument name="checkNumber" type="numeric" required="yes" hint="" />
        <cfif variables.specValidator.isValidCheckNumber(arguments.checkNumber)>
        	<cfset variables.checkNumber = arguments.checkNumber />
        </cfif>             	       	
    </cffunction> 
    
    <cffunction name="setCheckTotal" access="public" hint="" description="" output="no" returntype="void">    	
    	<cfargument name="checkTotal" type="numeric" required="yes" hint="" />
        <cfif variables.specValidator.isValidCheckTotal(arguments.checkTotal)>
        	<cfset variables.checkTotal = arguments.checkTotal />
        </cfif>             	       	
    </cffunction>        
    
    <cffunction name="setDisbursementsJSON" access="public" hint="" description="" output="no" returntype="void">    	
    	<cfargument name="disbursementsJSON" type="string" required="yes" hint="" />
        <cfif variables.specValidator.isValidDisbursementsJSON(arguments.disbursementsJSON)>
        	<cfset variables.disbursementsJSON = arguments.disbursementsJSON />
        </cfif>             	       	
    </cffunction>                 

    <cffunction name="setCommentsJSON" access="public" hint="" description="" output="no" returntype="void">    	
    	<cfargument name="commentsJSON" type="string" required="yes" hint="" />
        <cfif variables.specValidator.isValidCommentsJSON(arguments.commentsJSON)>
        	<cfset variables.commentsJSON = arguments.commentsJSON />
        </cfif>             	       	
    </cffunction>                 
</cfcomponent> 