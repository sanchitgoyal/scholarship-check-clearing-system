<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods
		
		1. IllegalTermCodeException
		2. IllegalTermSemesterException
		3. IllegalAcademicYearException
		4. IllegalTermBeginDateException
		5. IllegalTermEndDateException
	
----------------------------------------------------------------------------------------------------> 

<cfcomponent displayname="AcademicTerm" bindingname="AcademicTerm" hint="CFC To Represent Academic Term" output="no">
<!--- Constructor --->
    <cffunction name="init" access="public" output="no" returntype="AcademicTerm" hint="Initializes CFC" description="Initializes CFC">
    	<cfargument name="termCode" type="string" required="yes" />
    	<cfargument name="semester" type="string" required="yes" />     
    	<cfargument name="academicYear" type="string" required="yes" />   
    	<cfargument name="beginDate" required="yes" />        
    	<cfargument name="endDate" required="yes" />        
    	<cfargument name="active" type="boolean" required="no" default="false" />  
        
        <!--- Instantiate Spec Object --->
        <cfset variables.academicTermValidator = CreateObject("component","gears.studentfinancialaid.specs.AcademicTermSpec") />
        
        <cfset setTermCode(arguments.termCode) />    
		<cfset setSemester(semester) />
		<cfset setAcademicYear(academicYear) />
        <cfset setBeginDate(beginDate) />
       	<cfset setEndDate(endDate) />
        <cfset setActive(active) />
                  
        <cfreturn this />            	
    </cffunction>
    
<!------Getters------>
    
    <!--- Term Code --->
    <cffunction name="getTermCode" access="public" output="no" returntype="string" hint="Get Academic Term Code" description="Returns Term Code of Academic Term">
    	<cfreturn #variables.termCode# />    	
    </cffunction>
    
    <!--- Semester --->
    <cffunction name="getSemester" access="public" output="no" returntype="string" hint="Get Semester" description="Returns Semester Value of Academic Term">
    	<cfreturn #variables.semester# />    	
    </cffunction>    
    
    <!--- Academic Year --->
    <cffunction name="getAcademicYear" access="public" output="no" returntype="string" hint="Get Academic Year" description="Returns Academic Year of the Term">
    	<cfreturn #variables.academicYear# />    	
    </cffunction>    
    
	<!--- Begin Date --->
    <cffunction name="getBeginDate" access="public" output="no" returntype="date" hint="Get Begin Date" description="Returns Begin Date of the Term">
    	<cfreturn #variables.beginDate# />    	
    </cffunction>    
    
    <!--- End Date --->
    <cffunction name="getEndDate" access="public" output="no" returntype="date" hint="Get End Date" description="Returns End Date of the Term">
    	<cfreturn #variables.endDate# />    	
    </cffunction>       
    
    <!--- Active Flag --->
    <cffunction name="isActive" access="public" output="no" returntype="boolean" hint="Is the Term Active? " description="Returns if the Term is Active">
    	<cfreturn #variables.active# />    	
    </cffunction>         

<!------Setters------>

	<!--- Term Code --->
    <cffunction name="setTermCode" access="public" output="no" returntype="void" hint="Set Academic Term Code" description="Sets Term Code of Academic Term">
        <cfargument name="termCode" type="string" required="yes" />
        <!--------------- Test Cases ----------------
			1. Term Code is not a valid term code 
		--------------------------------------------->        
        <cfif variables.academicTermValidator.isValidTermCode(arguments.termCode)>
        	<cfset variables.termCode = arguments.termCode />    	
        </cfif>
    </cffunction>
    
    <!--- Semester --->
    <cffunction name="setSemester" access="public" output="no" returntype="void" hint="Set Semester" description="Sets Semester Value of Academic Term">
        <cfargument name="semester" type="string" required="yes" hint="fall/summer/spring" />
        <!----------------------- Test Cases -------------------------
			1. Semester is not a valid Academic Term Semester Value
		--------------------------------------------------------------->        
        <cfif variables.academicTermValidator.isValidSemester(arguments.semester)>
        	<cfset variables.semester = arguments.semester />    	
        </cfif>
    </cffunction>    
    
    <!--- Academic Year --->
    <cffunction name="setAcademicYear" access="public" output="no" returntype="void" hint="Set Academic Year" description="Sets Academic Year of the Term">
        <cfargument name="academicYear" type="string" required="yes" />        
    	<!------------------------ Test Cases -----------------------------
			1. Academic Year argument is not a valid Academic Year value 
		------------------------------------------------------------------->
        <cfif variables.academicTermValidator.isValidAcademicYear(arguments.academicYear)>
        	<cfset variables.academicYear = arguments.academicYear />    	
        </cfif>
    </cffunction>    

	<!--- Begin Date --->
    <cffunction name="setBeginDate" access="public" output="no" returntype="void" hint="Set Begin Date" description="Sets Begin Date of the Term">        
        <cfargument name="beginDate" required="yes" />            
    	<!---------------------------- Test Cases ---------------------------------
			1. Begin Date argument is not a valid Academic Term Begin Date value 
		-------------------------------------------------------------------------->
        <cfif variables.academicTermValidator.isValidBeginDate(arguments.beginDate)>
        	<cfset variables.beginDate = arguments.beginDate />    	
        </cfif>
    </cffunction>    
    
    <!--- End Date --->
    <cffunction name="setEndDate" access="public" output="no" returntype="void" hint="Set End Date" description="Sets End Date of the Term">
        <cfargument name="endDate" required="yes" />              
    	<!---------------------------- Test Cases ---------------------------------
			1. End Date argument is not a valid Academic Term End Date value 
		-------------------------------------------------------------------------->
        <cfif variables.academicTermValidator.isValidEndDate(arguments.endDate)>
        	<cfset variables.endDate = arguments.endDate />    	
        </cfif>
    </cffunction>       
    
    <!--- Active Flag --->
    <cffunction name="setActive" access="public" output="no" returntype="void" hint="Set the Term Active" description="Sets the Term as Active">
        <cfargument name="active" type="boolean" required="yes" />       
        <!--- Test Cases --- 
			1. None 
		-------------------->        
    	<cfset variables.active =  arguments.active />    	
    </cffunction>             
</cfcomponent>


