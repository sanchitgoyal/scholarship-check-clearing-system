<!------------------------------- Component Documentation ----------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalStudentRecordIDFormatException
		2. IllegalEmplIDFormatException
		3. IllegalStudentFirstNameFormatException
		4. IllegalStudentMiddleNameFormatException
		5. IllegalStudentLastNameFormatException
	
------------------------------------------------------------------------------------------------------------->

<cfcomponent displayname="Student" bindingname="Student" output="no">
	<cffunction name="init" access="public" hint="CFC Initializer" description="CFC Initializer" output="no" returntype="Student">
        <cfargument name="studentID" required="no" default="0" hint="Student's ID in SFA DB" />
        <cfargument name="emplID" required="no" default="" hint="Student's PeopleSoft Employee ID" />
    	<cfargument name="firstName" required="yes" hint="Student's First Name" />
        <cfargument name="middleName" required="no" default="" hint="Student's Middle Name" />
        <cfargument name="lastName" required="yes" hint="Student's Last Name" />
        <cfargument name="active" type="boolean" required="no" default="yes" hint="Is Student Active in System?" />
        
        <!--- Instantiate Spec Object --->
        <cfset variables.studentRecordValidator = CreateObject("component","gears.studentfinancialaid.specs.StudentSpec") />
        
        <cfset setStudentID(studentID) />
        <cfset setEmplID(emplID) />
        <cfset setFirstName(firstName) />
        <cfset setMiddleName(middleName) />
        <cfset setLastName(lastName) />
        <cfset setActive(active) />
        
        <cfreturn this />
    </cffunction>
  
  <!--- Getters --->  
    <cffunction name="getStudentID" access="public" hint="Returns Student ID" description="Returns Student ID" returntype="numeric" output="no">
	    <cfreturn variables.studentID />
    </cffunction>
    
	<cffunction name="getEmplID" access="public" hint="Returns PeopleSoft Empl ID" description="Returns PeopleSoft Empl ID" returntype="string" output="no">
		<cfreturn variables.emplID />
  	</cffunction>
    
    <cffunction name="getFirstName" access="public" hint="Returns Student's First Name" description="Returns Student's First Name" returntype="string" output="no">
	    <cfreturn variables.firstName />
    </cffunction>
    
    <cffunction name="getMiddleName" access="public" hint="Returns Student's Middle Name" description="Returns Student's Middle Name" returntype="string" output="no">
	    <cfreturn variables.middleName />
    </cffunction>      
    
    <cffunction name="getLastName" access="public" hint="Returns Student's Last Name" description="Returns Student's Last Name" returntype="string" output="no">
	    <cfreturn variables.lastName />
    </cffunction>    
  
    <cffunction name="isActive" access="public" hint="Returns If Student Is Active" description="Returns If Student Is Active" returntype="boolean" output="no">
	    <cfreturn variables.active />
    </cffunction>    

  <!----- Setters ------->
    <cffunction name="setStudentID" access="public" hint="Set Student Record ID" description="Set Student Record ID" returntype="void" output="no">
    	<cfargument name="studentID" required="yes" hint="Student Record ID" />
		<!------------ Test Cases ----------------
			1. Is not a valid student record ID 
		------------------------------------------>
        
        <!--- Cover Case 1: Throws IllegalStudentRecordIDFormatException --->
        <cfif variables.studentRecordValidator.isValidStudentID(arguments.studentID)>
			<cfset variables.studentID = arguments.studentID />  
        </cfif>    
    </cffunction>
    
    <cffunction name="setEmplID" access="public" hint="Set Empl ID" description="Set Empl ID" returntype="void" output="no">
    	<cfargument name="emplID" required="yes" hint="PeopleSoft Empl ID" />
        <!---------- Test Cases ------------
			1. Is not a valid UND Empl ID
		------------------------------------>
        
        <!--- Cover Case 1: Throws IllegalEmplIDFormatException --->
		<cfif variables.studentRecordValidator.isValidEmplID(arguments.emplID)>
			<cfset variables.emplID = arguments.emplID />  
        </cfif>    
    </cffunction>
    
    <cffunction name="setFirstName" access="public" hint="Set First Name" description="Set First Name" returntype="void" output="no">
    	<cfargument name="firstName" required="yes" hint="Student's First Name" />
        <!----------- Test Cases ---------
			1. Is not a valid First Name 
		--------------------------------->        

        <!--- Cover Case 1: Throws IllegalStudentFirstNameFormatException --->
        <cfif variables.studentRecordValidator.isValidFirstName(arguments.firstName)>        
        	<cfset variables.firstName = arguments.firstName />
        </cfif>      
    </cffunction>
    
    <cffunction name="setMiddleName" access="public" hint="Set Middle Name" description="Set Middle Name" returntype="void" output="no">
    	<cfargument name="middleName" required="yes" hint="Student's Middle Name" />
        <!----------- Test Cases -----------
			1. Is not a valid Middle Name
		------------------------------------>

        <!--- Cover Case 1: Throws IllegalStudentMiddleNameFormatException --->
        <cfif variables.studentRecordValidator.isValidMiddleName(arguments.middleName)>        
        	<cfset variables.middleName = arguments.middleName />
        </cfif>      
    </cffunction>
    
    <cffunction name="setLastName" access="public" hint="Set Last Name" description="Set Last Name" returntype="void" output="no">
    	<cfargument name="lastName" required="yes" hint="Student's Last Name" />
        <!--------- Test Cases -----------
			1. Is not a valid Last Name 
		---------------------------------->   
             
        <!--- Cover Case 1: Throws IllegalStudentLastNameFormatException --->
        <cfif variables.studentRecordValidator.isValidLastName(arguments.lastName)>        
        	<cfset variables.lastName = arguments.lastName />
        </cfif>      
    </cffunction>
    
    <cffunction name="setActive" access="public" hint="Set Student's Active Flag" description="Set Student's Active Flag" returntype="void" output="no">
		<cfargument name="active" type="boolean" required="yes" hint="Is Student Active In System?" />
        <!--- Test Cases ---
			1. None
		------------------->
        <cfset variables.active = arguments.active />
    </cffunction>
</cfcomponent>