<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalUserIDFormatException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent displayname="CheckComment" bindingname="CheckComment" hint="CFC to Represent A Comment on A Check Record" output="no">
<!--- Constructor --->
	<cffunction name="init" access="public" returntype="CheckComment" hint="CFC Initializer" description="CFC Initializer" output="no">
        <cfargument name="userID" type="numeric" required="yes" hint="Commenting User's ID" />        
    	<cfargument name="text" type="string" required="yes" hint="Comment Text" />
        <cfargument name="date" type="date" required="yes" hint="Comment Date/Time" />

		<!--- Instantiate Specification Object --->
		<cfset variables.commentValidator = CreateObject("component","gears.studentfinancialaid.specs.CheckCommentSpec") />

		<!--- Set Instance Variables --->
		<cfset setUserID(arguments.userID) />
        <cfset setText(arguments.text)  />
        <cfset setDate(arguments.date) />    
    
    	<!--- Return This Instance --->
    	<cfreturn this />
    </cffunction>
    
<!--- Getters --->
    
    <!--- User ID --->
    <cffunction name="getUserID" access="public" returntype="numeric" output="no" hint="Gets Comment User ID">
    	<cfreturn variables.userID />		
    </cffunction>
    <!--- Text --->
    <cffunction name="getText" access="public" returntype="string" output="no" hint="Gets Comment Text">
    	<cfreturn variables.text />		
    </cffunction>
    <!--- Date --->
    <cffunction name="getDate" access="public" returntype="date" output="no" hint="Gets Comment Date">
    	<cfreturn variables.date />		
    </cffunction>    
    
<!--- Setters --->
    
	<!--- User ID --->
    <cffunction name="setUserID" access="public" returntype="void" output="no" hint="Sets Commenting User ID">
    	<cfargument name="userID" type="numeric" required="yes" hint="Commenting User's ID" />        
		<!--- Throws IllegalUserIDFormatException --->
		<cfif variables.commentValidator.isValidUserID(arguments.userID)>
            <cfset variables.userID = arguments.userID />
        </cfif>            
    </cffunction>
    
    <!--- Text --->
    <cffunction name="setText" access="public" returntype="void" output="no" hint="Sets Comment Text">
    	<cfargument name="text" type="string" required="yes" hint="Comment Text" />
		<!--- Validate --->
		<cfif variables.commentValidator.isValidText(arguments.text)>
            <!--- Set Value --->
            <cfset variables.text = arguments.text />
        </cfif>         
    </cffunction>
    
    <!--- Date --->
    <cffunction name="setDate" access="public" returntype="void" output="no" hint="Sets Comment Date/Time">
		<cfargument name="date" type="date" required="yes" hint="Comment Date" />
		<!--- Validate --->
		<cfif variables.commentValidator.isValidDate(arguments.date)>
            <!--- Set Value --->
            <cfset variables.date = arguments.date />
        </cfif>      
    </cffunction>

</cfcomponent> 