<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalItemTypeCodeException
		2. IllegalItemTypeDescriptionException
		3. IllegalItemTypeFundException
		4. IllegalItemTypeDepartmentException
		5. IllegalItemTypeAccountException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent bindingname="ItemType" displayname="ItemType" hint="Student Financial Aid Item Types" output="no">
<!--- Constructor --->
	<cffunction name="init" access="public" returntype="ItemType" output="no" hint="Item Type CFC Initializer" description="Item Type CFC Initializer">		
		<cfargument name="itemTypeCode" required="yes" hint="Item Type Code" />
		<cfargument name="itemTypeDescription" required="yes" hint="Item Type Description" />
		<cfargument name="fund" required="yes" hint="Item Type Fund Value" />
		<cfargument name="department" required="yes" hint="Item Type Department Value" />
		<cfargument name="account" required="yes" hint="Item Type Account Value" />
        <cfargument name="active" type="boolean" required="no" default="yes" hint="Is Item Type Active?" />
		
        <!--- Instantiate Spec Object --->
        <cfset variables.itemTypeValidator = CreateObject("component","gears.studentfinancialaid.specs.ItemTypeSpec") />
	
		<cfset setItemTypeCode(arguments.itemTypeCode) />
        <cfset setItemTypeDescription(arguments.itemTypeDescription) />
        <cfset setFund(arguments.fund) />
        <cfset setDepartment(arguments.department) />
        <cfset setAccount(arguments.account) />
        <cfset setActive(arguments.active) />
        
        <cfreturn this />
	</cffunction>

<!--- Getters --->

	<!--- Item Type Code --->
    <cffunction name="getItemTypeCode" access="public" returntype="numeric" output="no" hint="Gets Item Type Code" description="Gets Item Type Code">
		<cfreturn variables.itemTypeCode />
	</cffunction>

	<!--- Item Type Description --->
    <cffunction name="getItemTypeDescription" access="public" returntype="string" output="no" hint="Gets Item Type Description" description="Gets Item Type Description">
		<cfreturn variables.itemTypeDescription />
	</cffunction>

	<!--- Item Type Fund --->
    <cffunction name="getFund" access="public" returntype="numeric" output="no" hint="Gets Item Type Fund" description="Gets Item Type Fund">
		<cfreturn variables.fund />
	</cffunction>
    
	<!--- Item Type Department --->
    <cffunction name="getDepartment" access="public" returntype="numeric" output="no" hint="Gets Item Type Department" description="Gets Item Type Department">
		<cfreturn variables.department />
	</cffunction>
    
	<!--- Item Type Account --->
    <cffunction name="getAccount" access="public" returntype="numeric" output="no" hint="Gets Item Type Account" description="Gets Item Type Account">
		<cfreturn variables.account />
	</cffunction>

	<!--- Item Type Active Flag --->
    <cffunction name="isActive" access="public" returntype="boolean" output="no" hint="Is Item Type Active?" description="Is Item Type Active?">
		<cfreturn variables.active />
	</cffunction>
    
                                 
<!--- Setters --->
	
	<!--- Item Type Code --->
	<cffunction name="setItemTypeCode" access="public" returntype="void" output="no" hint="Sets Item Type Code" description="Sets Item Type Code">					
		<cfargument name="itemTypeCode" required="yes" hint="Item Type Code To Set" />
		<!--------------- Test Cases ------------------
			1. itemTypeCode is not in valid format
		---------------------------------------------->
        
        <!--- Cover Case 1 : Throws IllegalItemTypeCodeException --->   
        <cfif variables.itemTypeValidator.isValidItemTypeCode(arguments.itemTypeCode)>  
 			<cfset variables.itemTypeCode = arguments.itemTypeCode />
        </cfif>    
	</cffunction>

	<!--- Item Type Description --->
	<cffunction name="setItemTypeDescription" access="public" returntype="void" output="no" hint="Sets Item Type Description" description="Sets Item Type Description">
		<cfargument name="itemTypeDescription" required="yes" hint="Item Type Description To Set" />
		<!----------------- Test Cases ------------------------
			1. itemTypeDescription is not in valid format 
		------------------------------------------------------->  
        
        <!--- Covers Case 1: Throws IllegalItemTypeDescriptionException --->  
        <cfif variables.itemTypeValidator.isValidItemTypeDescription(arguments.itemTypeDescription)>  
        	<cfset variables.itemTypeDescription = arguments.itemTypeDescription />   
        </cfif>         
	</cffunction>
    
	<!--- Item Type Fund --->
	<cffunction name="setFund" access="public" returntype="void" output="no" hint="Sets Item Type Fund" description="Sets Item Type Fund">
		<cfargument name="fund" required="yes" hint="Item Type Fund To Set" />
		<!------------ Test Cases ---------------
			1. fund is not in valid format
		---------------------------------------->

        <!--- Covers Case 1: Throws IllegalItemTypeFundException --->  
        <cfif variables.itemTypeValidator.isValidItemTypeFund(arguments.fund)>  
			<cfset variables.fund = arguments.fund />   
        </cfif>         
	</cffunction>
    
    <!--- Item Type Department --->
    <cffunction name="setDepartment" access="public" returntype="void" output="no" hint="Sets Item Type Department" description="Sets Item Type Description">
		<cfargument name="department" required="yes" hint="Item Type Department To Set" />
		<!-------------- Test Cases ---------------
			1. department is not in valid format
		------------------------------------------->
        
        <!--- Covers Case 1: Throws IllegalItemTypeDepartmentException --->  
        <cfif variables.itemTypeValidator.isValidItemTypeDepartment(arguments.department)>  
			<cfset variables.department = arguments.department />   
        </cfif>         
	</cffunction>
    
    <!--- Item Type Account --->
    <cffunction name="setAccount" access="public" returntype="void" output="no" hint="Sets Item Type Account" description="Sets Item Type Account">
		<cfargument name="account" required="yes" hint="Item Type Account To Set" />
		<!-------------- Test Cases ---------------
			1. account is not in valid format
		------------------------------------------->
        
        <!--- Covers Case 1: Throws IllegalItemTypeAccountException --->  
        <cfif variables.itemTypeValidator.isValidItemTypeAccount(arguments.account)>  
			<cfset variables.account = arguments.account />   
        </cfif>         
	</cffunction>
    
	<!--- Item Type Active Flag --->
    <cffunction name="setActive" access="public" returntype="void" output="no" hint="Set Item Type Active Flag" description="Set Item Type Active Flag">
		<cfargument name="active" type="boolean" required="yes" hint="yes/no" />
		<cfset variables.active = arguments.active />
	</cffunction>            
</cfcomponent>