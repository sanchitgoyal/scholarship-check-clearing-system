<cfcomponent bindingname="CheckDisbursement" displayname="CheckDisbursement" output="no" hint="Check Disbursement CFC">
	<!--- Constructor --->
    <cffunction name="init" access="public" returntype="CheckDisbursement" output="no" hint="CFC Initializer" description="CFC Initializer">
		<cfargument name="idStudent" type="numeric" required="yes" hint="Record ID of Disbursed Student" />
        <cfargument name="termCode" type="string" required="yes" hint="Code of Term of Disbursement" />
        <cfargument name="amount" type="numeric" required="yes" hint="Amount of Disbursement" />
        <cfargument name="authDisb" type="boolean" required="yes" hint="Flag for Authorization of Disbursement" />
        <cfargument name="vouchGen" type="boolean" required="yes" hint="Flag for Voucher Generation" />
        
        <cfset setIDStudent(idStudent) /> 
        <cfset setTermCode(termCode) />
        <cfset setAmount(amount) />
        <cfset setAuthDisb(authDisb) />
        <cfset setVouchGen(vouchGen) />

    	<cfreturn this />		
    </cffunction>
    
    <!--- Setters --->
    <cffunction name="setIDStudent" access="public" returntype="void" output="no" hint="Set Student Record ID" description="Set Student Record ID">
		<cfargument name="idStudent" type="numeric" required="yes" hint="Record ID of Disbursed Student" />
        <!--------------------- Test Cases ------------------------
			1. Non Numeric Student Record ID 
			2. Student Record ID Non Positive (Zero/Negative)
			3. Non Integer Student Record ID
		---------------------------------------------------------->			
		<cfif NOT (IsNumeric(idStudent) and idStudent gt 0 and Int(idStudent) eq idStudent)>        
            <cfthrow type="IllegalStudentRecordIDException" message="Student Record ID Should be Positive Non Zero Integer" 
                    detail="Student Record ID Should be Positive Non Zero Integer" extendedinfo="#idStudent#" />           
        <cfelse>
			<cfset variables.idStudent = arguments.idStudent />
        </cfif>        
    </cffunction>
    
    <cffunction name="setTermCode" access="public" returntype="void" output="no" hint="Set Code of Term of Disbursement" 
    		description="Code of Term of Disbursement">
        <cfargument name="termCode" type="string" required="yes" hint="Code of Term of Disbursement" />
		<cfset variables.termCode = arguments.termCode />
    </cffunction>
    
    <cffunction name="setAmount" access="public" returntype="void" output="no" hint="Set Amount of Disbursement" description="Set Amount of Disbursement">
        <cfargument name="amount" type="numeric" required="yes" hint="Amount of Disbursement" />
		<cfset variables.amount = arguments.amount />
    </cffunction>
    
	<cffunction name="setAuthDisb" access="public" returntype="void" output="no" hint="Set/Unset Flag for Authorization of Disbursement" 
    			description="Set/Unset Flag for Authorization of Disbursement">
        <cfargument name="authDisb" type="boolean" required="yes" hint="Flag for Authorization of Disbursement" />
		<cfset variables.authDisb = arguments.authDisb />
    </cffunction>

	<cffunction name="setVouchGen" access="public" returntype="void" output="no" hint="Set/Unset Flag for Voucher Generation" 
    			description="Set/Unset Flag for Voucher Generation">
        <cfargument name="vouchGen" type="boolean" required="yes" hint="Flag for Voucher Generation" />
		<cfset variables.vouchGen = arguments.vouchGen />
    </cffunction>
    
    <!--- Getters --->
    <cffunction name="getIDStudent" access="public" returntype="numeric" output="no" hint="Set Student Record ID" description="Get Student Record ID">
        <cfreturn variables.idStudent />
    </cffunction>
    
    <cffunction name="getTermCode" access="public" returntype="string" output="no" hint="Get Code of Term of Disbursement" 
    		description="Get Code of Term of Disbursement">
		<cfreturn variables.termCode />
    </cffunction>
    
    <cffunction name="getAmount" access="public" returntype="numeric" output="no" hint="Get Amount of Disbursement" description="Get Amount of Disbursement">
		<cfreturn variables.amount />
    </cffunction>
    
	<cffunction name="isAuthDisb" access="public" returntype="boolean" output="no" hint="Get Flag for Authorization of Disbursement" 
    			description="Get Flag for Authorization of Disbursement">
		<cfreturn variables.authDisb />
    </cffunction>

	<cffunction name="isVouchGen" access="public" returntype="boolean" output="no" hint="Get Flag for Voucher Generation" 
    			description="Get Flag for Voucher Generation">
		<cfreturn variables.vouchGen />
    </cffunction>
</cfcomponent> 