<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. UnknownCFCException 
	
---------------------------------------------------------------------------------------------------->
<cfcomponent displayname="AppConfig" bindingname="AppConfig" output="no" hint="CFC for Handling Application Settings">    
    <!--- Constructor --->
    <cffunction name="init" access="public" returntype="AppConfig" output="no" hint="CFC Constructor" description="CFC Constructor">
    	<cfset variables.appStructure = {
			path = "gears.studentfinancialaid",
			cfcGroups = ArrayNew(1)
		} />
        <cfset variables.appStructure.cfcGroups[1] = {
				path = "gears.studentfinancialaid",
				cfcList = "AcademicTerm,CheckComment,ItemType,Student,CheckTag,CheckDisbursement,Check"
		} />        
        <cfset variables.appStructure.cfcGroups[2] = {
				path = "gears.studentfinancialaid.managers",
				cfcList = "AcademicTermManager,CheckCommentManager,ItemTypeManager,ReportsManager,StudentManager,TagManager,CheckManager"
		} />        
        <cfset variables.appStructure.cfcGroups[3] = {
				path = "gears.studentfinancialaid.reports",
				cfcList = "AbstractReport,CheckDisbursementReport"
		} />
        <cfset variables.appStructure.cfcGroups[4] = {
				path = "gears.studentfinancialaid.specs",
				cfcList = "AcademicTermSpec,CheckCommentSpec,CheckSpec,CheckTagSpec,ExternalSpec,ItemTypeSpec,StudentSpec"
		} />
        <cfset variables.appStructure.cfcGroups[5] = {
				path = "gears.studentfinancialaid.utils",
				cfcList = "AppConfig,GearsHandler,Mailers"
		} />         
                
        <cfreturn this />               
    </cffunction>
    
	<!--- Get A CFC's Full Path Configuration --->
	<cffunction name="getCFCFullPath" access="public" returntype="string" output="no" hint="Returns a CFC's Full Path" description="Pass in CFC Name to Get Its Full Path Configuration">
    	<cfargument name="cfcShortName" type="string" required="yes" hint="CFC Short Name" />
        <!-------------- Test Cases ---------------
			1. Blank CFC Short Name Argument 
			2. Unknown CFC Short Name Argument 
		------------------------------------------>
        
        <!------------------------------------ 
			Compare argument against known 
			CFC names in the application 
		------------------------------------->
		<cfloop from="1" to="#ArrayLen(appStructure.cfcGroups)#" index="i">
            <cfif ListFind(appStructure.cfcGroups[i].cfcList,arguments.cfcShortName)>
				<cfreturn appStructure.cfcGroups[i].path & "." & arguments.cfcShortName />            	
            </cfif>
        </cfloop>
        
        <!--- Cover Test Cases: 1 & 2 --->
        <cfthrow type="UnknownCFCException" message="Unknown CFC Name" detail="No such CFC found in the application" extendedinfo="#arguments.cfcShortName#" />
    </cffunction>
</cfcomponent>