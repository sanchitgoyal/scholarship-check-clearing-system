<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. UnknownCFCException 
	
---------------------------------------------------------------------------------------------------->
<cfcomponent displayname="AppConfig" bindingname="AppConfig" output="no" hint="CFC for Handling Application Settings">
	<cfset basePath = "gears.studentfinancialaid" />    
    <cfset basicCFC  = cfcBasePath  />
    <cfset managerCFC = cfcBasePath & ".managers" />
    <cfset reportCFC = cfcBasePath & ".reports" />
    <cfset specCFC = cfcBasePath & ".specs" />
    <cfset utilCFC = cfcBasePath & ".utils" />
<!---    
	<!--- Get A CFC's Full Path Configuration --->
	<cffunction name="getCFCFullPath" access="public" returntype="string" output="no" hint="Returns a CFC's Full Path" description="Pass in CFC Name to Get Its Full Path Configuration">
    	<cfargument name="cfcShortName" type="string" required="yes" hint="CFC Short Name" />
        <!-------------- Test Cases ---------------
			1. Blank CFC Short Name Argument 
			2. Unknown CFC Short Name Argument 
		------------------------------------------>
        
        <!------------------------------------ 
			Compare argument against known 
			CFC names in the application 
		------------------------------------->
        <cfswitch expression="#arguments.cfcShortName#">
        	<cfcase value="AcademicTerm"><cfreturn basicCFC & "AcademicTerm"/></cfcase>
            <!--- Cover Test Cases: 1 & 2 --->
            <cfdefaultcase>
            	<!--- Throw Unknown CFC Name Exception--->
            	<cfthrow type="UnknownCFCException" message="Not a Known CFC" detail="CFC Name not a known CFC in the Application"  extendedinfo="#arguments.cfcShortName#" />
            </cfdefaultcase>            
        </cfswitch>			
    </cffunction>
--->    
	
</cfcomponent>