<cfcomponent displayname="GearsHandler" bindingname="GearsHandler" output="no" hint="Handles Gears Framework Gaps">
	<!--- Constructor --->
    <cffunction name="init" access="public" returntype="GearsHandler" output="no" 
    			hint="CFC Initializer" description="CFC Initializer">
		<cfargument name="dataSource" type="string" required="yes" hint="Coldfusion Datasource to Use with CFC" />
        <cfset variables.dataSource = arguments.dataSource />   
        <cfreturn this />             
	</cffunction>

	<!--- Get Gears User Name by ID --->                    
	<cffunction name="getUserNameByID" access="public" returntype="string" output="no" 
    		hint="Gets Gears User Name by User ID" description="Gets Gears User Name by User ID">
		<cfargument name="userID" type="numeric" required="yes" hint="Gears User ID" />
        <cfargument name="formatString" type="string" required="no" default="lastname, firstname" 
        			hint="Format String to Parse for lastname & firstname keywords to replace with values" />
		<!------------------------------- Test Cases -------------------------------------
			1. user ID is not in standard gears format
			2. format string doesn't contain either 'lastname' or  'firstname' keywords
			3. Given User ID is non exisistent in gears DB
			4. Given Datasource does not have sufficient privlidges 
		---------------------------------------------------------------------------------->			
        <cfquery datasource="#variables.dataSource#" name="qNameByID">
        	SELECT FirstName, LastName
            FROM gears.people
            WHERE UserID = #arguments.userID#
        </cfquery>
        
        <cfreturn Replace(Replace(formatString,'lastname',qNameByID.LastName[1]),'firstname',qNameByID.FirstName[1]) />
        
	</cffunction>            
</cfcomponent>