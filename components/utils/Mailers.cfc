<cfcomponent displayname="Mailers" bindingname="Mailers" output="no" hint="CFC for Handling Application Mailing Needs">
	<!--- Private Functions --->
    
	<!--- Get Web Team Email-ID for Use in Other Functions --->
    <cffunction name="getWebTeamEmailID" access="private" returntype="string"  hint="Returns Web Applications Team EMail IDs" 	
    			description="Returns Web Applications Team EMail IDs" output="no">
                <!--- Test Cases ---
					1. None 
				-------------------->
                
        	<!---<cfreturn #application.primarysupportemail# />--->
            <cfreturn "sanchit.goyal@und.edu" />
    </cffunction> 
    
    <!--- Get SFA Main Application Email-ID for Use in Other Functions --->
    <cffunction name="getSFAApplicationEmailID" access="private" returntype="string" hint="Returns SFA Application Email ID" 
    			description="Returns SFA Application Email ID" output="no">
                <!--- Test Cases ---
					1. None 
				-------------------->                
    	<cfreturn "StudentFinancialAid-NoReply@mail.und.edu" />
    </cffunction>
    
    <!--- Public Functions --->
    
	<!--- Web Team Mailers --->   
        
    <!--- Main Mailer Function ---> 
	<cffunction name="mailWebTeam" access="public" returntype="void" hint="Mails Web Team" description="Mails Web Team" output="no">
    	<cfargument name="subject" required="yes" type="string">
        <cfargument name="body" required="yes" type="any">
        <!---------------- Test Cases -----------------------
			1. Subject is Empty String 	--> Doesn't Matter 
			2. Body is Empty			-->	Doesn't Matter 
		----------------------------------------------------->
        <cfmail from="#getSFAApplicationEmailID()#" to="#getWebTeamEmailID()#" 
        	 	subject="#subject#" type="html">
        		<cfdump var="#body#" />
        </cfmail> 
    </cffunction>
    
    <cffunction name="mailExceptionToWebTeam" access="public" returntype="void" hint="Mails Exception To Web Team" description="Mails Exception To Web Team" output="no">
    	<cfargument name="exceptionObject" required="yes" type="any" />
        <cfargument name="subject" default="" type="string" /> 
        <cfargument name="userID" default="System" type="string" />
        
        <cfif subject eq ''><cfset subject = exceptionObject.message /></cfif>
        <cfmail from="#getSFAApplicationEmailID()#" to="#getWebTeamEmailID()#" 
        	 	subject="#subject#" type="html">
                Encoundered by: #userID# 
                <br/>
        		<cfdump var="#exceptionObject#" />
        </cfmail> 
    </cffunction>

    
    <!--- Non-Numeric Term Code Error Mailer --->
    <cffunction name="webTeamErrorMailer" access="public" returntype="void" hint="Mails Errors To Web Team"
    			description="Mails Errors To Web Team" output="no">
    	<cfargument name="subject" required="yes" type="string" />
        <cfargument name="user" default="System" required="no" type="string" /> 
        <cfargument name="context" required="yes" type="string" />
        <cfargument name="additionalDetail" required="no" default="" type="any" />
        
        <cfset body = "
						Encountered By	: #user# <br/>
						Error-Context	: #context# <br/>	
						<br /><br />											
					" & #session# />
        <cfdump var="#body#" />
    </cffunction>
</cfcomponent>