<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods
		
		1. None 
----------------------------------------------------------------------------------------------------> 

<cfcomponent bindingname="AbstractReport" displayname="AbstractReport" hint="Abstract SFA Report" output="no">
	<!--- Constructor --->
    <cffunction name="init" access="public" returntype="AbstractReport" output="no" description="CFC Initializer" hint="CFC Initializer">
    	<!--- Input: Coldfusion Data Source --->
        <cfargument name="dataSource" type="string" required="yes" hint="Data Source" />
        <!--- Copy Data Source --->
        <cfset variables.dataSource = arguments.dataSource />
        <!--- Return This Instance --->
        <cfreturn this />
    </cffunction>
</cfcomponent>