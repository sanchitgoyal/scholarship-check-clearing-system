<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods
		1. UnknownReportTypeException
		2. IllegalTermCodeException
		
----------------------------------------------------------------------------------------------------> 

<cfcomponent displayname="CheckDisbursementReport" bindingname="CheckDisbursementReport" extends="gears.studentfinancialaid.reports.AbstractReport" hint="Check Disbursement Report" output="no">
	<!--- Constructor --->
    <cffunction name="init" access="public" returntype="CheckDisbursementReport" output="no" hint="CFC Initializer" description="CFC Initializer">
    	<!--- Input: Coldfusion Data Source --->
        <cfargument name="dataSource" type="string" required="yes" hint="Data Source Name" />
        <!--- Initialize Data Source --->
        <cfset variables.dataSource = arguments.dataSource />
        <!--- Initialize Spec Validator --->
        <cfset variables.academicTermValidator = CreateObject("component","gears.studentfinancialaid.specs.AcademicTermSpec")/>
        <!--- Return This Instance --->
        <cfreturn this />
    </cffunction>
    
    <!--- Export Report: As Query --->
	<cffunction name="getAsQuery" access="public" returntype="query" output="no" hint="Get Report as Query" description="Get Report as Query">
    	<!--- Input: Academic Term Code --->
        <cfargument name="termCodes" type="string" required="yes" hint="List of Academic Term Codes" />
        <!--- Input: Type of Report Authorized/Outstanding/Vouchered --->
        <cfargument name="reportType" type="string" required="yes" hint="Type of Report:authorized|outstanding|vouchered" />
        <!-------------------------------- Test Cases ---------------------------------
			1. Term Codes is not a valid comma separated non empty list of term codes
			2. Any term code inside the term codes list is not in valid format
			2. Report Type is anything but one of authorized|outstanding|vouchered
		------------------------------------------------------------------------------->	
        
        <!--- Case 1 : ---> 
		<cfif ListLen(arguments.termCodes) NEQ 0>
			
			<!--- Case 2 : Throws IllegalTermCodeException --->
            <cfloop list="#arguments.termCodes#" index="termCode">
            	<cfset academicTermValidator.isValidTermCode(termCode=termCode) />
            </cfloop>
            
			<!--- Case 3--->
            <cfif arguments.reportType neq 'authorized' and arguments.reportType neq 'outstanding' and arguments.reportType neq 'vouchered'>
                <!--- Throw UnknownReportTypeException --->
                <cfthrow type="UnknownReportTypeException" message="Unknown Report Type" 
                        detail="Report Type Should be one of authorized|outstanding|vouchered" extendedinfo="#arguments.reportType#" />
            </cfif> 
            
            <!--- Query Database --->
            <cfquery name="qReportDisbursements" datasource="#variables.dataSource#">
                SELECT  <!--- Disbursed Student Name --->
                        CONCAT(ifnull(std.lastName,''),', ',ifnull(std.firstName,''),' ',ifnull(std.middleName,'')) as student,
                        <!--- Disbursed Student EmplID --->
                        std.emplID as emplID,
                        <!--- Academic Term of Disbursement --->
                        chkDisb.termCode as term,
                        <!--- Amount Disbursed --->
                        chkDisb.amount as disbursementAmount,
                        <!--- Check Number --->
                        chk.checkNumber as checkNumber,
                        <!--- Item Type Description --->
                        itype.description as description,
                        <!--- Check Amount --->
                        chk.checkTotal as checkAmount,
                        <!--- Check Deposit Date --->
                        date_format(chk.depositedOn ,'%m-%d-%Y') as depositDate,
                        <!--- Check Deposit Number --->
                        chk.depositNumber as depositNumber,
                        <!--- Item Type Code --->
                        itype.itemTypeCode as itemType,
                        <!--- Item Type Fund --->
                        itype.fund as fund,
                        <!--- Item Type Department --->
                        itype.department as department,
                        <!--- Item Type Account --->
                        itype.department as account
                FROM    und_finaid.checkdisbursement chkDisb,
                        und_finaid.check chk,
                        und_finaid.student std,
                        und_finaid.itemTypes itype
                WHERE   chkDisb.idStudent = std.idStudent
                AND     chkDisb.depositNumber = chk.depositNumber
                AND     chk.itemTypeCode = itype.itemTypeCode  
                AND		chkDisb.termCode IN (<cfqueryparam cfsqltype="cf_sql_numeric" list="yes" value="#Trim(arguments.termCodes)#" />) 	
                <cfswitch expression="#arguments.reportType#">
                    <cfcase value="authorized">
                        AND	chkDisb.authDisb = 1 AND chkDisb.vouchGen = 0
                    </cfcase>
                    <cfcase value="outstanding">
                        AND	chkDisb.authDisb = 0 AND chkDisb.vouchGen = 0 
                    </cfcase>
                    <cfcase value="vouchered">
                        AND	chkDisb.authDisb = 0 AND chkDisb.vouchGen = 1
                    </cfcase>                                        
                </cfswitch>            
            </cfquery>
            
			<!--- Return Query Results --->
            <cfreturn qReportDisbursements />
            
        <cfelse>
	    	<!--- Throw Specific Exception --->
	    	<cfthrow type="IllegalTermCodeException" message="Bad Formatting for Term Code" 
	        		detail="Term Code Should be a 4 Digit Positive Number. Eg: 0750" extendedinfo="#arguments.termCodes#" />
        </cfif>
        
    </cffunction>

    <!--- Export Report: As XLS File --->
	<cffunction name="getAsXLSFile" access="public" returntype="any" output="no" hint="Get Report as Query" description="Get Report as Query">
    	<!--- Input: Academic Term Code List --->
        <cfargument name="termCodes" required="yes" hint="List of Academic Term Codes" />
        <!--- Input: Type of Report Authorized/Outstanding/Vouchered --->
        <cfargument name="reportType" type="string" required="yes" hint="Type of Report:authorized|outstanding|vouchered" />
        <!-------------------------------- Test Cases ---------------------------------
			1. Term Code is not in valid format
			2. Report Type is anything but one of authorized|outstanding|vouchered
		------------------------------------------------------------------------------->

		<!--- Query Records : Throws UnknownReportTypeException(Case 2), IllegalTermCodeException (Case 1) --->            
        <cfset reportQuery = getAsQuery(termCodes=arguments.termCodes,reportType=arguments.reportType) />

        <!--- Create Spreadsheet Object --->
        <cfset theSheet = SpreadsheetNew("Term Disbursement Report")>
        <!--- Add Header Row --->
        <cfset reportHeader = 'Student,EmplID,Term,Disbursement Amount($),Scholarship Check No.,Description,Check Amount($),Deposit Date,Deposit No.,Item Type,Fund,Department,Account'/>
        <cfset SpreadSheetAddRow(theSheet, reportHeader) />
        <!--- add all the rows from the query to the sheet --->
        <cfset spreadSheetAddRows(theSheet, reportQuery) />
        <!--- create a custom number format for the ItemType --->
        <cfset itemTypeColumFormat = {} />
        <cfset itemTypeColumFormat.dataformat = "0" />
        <cfset spreadSheetFormatColumn(theSheet, itemTypeColumFormat, 9) />
        <!--- Loop through Query & Add Rows --->
        <cfloop query="reportQuery">
            <cfscript>
                /***** Format Cells *****/
                spreadSheetSetCellFormula(theSheet, """#emplID#""", reportQuery.currentRow+1, 2);
                spreadSheetSetCellFormula(theSheet, """#itemType#""", reportQuery.currentRow+1, 10);      
                spreadSheetSetCellFormula(theSheet, """#fund#""", reportQuery.currentRow+1, 11);      
                spreadSheetSetCellFormula(theSheet, """#department#""", reportQuery.currentRow+1, 12);      
                spreadSheetSetCellFormula(theSheet, """#account#""", reportQuery.currentRow+1, 13);         				
            </cfscript>
        </cfloop>
        <!--- Return Sheet Object --->
        <cfreturn theSheet />
	</cffunction>                
</cfcomponent>