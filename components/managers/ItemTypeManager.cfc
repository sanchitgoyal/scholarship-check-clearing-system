<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods
	
		1. DuplicateItemTypeParameterException
		2. IllegalItemTypeCodeException
		3. CannotDeleteItemTypeException
		4. ItemTypeRecordNotFoundException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent displayname="ItemTypeManager" bindingname="ItemTypeManager" hint="Item Type Manager CFC" output="no">
	<!--- Constructor --->
    <cffunction name="init" access="public" output="no" returntype="ItemTypeManager" hint="CFC Initializer" description="CFC Initializer">
		<!--- Input: Coldfusion Data Source --->
        <cfargument name="dataSource" type="string" required="yes" hint="ColdFusion Datasource" />
        <!--- Set Data Source Reference --->
		<cfset variables.dataSource = arguments.dataSource /> 
		<!--- Instantiate Item Type Spec --->
		<cfset variables.itemTypeValidator = CreateObject("component","gears.studentfinancialaid.specs.ItemTypeSpec") />  
        <!--- Return This Instance --->
        <cfreturn this />
    </cffunction> 
    
    <!--- Create Item Type Record --->    
    <cffunction name="createItemType" access="public" output="no" returntype="void" hint="Create Item Type Record" description="Create Item Type Record">
		<!--- Input: Item Type Object --->
        <cfargument name="itemTypeRecord" required="yes" type="gears.studentfinancialaid.ItemType" hint="Item Type CFC Object" />
        <!---------------- Test Cases ----------------
			1. Duplicate record exists in database 
		--------------------------------------------->  
        <cftransaction action="begin" isolation="serializable">
        	<cftry>
            	<!--- Execute DB Changes --->
                <cfquery result="iItemTypeRecord" datasource="#variables.dataSource#">
                    INSERT INTO und_finaid.itemtypes
                    VALUES (
                    			<!--- Item Type Code --->
                                #itemTypeRecord.getItemTypeCode()#,
                                <!--- Description --->
                                '#itemTypeRecord.getItemTypeDescription()#',
                                <!--- Fund --->
                                #itemTypeRecord.getFund()#,
                                <!--- Department --->
                                #itemTypeRecord.getDepartment()#,
                                <!--- Account --->
                                #itemTypeRecord.getAccount()#,
                                <!--- Active Flag --->
								<cfif itemTypeRecord.isActive()>1<cfelse>0</cfif>
                            )
                </cfquery>
                
                <!--- Catch & Process Exceptions --->
                
                <!--- Database Exceptions --->
                <cfcatch type="database">
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- MySQL Dependent Code --->
                    <cfswitch expression="#cfcatch.NativeErrorCode#">
                    	<!--- Duplicate Item Code Found Case --->
                    	<cfcase value="1062">
                        	<!--- Throw Specific Exception --->
                        	<cfthrow type="DuplicateItemTypeParameterException" message="Item Type Code Cannot be Duplicated" 
                                    detail="Item Type Code should uniquely identify an Item Type in the system and therefore cannot be duplicated" 
                                    extendedinfo="#itemTypeRecord.getItemTypeCode()#" />
                        </cfcase>
                        <!--- Unknown Case --->
                        <cfdefaultcase>
                        	<!--- Rethrow For Upper Level Processing --->
                        	<cfrethrow />
                        </cfdefaultcase>
                    </cfswitch>
                </cfcatch>
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
           </cftry>
        </cftransaction>
	</cffunction> 
    
    <!--- Edit Item Type Record --->
    <cffunction name="editItemType" access="public" output="yes" returntype="void" hint="Edit Item Type Record" description="Edit Item Type Record">
		<!--- Input: Item Type Code of Record to Edit --->
        <cfargument name="itemTypeOriginalCode" required="yes" hint="Original DB Reference for Item Type" />
        <!--- Input: New Item Type Record to Overwrite --->
        <cfargument name="itemTypeRecord" type="gears.studentfinancialaid.ItemType" required="yes" hint="Item Type Record to Create in DB" />
        <!-------------------------------- Test Cases -----------------------------
			1. itemTypeOriginalCode is not in valid format 									
			2. Duplicate matching record exists in the database 
		--------------------------------------------------------------------------->
        
        <!--- Cover Case 1 : Throws IllegalItemTypeCodeException --->   
        <cfif variables.itemTypeValidator.isValidItemTypeCode(arguments.itemTypeOriginalCode)>  
            <cftransaction action="begin" isolation="serializable">
                <cftry>
                    <!--- Execute DB Changes --->
                    <cfquery result="uItemTypeRecord" datasource="#variables.dataSource#">
                        UPDATE und_finaid.itemtypes
                        SET
                            <!--- Item Type Code --->
                            itemTypeCode = #itemTypeRecord.getItemTypeCode()#,
                            <!--- Item Type Description --->
                            description = '#itemTypeRecord.getItemTypeDescription()#',
                            <!--- Item Type Fund --->
                            fund = #itemTypeRecord.getFund()#,
                            <!--- Item Type Department --->
                            department = #itemTypeRecord.getDepartment()#,
                            <!---- Item Type Account --->
                            account = #itemTypeRecord.getAccount()#,
                            <!--- Item Type Active Flag --->
                            active = <cfif itemTypeRecord.isActive()>1<cfelse>0</cfif>
                        WHERE itemTypeCode = #itemTypeOriginalCode#   
                    </cfquery>
                    
                    <!--- Catch & Process Exceptions --->
                    
                    <!--- Database Exceptions --->
                    <cfcatch type="database">
                        <!--- Rollback Transaction --->
                        <cftransaction action="rollback" isolation="serializable" />
                        <!--- MySQL Dependent Code --->
                        <cfswitch expression="#cfcatch.NativeErrorCode#">
                            <!--- Duplicate Item Code Found Case --->
                            <cfcase value="1062">
                                <!--- Throw Specific Exception --->
                                <cfthrow type="DuplicateItemTypeParameterException" message="Item Type Code Cannot be Duplicated" 
                                        detail="Item Type Code should uniquely identify an Item Type in the system and therefore cannot be duplicated" 
                                        extendedinfo="#itemTypeRecord.getItemTypeCode()#" />
                            </cfcase>
                            <!--- Unknown Case --->
                            <cfdefaultcase>
                                <!--- Rethrow For Upper Level Processing --->
                                <cfrethrow />
                            </cfdefaultcase>
                        </cfswitch>
                    </cfcatch>
                    
                    <!--- Unknown Exceptions --->
                    <cfcatch type="any"> 
                        <!--- Rollback Transaction --->
                        <cftransaction action="rollback" isolation="serializable" />
                        <!--- Rethrow For Upper Level Processing --->
                        <cfrethrow />
                    </cfcatch>
               </cftry>
            </cftransaction>
		</cfif>            
	</cffunction> 
    
    <!--- Activate/Disable Item Type Record(s) --->
    <cffunction name="setItemTypeActive" access="public" output="no" returntype="void" hint="Activate/Deactivate Item Type(s)" description="Activate/Deactivate Item Type(s)">
		<!--- Input: List of Item Type Codes --->
        <cfargument name="selectedItemTypesList" type="string" required="yes" hint="List of Item Type Codes" />
        <!--- Input: Flag for Activation/Deactivation --->
        <cfargument name="active" type="boolean" required="yes" hint="true/false" />
        <!-------------------------- Test Cases ---------------------------
			1. selectedItemTypesList is empty 
			2. selectedItemTypesList contains an illegal item type code 
		------------------------------------------------------------------>
                
		<!--- Convert List to Array for easier handelling--->        
		<cfset selectedItemTypeArray = ListToArray('#arguments.selectedItemTypesList#',',') />
        
		<!--- Handle Test Case 1 --->
        <cfif ArrayLen(selectedItemTypeArray) eq 0>
        	<cfthrow type="IllegalItemTypeCodeException" message="No Item Type Selected For Activation/Deactivation" 
            	detail="Activation/Deactivation operation cannot be completed without an Item Type selection" />
		</cfif>                  
        
        <!--- Handle Test Case 2 --->
        <cfloop index="i" from="1" to="#ArrayLen(selectedItemTypeArray)#">
			<!--- Throws IllegalItemTypeCodeException --->   
            <cfif variables.itemTypeValidator.isValidItemTypeCode(selectedItemTypeArray[i])><!---Do Nothing---></cfif>    
        </cfloop>
                        
        <cftransaction action="begin" isolation="serializable">
        	<cftry>
            	<!--- Execute DB Changes --->
                <cfquery result="iItemTypeRecord" datasource="#variables.dataSource#">
                    UPDATE und_finaid.itemtypes
                    SET active = <cfif active>1<cfelse>0</cfif>
                    <cfif ArrayLen(selectedItemTypeArray) eq 1>
                    	WHERE itemTypeCode = #Trim(arguments.selectedItemTypesList)# 
                    <cfelse>
                    	WHERE itemTypeCode IN (#Trim(arguments.selectedItemTypesList)#)
                    </cfif>      
                </cfquery>
                
                <!--- Catch & Process Exceptions --->
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
           </cftry>
        </cftransaction>
	</cffunction> 
    
    
    <!--- Delete Item Type Record(s) --->
    <cffunction name="deleteItemType" access="public" output="no" returntype="void" hint="Delete Item Type Record(s)" description="Delete Item Type Record(s)">
		<!--- Input: List of Item Type Codes --->
        <cfargument name="selectedItemTypesList" type="string" required="yes" hint="List of Item Type Codes" />
        <!-------------------------- Test Cases ---------------------------
			1. selectedItemTypesList is empty 
			2. selectedItemTypesList contains an illegal item type code 
		------------------------------------------------------------------>
                
		<!--- Convert List to Array for easier handelling--->        
		<cfset selectedItemTypeArray = ListToArray('#arguments.selectedItemTypesList#',',') />
        
		<!--- Handle Test Case 1 --->
        <cfif ArrayLen(selectedItemTypeArray) eq 0>
        	<cfthrow type="IllegalItemTypeCodeException" message="No Item Type Selected For Deletion" 
            	detail="Deletion operation cannot be completed without an Item Type selection" />
		</cfif>                  
        
        <!--- Handle Test Case 2 --->
        <cfloop index="i" from="1" to="#ArrayLen(selectedItemTypeArray)#">
			<!--- Throws IllegalItemTypeCodeException --->   
            <cfif variables.itemTypeValidator.isValidItemTypeCode(selectedItemTypeArray[i])><!---Do Nothing---></cfif>    
        </cfloop>
        
        <cftransaction action="begin" isolation="serializable">
        	<cftry>
            	<!--- Execute DB Changes --->
                <cfquery result="dItemTypeRecord" datasource="#variables.dataSource#">
                    DELETE FROM und_finaid.itemtypes
                    <cfif ArrayLen(selectedItemTypeArray) eq 1>
                    	WHERE itemTypeCode = #Trim(selectedItemTypesList)# 
                    <cfelse>
                    	WHERE itemTypeCode IN (#Trim(selectedItemTypesList)#)
                    </cfif>      
                </cfquery>
                
                <!--- Catch & Process Exceptions --->
                
                <!--- Database Exceptions --->
                <cfcatch type="database">
                    <!--- Roll Back Transaction --->
                    <cftransaction action="rollback" isolation="serializable" />
                    <!--- MySQL Dependent Processing --->
                    <cfswitch expression="#cfcatch.NativeErrorCode#">
                        <!--- Foreign Key Constraint Violation --->
                        <cfcase value="1451">
                            <cfthrow type="CannotDeleteItemTypeException" message="Cannot Delete Item Type" extendedinfo="#Trim(arguments.selectedItemTypesList)#" 
                            		detail="Item Type cannot be deleted at this time due to being in use in the system. Any such item type record can only be deactivated." />
                        </cfcase>
                        <!--- Unknown Case --->
                        <cfdefaultcase>
                        	<!--- Rethrow For Upper Level Processing --->
                        	<cfrethrow />
                        </cfdefaultcase>
                    </cfswitch>
                </cfcatch>
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
           </cftry>
        </cftransaction>
	</cffunction> 
    
    <!--- Get Item Type Record --->
    <cffunction name="getItemType" access="public" output="no" returntype="gears.studentfinancialaid.ItemType" hint="Get Item Type Record" description="Get Item Type Record">
		<!--- Input: Item Type Code to Fetch from Database --->
        <cfargument name="itemTypeCode" required="yes" hint="Item Type Code" />
        <!-------------- Test Cases ---------------
			1. Item Type Code is Invalid 
			2. No Matching Item Type Record Found
		------------------------------------------>  
        
		<!--- Cover Case 1: Throws IllegalItemTypeCodeException --->
        <cfif itemTypeValidator.isValidItemTypeCode(arguments.itemTypeCode)>
            <!--- Query DB --->
            <cfquery datasource="#variables.dataSource#" name="qItemType">
                SELECT itemTypeCode, description, fund, department, account, active
                FROM und_finaid.itemtypes
                WHERE itemTypeCode = #arguments.itemTypeCode#                    
            </cfquery>
            <cfif qItemType.recordCount eq 1>
                <!--- Create & Return Item Type Object --->
                <cfreturn CreateObject("component","gears.studentfinancialaid.ItemType")
                            .init(
                            	<!--- Item Type Code --->
                                itemTypeCode = qItemType.itemTypeCode[1],
                                <!--- Item Type Description --->
                                itemTypeDescription = qItemType.description[1],
                                <!--- Item Type Fund --->
                                fund = qItemType.fund[1], 
                                <!--- Item Type Description --->
                                department = qItemType.department[1],
                                <!--- Item Type Account --->
                                account = qItemType.account[1],
                                <!--- Item Type Active Flag --->
                                active = qItemType.active[1] eq 1
                            ) />
            <cfelse>
                <!--- Item Type Record Not Found --->
                <cfthrow type="ItemTypeRecordNotFoundException" message="No Item Type Record Found" 
                        detail="No Item Type Record Found for This Item Type Code" extendedinfo="#arguments.itemTypeCode#" />                                
            </cfif>                                
        </cfif>
	</cffunction>    
    
    <!--- Get Item Types As AutoComplete JSON --->
	<cffunction name="getItemTypesAsAutoCompleteJSON" access="public" output="no" returntype="string" hint="" description="">
		<!--- Input: Search Term --->
        <cfargument name="searchTerm" type="string" required="yes" hint="" />    
        <!--- Input: Active Flag --->
        <cfargument name="active" required="no" type="boolean" hint="" />
        <!---------- Test Cases ----------
			1. Search Term is Blank 
		--------------------------------->
        
        <cfif Trim(arguments.searchTerm) eq ''>
        	<!--- Case 1. : Return All Item Types --->
            <cfquery datasource="#variables.dataSource#" name="qItemTypes">
                SELECT itemTypeCode, description, fund, department, account  
                FROM und_finaid.itemTypes            
                <cfif StructKeyExists(arguments,'active')>
                    WHERE active = <cfif arguments.active>1<cfelse>0</cfif>
                </cfif>       
            </cfquery>
		<cfelse>
			<!--- Convert Search Term White Spaces -> Commas --->
            <cfset searchTermArray = ListToArray(REReplace(arguments.searchTerm,'\s',','),',') />
            <!--- Loop Through Array to Prepare for Query --->
            <cfloop index="i" from="1" to="#ArrayLen(searchTermArray)#">
            	<cfset searchTermArray[i] = '"%'&searchTermArray[i]&'%"' />
            </cfloop>
            
            <!--- Search Based on Terms --->
            <cfquery datasource="#variables.dataSource#" name="qItemTypes">
                SELECT itemTypeCode, description, fund, department, account
                FROM und_finaid.itemTypes            
                WHERE itemTypeCode LIKE #searchTermArray[1]# 
					OR description LIKE #searchTermArray[1]#				                     
                <cfloop index="i" from="2" to="#ArrayLen(searchTermArray)#">
                    OR itemTypeCode LIKE #searchTermArray[i]# 
                    OR itemTypeCode LIKE #searchTermArray[i]#                     
                </cfloop>
                <cfif StructKeyExists(arguments,'active')>
                    AND active = <cfif arguments.active>1<cfelse>0</cfif>
                </cfif>       
            </cfquery>
        </cfif>		
        	           
        <!--- Loop Query to Create Autocomplete Format Structure --->
		<cfset itemTypeResultsArray = ArrayNew(1) />
        <cfloop query="qItemTypes">
        	<cfset itemTypeResult = StructNew() />
			<cfset itemTypeResult['id']=itemTypeCode />
			<cfset itemTypeResult['desc']=description />
            <cfset itemTypeResult['acc']=account />
            <cfset itemTypeResult['dept']=department />
            <cfset itemTypeResult['fund']= fund />
			<cfset itemTypeResult['value']=itemTypeCode&' - '&description />                                                    	
            <cfset ArrayAppend(itemTypeResultsArray,itemTypeResult) />
        </cfloop>
        
        <cfreturn SerializeJSON(itemTypeResultsArray) />
	</cffunction>                
</cfcomponent>