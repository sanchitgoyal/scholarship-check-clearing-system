<!------------------------------- Component Documentation ----------------------------------------

	Summary of Exceptions Thrown By Methods
	
	1. CannotEditStudentRecordException
	2. CannotDeleteStudentRecordException
	3. InvalidStudentIDFormatException
		
------------------------------------------------------------------------------------------------------------->

<cfcomponent bindingname="StudentManager" displayname="StudentManager" hint="CFC To Interact With Student Records on DB" output="no">
	<!--- Constructor --->
    <cffunction name="init" access="public" hint="CFC Initializer" description="CFC Initializer" output="no" returntype="StudentManager">
		<!--- Input: Coldfusion Data Source --->
        <cfargument name="dataSource" required="yes" type="string" hint="ColdFusion Datasource" />
        <!--- Set Data Source Reference --->
		<cfset variables.dataSource = dataSource /> 
        <!--- Instantiate Spec Validator --->
        <cfset variables.studentSpecValidator = CreateObject("component","gears.studentfinancialaid.specs.StudentSpec") />
        <!--- Return This Instance --->
        <cfreturn this />
    </cffunction> 
    
    <!--- Create Student Record --->
    <cffunction name="createStudentRecord" access="public" output="no" returntype="void" hint="Create Student Record" description="Create Student Record ">
		<!--- Input: Student Record Object --->
        <cfargument name="studentRecord" type="gears.studentfinancialaid.Student" required="yes" hint="Student Record Object" />
    	<!--- Test Cases ---
			1. None 
		------------------->
        <cftransaction action="begin" isolation="serializable">
        	<cftry>
            	<!--- Execute DB Changes --->
                <cfquery result="iStudentRecord" datasource="#variables.dataSource#">
                    INSERT INTO und_finaid.student(emplID,firstName,middleName,lastName,active)
                    VALUES (
							<!--- Empl ID --->
                            <cfif Trim(studentRecord.getEmplID()) neq ''><cfqueryparam cfsqltype="cf_sql_varchar" value="#studentRecord.getEmplID()#" /><cfelse>NULL</cfif>,
                            <!--- First Name --->
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#studentRecord.getFirstName()#" />,
                            <!--- Middle Name --->
                            <cfif Trim(studentRecord.getMiddleName()) neq ''><cfqueryparam cfsqltype="cf_sql_varchar" value="#studentRecord.getMiddleName()#" /><cfelse>NULL</cfif>,
                            <!--- Last Name --->
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#studentRecord.getLastName()#" />,
                            <!--- Active Flag --->
                            <cfif studentRecord.isActive()>1<cfelse>0</cfif>
                   	)
                </cfquery>
                
                <!--- Catch & Process Exceptions --->
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
           </cftry>
        </cftransaction>
    </cffunction>
    
    <!--- Import Student Record --->
    <cffunction name="importStudentRecords" access="public" output="no" returntype="struct" hint="Import Student Records" description="Import Student Records">
		<cfargument name="importFile" type="string" required="yes" hint="Student Record Import File Full Path Name" />
		<!--------------- Test Cases -----------------
			1. Invalid file path 
			2. File Not Found 
			3. File Not Excel File (.xls/.xlsx)
			4. Excel File not in Proper Format 
		--------------------------------------------->
        
        <!--- Initialize Import Result --->
		<cfset importResult = {updated=0,inserted=0} />
        
        <!--- Cover Case 1 & 2 --->
        <cfif NOT FileExists(arguments.importFile)>
        	<!--- Throw Exception --->
        	<cfthrow type="FileNotFoundException" message="Invalid File / File Not Found" 
            		detail="File path does not point to a valid file" extendedinfo="#arguments.importFile#" />
        <!--- Cover Case 3 --->            
        <cfelseif NOT isSpreadsheetFile(importFile)>
        	<!--- Throw Exception --->
        	<cfthrow type="IllegalImportFileFormatException" message="Not an Acceptable Type for Import File" 
            		detail="Student record import file needs to be an excel file (.xls/.xlsx)" extendedinfo="#arguments.importFile#" />       
        <cfelse>
        	<cftry>            
                <!--- Read Spreadsheet --->
 	       		<cfspreadsheet action="read" src="#importFile#" query="studentData" headerrow="1" />

				<!--- Cover Case 4 --->
                <cfset spreadSheetColumns = studentData.columnList />
                <cfif ListLen(spreadSheetColumns) neq 4 or 
					ListFind(spreadSheetColumns,'ID') eq 0 or 
					ListFind(spreadSheetColumns,'LASTNAME') eq 0 or 
					ListFind(spreadSheetColumns,'FIRSTNAME') eq 0 or 
					ListFind(spreadSheetColumns,'MIDDLENAME') eq 0>
                    <!--- Throw Exception --->
                    <cfthrow type="IllegalImportFileFormatException" message="Illegal Import File Format Error"
                            detail="Student data import file must have following four columns - ID,FirstName,MiddleName,LastName in order" extendedinfo="#spreadSheetColumns#" />        
				</cfif>		
                
                <!--- Compile Reference for Matching Existing Records --->
                <cfquery name="matchingStudentRecords" datasource="#variables.dataSource#">
                	SELECT emplID as ID, firstName as FirstName, lastName as LastName, middleName as MiddleName 
                    FROM und_finaid.student
                    WHERE emplID IN (<cfqueryparam list="yes" cfsqltype="cf_sql_varchar" value="#ValueList(studentData.ID,',')#" />)
                </cfquery>
                
                <!--- Collate Update Set --->                 
                <cfquery name="qUpdationRecords" dbtype="query">
                	SELECT studentData.ID, studentData.FirstName, studentData.LastName, studentData.MiddleName, 
                    	matchingStudentRecords.FirstName as FName, matchingStudentRecords.LastName as LName, matchingStudentRecords.MiddleName as MName
                    FROM studentData, matchingStudentRecords
                    WHERE matchingStudentRecords.ID = studentData.ID
                </cfquery>
                
                <!--- Collate Insert Set --->                
                <cfquery name="qInsertionRecords" dbtype="query">
                	SELECT studentData.ID, studentData.FirstName, studentData.LastName, studentData.MiddleName
                    FROM studentData
                    WHERE studentData.ID NOT IN (<cfqueryparam list="yes" cfsqltype="cf_sql_varchar" value="#ValueList(matchingStudentRecords.ID,',')#" />)
                    AND studentData.ID <> 'ID'
                </cfquery>
				
                <!--- Make DB Changes --->
                <cftransaction action="begin" isolation="serializable">
                	<!--- Update Existing Records --->
                    <cfloop query="qUpdationRecords">
                    	<!--- Update only changed records --->
						<cfif FirstName neq Fname or MiddleName neq MName or LastName neq Lname>
							<!--- Match & Change --->
                            <cfquery name="uStudentRecord" result="updationResult" datasource="#variables.dataSource#">                                        			
                                    UPDATE und_finaid.student
                                    SET emplID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#ID#" />,
                                    firstName = <cfqueryparam cfsqltype="cf_sql_varchar" value="#FirstName#" />,
                                    lastName = <cfqueryparam cfsqltype="cf_sql_varchar" value="#LastName#" />,
                                    middleName = <cfqueryparam cfsqltype="cf_sql_varchar" value="#MiddleName#" />
                                    WHERE emplID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#ID#" />
                            </cfquery>
							
							<!--- Count Updated Records --->
                            <cfset importResult.updated = importResult.updated + 1 />
						</cfif>                            
					</cfloop>
                    
                    <!--- Insert New Records --->
                    <cfif qInsertionRecords.recordCount gt 0>
						<!--- Insert Record in DB --->
                        <cfquery name="iStudentRecord" result="insertionResult" datasource="#variables.dataSource#">
                            INSERT INTO und_finaid.student (emplID,firstName,lastName,middleName)
                            Values
                            <cfset counter = 1 />
                            <cfloop query="qInsertionRecords">
                            	(
                                	<cfqueryparam cfsqltype="cf_sql_varchar" value="#ID#" />,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#FirstName#" />,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LastName#" />,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#MiddleName#" />
                                )
								<cfif counter lt qInsertionRecords.recordCount>,<cfset counter = counter + 1 /></cfif>
                            </cfloop>
                        </cfquery>
                                                
						<!--- Update Insertion Count --->
                        <cfset importResult.inserted = ListLen(insertionResult.GeneratedKey,",") />
                	</cfif>        
				</cftransaction>

                <!--- Catch & Process Exceptions --->
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
            </cftry>                 	 
        </cfif>  
        <!--- Return Import Result --->
        <cfreturn importResult />     
    </cffunction>                        

    <!--- Export Student Records --->
    <cffunction name="exportStudentRecords" access="public" output="yes" returntype="any" hint="Export Student Records" description="Export Student Records">
    	<!--- Test Cases ---
			1. None 
		-------------------->	   
        
        <!--- Query All Records --->
        <cfquery name="qStudentRecords" datasource="#variables.dataSource#">
            SELECT
                `student`.`idStudent`,
                `student`.`emplID`,
                `student`.`firstName`,
                `student`.`middleName`,
                `student`.`lastName`,
                `student`.`active`
            FROM `und_finaid`.`student`;
        </cfquery>
        
        <!--- Create Spreadsheet Object --->
		<cfset exportSheet = SpreadsheetNew("Student Records") />
        <!--- Add Header Row --->
		<cfset SpreadSheetAddRow(exportSheet, 'Record ID,Empl ID,First Name,Middle Name,Last Name,Active') />
        <!--- Add Record Rows --->
		<cfset spreadSheetAddRows(exportSheet, qStudentRecords) />
		<!--- Format Record Rows --->
        <cfloop query="qStudentRecords">
			<cfset spreadSheetSetCellFormula(exportSheet, """#emplID#""", qStudentRecords.currentRow+1, 2) />         
        </cfloop>
        
        <!--- Return SpreadSheet --->
        <cfreturn exportSheet />
    </cffunction>
    
    <!--- Edit Student Record --->
    <cffunction name="editStudentRecord" access="public" output="no" returntype="void" hint="Edit Student Record" description="Edit Student Record">
		<!--- Input: Student Record Object --->
        <cfargument name="studentRecord" type="gears.studentfinancialaid.Student" required="yes" hint="Student Record Object" />
        <!----------------------------------------- Test Cases -------------------=------------------------------------
			1. Student ID is not set in the Student Record Object  (Invalid Student Record ID Reference for Editing) 
		--------------------------------------------------------------------------------------------------------------->
        
        <!--- Cover Case 1 --->
        <cfif studentRecord.getStudentID() eq 0>
        	<!--- Throw Exception --->
        	<cfthrow type="CannotEditStudentRecordException" message="Cannot Edit Student Record!" 
            		detail="Could not find a valid student record ID on the student record for saving it to database." 
            		extendedinfo="No reference to student ID found in the record to lookup the database for saving changes." /> 
        </cfif>
        		
    	<cftransaction action="begin" isolation="serializable">
        	<cftry>
            	<!--- Execute DB Changes --->
                <cfquery result="iStudentRecord" datasource="#variables.dataSource#">
                    UPDATE und_finaid.student  
                    SET 
                    	<!--- Empl ID --->
                    	emplID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#studentRecord.getEmplID()#" />,
                        <!--- First Name --->
                        firstName = <cfqueryparam cfsqltype="cf_sql_varchar" value="#studentRecord.getFirstName()#" />,
                        <!--- Middle Name --->
                        middleName = <cfqueryparam cfsqltype="cf_sql_varchar" value="#studentRecord.getMiddleName()#" />,
                        <!--- Last Name --->
                        lastName = <cfqueryparam cfsqltype="cf_sql_varchar" value="#studentRecord.getLastName()#" />,
                        <!--- Active Flag --->
                        active = <cfif studentRecord.isActive()>1<cfelse>0</cfif>
                    WHERE     
                    	idStudent = <cfqueryparam cfsqltype="cf_sql_varchar" value="#studentRecord.getStudentID()#" />  
                </cfquery>
                                
                <!--- Catch & Process Exceptions --->
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
           </cftry>
        </cftransaction>
    </cffunction>
    
    <!--- Delete Student Record --->
    <cffunction name="deleteStudentRecord" access="public" output="no" returntype="void" hint="Delete Student Record(s)" description="Delete Student Record(s)">
		<!--- Input: Comma delimited list of Student IDs --->
        <cfargument name="studentIDList" type="string" required="yes" hint="List of Student IDs" />        
		<!------------------------------ Test Cases --------------------------------------
			1. DB Exception 									--> Rollback & Rethrow
			2. studentIDList is blank 							--> Throw Exception
			3. studentIDList contains an invalid Student ID		--> Throw Exception
		---------------------------------------------------------------------------------->
        
        <!--- Cover Case 2 --->
        <cfif Trim(arguments.studentIDList) eq ''>
            <!--- Throw Exception --->
            <cfthrow type="IllegalStudentRecordIDFormatException" message="Missing Student Record Selection" 
            		detail="No Student Record selected for Deletion" extendedinfo="#arguments.studentIDList#" />         
        </cfif>		
        	
      	<!--- Loop Through List --->
        <cfset listCount = 0 />
        <cfloop list="#arguments.studentIDList#" index="id" delimiters=",">
        	<!--- Cover Case 3 --->
        	<cfif variables.studentSpecValidator.isValidStudentID(id)><!--- Throws IllegalStudentRecordIDFormatException ---></cfif>
            <cfset listCount = listCount+1 />
        </cfloop>

    	<cftransaction action="begin" isolation="serializable">
        	<cftry>
            	<!--- Execute DB Changes --->
                <cfquery result="iStudentRecord" datasource="#variables.dataSource#">
                    DELETE FROM und_finaid.student 
                    WHERE  idStudent IN (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#studentIDList#" />)
                </cfquery>
                
                <!--- Catch & Process Exceptions --->
                
                <!---- Database Exceptions --->
                <cfcatch type="database">
                    <!--- Roll Back Transaction --->
                    <cftransaction action="rollback" isolation="serializable" />
                    <!--- MySQL Dependent Processing --->
                    <cfswitch expression="#cfcatch.NativeErrorCode#">
						<!--- Foreign Key Constraint Violation --->
                        <cfcase value="1451">
                        	<cfthrow type="CannotDeleteStudentRecordException" message="Cannot Delete Student Record" extendedinfo="#studentIDList#" 
                            detail="Student record(s) cannot be deleted at this time due to being in use in the system. Any such student record(s) can only be deactivated." />
                        </cfcase>
                        
                        <!--- Unknown Native Error Code --->
                        <cfdefaultcase>
							<!--- Rollback Transaction --->
                            <cftransaction action="rollback" isolation="serializable" />
                            <!--- Rethrow For Upper Level Processing --->
                            <cfrethrow />
                        </cfdefaultcase>
                    </cfswitch>
                </cfcatch>
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
           </cftry>
        </cftransaction>
    </cffunction>
    
    <!--- Merge Student Records --->
    <cffunction name="mergeStudentRecords" access="public" output="no" returntype="void" hint="Merge Student Records" description="Merge Student Records">
		<!--- Input: Comma delimited list of 2 Student IDs --->
        <cfargument name="studentIDList" type="string" required="yes" hint="List of Student IDs" />        
		<!---------------- Test Cases --------------------------
			1. studentIDList doesnt have exactly 2 items
			2. studentIDList contains an invalid Student ID
			3. Any student ID in the list is not present in the DB
			4. Both students have non blank empl id fields 
			5. Both students have blank empl id fields
			6. First Name or Last Name is not a match between both students
		-------------------------------------------------------->
		<cfset studentsToMerge = ListToArray(arguments.studentIDList,',',false) />
		<cfset studentsToMergeProperties = ArrayNew(2) />
        
		<!--- Cover Case 1 --->
		<cfif ArrayLen(studentsToMerge) neq 2>
        	<!--- Throw Exception --->
            <cfthrow type="CannotMergeStudentRecordsException" message="Cannot Merge Student Records!" 
            		detail="Please select exactly 2 Student records (With & Without Empl ID) values to perform merge operation." extendedinfo="#arguments.studentIDList#" />
        <cfelse>
        	<!--- Covers Case 2 --->
        	<cfloop index="i" from="1" to="2">
            	<!--- Throws IllegalStudentRecordIDFormatException --->
            	<cfif variables.studentSpecValidator.isValidStudentID(studentsToMerge[i])>
                	<cfquery name="qEmplID" datasource="#variables.dataSource#">
                    	SELECT emplID,firstName,lastName 
                        FROM und_finaid.student
                        WHERE idStudent = #studentsToMerge[i]#
                    </cfquery>
                    <!--- Cover Case 3 --->
                    <cfif qEmplID.recordCount neq 1>
                    	<!--- Throw Exception --->
                        <cfthrow type="CannotMergeStudentRecordsException" message="Cannot Merge Student Records!" 
                                detail="Selected Student Record not found in the Data Base. Therefore cannot merge the student records." extendedinfo="#studentsToMerge[i]#" />
                    <cfelse>
                    	<cfset studentsToMergeProprties[i][1] = qEmplID.emplID[1] /> 
                        <cfset studentsToMergeProprties[i][2] = qEmplID.firstName[1] /> 
                        <cfset studentsToMergeProprties[i][3] = qEmplID.lastName[1] /> 
                    </cfif>
                </cfif>
            </cfloop>
            <!--- Cover Case 4,5 & 6 --->
            <cfif (NOT(Trim(studentsToMergeProprties[1][1]) eq '' xor Trim(studentsToMergeProprties[2][1]) eq '')) 
					or Trim(studentsToMergeProprties[1][2]) neq Trim(studentsToMergeProprties[2][2]) 
					or Trim(studentsToMergeProprties[1][3]) neq Trim(studentsToMergeProprties[2][3])>
                <!--- Throw Exception --->    
                <cfthrow type="CannotMergeStudentRecordsException" message="Cannot Merge Student Records!" 
                        detail="Exactly one of the selected student records should have a blank EmplID and both records should have identical first & last names." 
                        extendedinfo="#arguments.studentIDList#" />
            <cfelse>
            	<!--- Execute Merging --->
            	<cftransaction action="begin" isolation="serializable">
                	<cftry>
                    	<!--- Decide Old & New Record for Merging --->
						<cfset oldRecordID =  studentsToMerge[2] />
                        <cfset newRecordID =  studentsToMerge[1] />
                        <cfif Trim(studentsToMergeProprties[1][1]) eq ''>
                            <cfset oldRecordID =  studentsToMerge[1] />
                            <cfset newRecordID =  studentsToMerge[2] />
                        </cfif>
                    	<!--- Update Disbursements --->
                        <cfquery name="uDisbursements" datasource="#variables.dataSource#">
                            update und_finaid.checkdisbursement
                            SET idStudent = #newRecordID#
                            WHERE idStudent = #oldRecordID#
                        </cfquery>
                        <!--- Delete Old Record --->
                        <cfquery name="dOldRecord" datasource="#variables.dataSource#">
                            delete from und_finaid.student
                            WHERE idStudent = #oldRecordID# 
                        </cfquery>         
                
						<!--- Unknown Exceptions --->
                        <cfcatch type="any"> 
                            <!--- Rollback Transaction --->
                            <cftransaction action="rollback" isolation="serializable" />
                            <!--- Rethrow For Upper Level Processing --->
                            <cfrethrow />
                        </cfcatch>
                    </cftry>                   
				</cftransaction>    
            </cfif>
        </cfif>
	</cffunction>        
    
    <!--- Get Student Records As AutoComplete JSON --->
	<cffunction name="getStudentRecordsAsAutoCompleteJSON" access="public" output="no" returntype="string" 
    			hint="Searches & Returns JSON of Student Records" description="Searches & Returns JSON of Student Records">
		<!--- Input: Search Term --->
        <cfargument name="searchTerm" type="string" required="yes" hint="Search Term" />   
        <!--- Input: Fetch Active/Inactive Records ? --->
        <cfargument name="active" type="boolean" required="no" hint="Active/Inactive Records ?"/>
        <!--- Input: Record Limit --->
        <cfargument name="recordLimit" type="numeric" required="no" default="100" hint="Limit on number of top records to fetch" />
		
		<!-------- Test Cases -----------
			1. Search Term is empty 
		-------------------------------->	
        
        <!--- Var Scoping --->
		<cfset var searchTermArray = ArrayNew(1)  />
        <cfset var numericSearchTerms = ArrayNew(1) />
        <cfset var nonNumericSearchTerms = ArrayNew(1)  />
        
        <cfif Trim(arguments.searchTerm) eq ''>
        	 <!--- Case 1. : Return All Item Types --->
            <cfquery datasource="#variables.dataSource#" name="qSearchStudentRecords">
                SELECT idStudent, emplID, lastName, firstName
                FROM und_finaid.student
                <cfif StructKeyExists(arguments,'active')>
                	WHERE active = <cfif arguments.active>1<cfelse>0</cfif>
                </cfif>
                LIMIT 0, #arguments.recordLimit#            
            </cfquery>
		<cfelse>
        	<!--- Process Search Terms --->
            
			<!--- Convert White Spaces To Commas --->
            <cfset searchTermArray = ListToArray(REReplace(arguments.searchTerm,'\s',','),',') />

            <!--- Loop Through Array to Prepare for Query --->
            <cfloop index="i" from="1" to="#ArrayLen(searchTermArray)#">
                <!--- Separate Numeric from Non Numeric Search Terms --->
                <cfif IsNumeric(searchTermArray[i])>
                	<cfset ArrayAppend(numericSearchTerms, searchTermArray[i]) />
                <cfelse>
                	<cfset ArrayAppend(nonNumericSearchTerms, searchTermArray[i]) />
                </cfif>    
            </cfloop>

            <!--- Search & Query Student Records --->
            <cfquery datasource="#variables.dataSource#" name="qSearchStudentRecords">
                SELECT * FROM
                (
                    SELECT idStudent, emplID, lastName, firstName, active
                    FROM und_finaid.student    
                    WHERE 
                    <cfif ArrayLen(numericSearchTerms)>
                        emplID IN (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#ArrayToList(numericSearchTerms)#" />) OR 
                    </cfif>
                    <cfif ArrayLen(nonNumericSearchTerms)>
                        lastName IN (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#ArrayToList(nonNumericSearchTerms)#" />) OR 
                        firstName IN (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#ArrayToList(nonNumericSearchTerms)#" />) OR
                    </cfif>
                    FALSE  
    
                    UNION 
                    
                    SELECT idStudent, emplID, lastName, firstName, active
                    FROM und_finaid.student
                    WHERE    
                    <cfif ArrayLen(numericSearchTerms)>
                        emplID REGEXP (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#ArrayToList(numericSearchTerms,'|')#" />) OR 
                    </cfif>
                    <cfif ArrayLen(nonNumericSearchTerms)>
                        lastName REGEXP (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#ArrayToList(nonNumericSearchTerms,'|')#" />) AND 
                        firstName REGEXP (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#ArrayToList(nonNumericSearchTerms,'|')#" />) OR
                    </cfif>
                    FALSE  
    
                    UNION 
                    
                    SELECT idStudent, emplID, lastName, firstName, active
                    FROM und_finaid.student   
                    WHERE 
                    <cfif ArrayLen(numericSearchTerms)>
                        emplID REGEXP (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#ArrayToList(numericSearchTerms,'|')#" />) OR 
                    </cfif>
                    <cfif ArrayLen(nonNumericSearchTerms)>
                        lastName REGEXP (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#ArrayToList(nonNumericSearchTerms,'|')#" />) XOR 
                        firstName REGEXP (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#ArrayToList(nonNumericSearchTerms,'|')#" />) OR
                    </cfif>
                    FALSE  
                ) suggestionList
                <cfif StructKeyExists(arguments,'active')>
                	WHERE active = <cfif arguments.active>1<cfelse>0</cfif>
                </cfif>
                LIMIT 0, #arguments.recordLimit#        
            </cfquery>
        </cfif>
        
        <!--- Loop Query to Create Autocomplete Format Structure --->
		<cfset studentArray = ArrayNew(1) />
        <cfloop query="qSearchStudentRecords">
        	<cfset studentStruct = StructNew() />
			<cfset studentStruct['id']=' '&emplID />
			<cfset studentStruct['recordID']=idStudent />
            <cfset studentStruct['firstname']=firstName />
            <cfset studentStruct['lastname']=lastName />
            <cfset studentStruct['value']=lastName&', '&firstName/>                                	
            <cfset ArrayAppend(studentArray,studentStruct) />
        </cfloop>
        
        <cfreturn SerializeJSON(studentArray) />
	</cffunction>                
    
</cfcomponent>