<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. DuplicateAcademicTermParameterException
		2. IllegalTermCodeException
		3. CannotDeleteAcademicTermException	
	
---------------------------------------------------------------------------------------------------->

<cfcomponent displayname="AcademicTermManager" bindingname="AcademicTermManager" hint="Academic Term Manager CFC" output="no">
	<!--- Constructor --->
    <cffunction name="init" access="public" output="no" returntype="AcademicTermManager" hint="CFC Initializer" description="CFC Initializer">
    	<!--- Input: ColdFusion Data Source --->
    	<cfargument name="dataSource" type="string" required="yes" />
		<!--- Set Data Source Reference --->
        <cfset variables.dataSource = arguments.dataSource />
		<!--- Instantiate Academic Term Spec --->
		<cfset variables.academicTermValidator = CreateObject("component","gears.studentfinancialaid.specs.AcademicTermSpec") />  
        <!--- Return This Instance --->
        <cfreturn this />                                 
    </cffunction>  
    
	<!--- Create Academic Term Record --->        		        
    <cffunction name="createAcademicTerm" access="public" output="no" returntype="void" hint="Create Academic Term Record in DB" description="Create Academic Term Record in DB">
        <!--- Input: Academic Term Object --->
        <cfargument name="termToCreate" required="yes" type="gears.studentfinancialaid.AcademicTerm" hint="Academic Term CFC object" />    
		<!------------------------------- Test Cases --------------------------------------
			1. Inserted Term Code Could Be Same as an Existing Term Code in Database
		----------------------------------------------------------------------------------->
        <cftransaction action="begin" isolation="serializable">
        	<cftry>
            	<!--- Execute DB Changes --->
                <cfquery name="iAcademicTerm" datasource="#variables.dataSource#">
                    INSERT INTO und_finaid.academicterm
                    VALUES	(
                                <!--- Term Code --->
                                '#arguments.termToCreate.getTermCode()#',
                                <!--- Semester --->
                                '#arguments.termToCreate.getSemester()#',
                                <!--- Academic Year --->
                                '#arguments.termToCreate.getAcademicYear()#',
                                <!--- Begin Date --->
                                '#DateFormat(arguments.termToCreate.getBeginDate(),"yyyy-mm-dd")#',
                                <!--- End Date --->
                                '#DateFormat(arguments.termToCreate.getEndDate(),"yyyy-mm-dd")#',
                                <!--- Is Active? --->
                                <cfif arguments.termToCreate.isActive()>1<cfelse>0</cfif>
                            )
                </cfquery>
                
                <!--- Catch & Process Exceptions --->
                
                <!---- Database Exceptions --->
                <cfcatch type="database">
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- MySQL Dependent Code --->
                    <cfswitch expression="#cfcatch.NativeErrorCode#">
                    	<!--- Duplicate Item Code Found Case --->
                    	<cfcase value="1062">
                        	<!--- Throw Exception : Duplicate Academic Term --->
                        	<cfthrow type="DuplicateAcademicTermParameterException" message="Academic Term Code Cannot be Duplicated" 
                                    detail="Academic Term Code Uniquely Identifies an Academic Term In The System" extendedinfo="#arguments.termToCreate.getTermCode()#" />
						</cfcase>
                        <!--- Unknown Case --->
                        <cfdefaultcase>
                        	<!--- Rethrow For Upper Level Processing --->
                        	<cfrethrow />
                        </cfdefaultcase>
                    </cfswitch>
                </cfcatch>
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
			</cftry>
		</cftransaction>        
	</cffunction>  

    <!--- Edit Academic Term Record --->
    <cffunction name="editAcademicTerm" access="public" output="yes" returntype="void" hint="Edit Academic Term Record" description="Edit Academic Term Record in DB">        
		<!--- Input: Term Code to Edit --->
        <cfargument name="termCodeReference" type="string" required="yes" hint="TermCode Reference of the Academic Term in the Database" />
        <!--- Input: Term Data to Replace With --->
        <cfargument name="termObject" type="gears.studentfinancialaid.AcademicTerm" required="yes" hint="Academic Term CFC Object" />
        <!------------------------------------------- Test Cases ----------------------------------------------------
			1. termCodeReference is not a valid Academic Term Code 
			2. New Term Code of Academic Term Could Be Same as another Existing Academic Term's Code in Database
		------------------------------------------------------------------------------------------------------------->
        <cftransaction action="begin" isolation="serializable">
            <cftry>
                <!--- Cover Test Case 1 --->
                <cfif variables.academicTermValidator.isValidTermCode(arguments.termCodeReference)>
					<!--- Execute DB Changes --->
                    <cfquery result="uTerms" datasource="#variables.dataSource#">
                        UPDATE und_finaid.academicterm 				            	
                        SET 
                        	<!--- Term Code --->
                            termCode = '#arguments.termObject.getTermCode()#', 
                            <!--- Semester --->
                            semester = '#arguments.termObject.getSemester()#', 
                            <!--- Academic Year --->
                            academicYear = '#arguments.termObject.getAcademicYear()#', 
                            <!--- Begin Date --->
                            beginDate = '#DateFormat(arguments.termObject.getBeginDate(),"yyyy-mm-dd")#', 	
                            <!--- End Date --->
                            endDate = '#DateFormat(arguments.termObject.getEndDate(),"yyyy-mm-dd")#', 
                            <!--- Is Active --->
                            active = <cfif arguments.termObject.isActive()>1<cfelse>0</cfif>                        
                        WHERE termCode = '#arguments.termCodeReference#'
                    </cfquery>
                </cfif>
                
                <!--- Catch & Process Exceptions --->
                
                <!---- Database Exceptions --->
                <cfcatch type="database">
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- MySQL Dependent Code --->
                    <cfswitch expression="#cfcatch.NativeErrorCode#">
                    	<!--- Duplicate Item Code Found Case --->
                    	<cfcase value="1062">
                        	<!--- Throw Exception : Duplicate Academic Term --->
                        	<cfthrow type="DuplicateAcademicTermParameterException" message="Academic Term Code Cannot be Duplicated" 
                                    detail="Academic Term Code Uniquely Identifies an Academic Term In The System" extendedinfo="#arguments.termObject.getTermCode()#" />
						</cfcase>
                        <!--- Unknown Case --->
                        <cfdefaultcase>
                        	<!--- Rethrow For Upper Level Processing --->
                        	<cfrethrow />
                        </cfdefaultcase>
                    </cfswitch>
                </cfcatch>
                
                <!--- Illegal Term Code Exception --->
                <cfcatch type="IllegalTermCodeException">
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
            </cftry>              
        </cftransaction>
	</cffunction>

    <!--- Activate/Disable Academic Term Record(s) --->
    <cffunction name="setAcademicTermActive" access="public" output="no" returntype="void" hint="Activate/Deactivate Academic Term(s)" description="Activate/Deactivate Academic Term(s)">
        <!--- Input: List of Term Codes To Activate/Disable --->
        <cfargument name="termCodes" required="yes" type="string" hint="Comma Delimited List of Academic Term Codes" />
        <!--- Input: Flag for Activation/DeActivation --->
        <cfargument name="active" required="yes" type="boolean" hint="Boolean Flag Value for Activation/Deactivation" />
        <!---------------------------------------------------Test Cases-----------------------------------------------------
			1. termCodes is Empty String									-->	Throw Exception 
			2. termCodes contains any Invalid Term Code						--> Throw Exception 
			3. termCodes contains a term code which is not in DB			-->	Ignore
			4. DB Exception													-->	Rethrow 
		-------------------------------------------------------------------------------------------------------------------->

		<!--- Convert List to Array for easier handelling--->        
		<cfset termCodeArray = ListToArray('#Trim(arguments.termCodes)#',',') />
        
		<!--- Handle Test Case 1 --->
        <cfif ArrayLen(termCodeArray) eq 0>
            <cfthrow type="IllegalTermCodeException" message="Missing Academic Term Selection" 
            		detail="No Academic Term selected for Activation/Deactivation" extendedinfo="#arguments.termCodes#" />
		</cfif>                  

		<!--- Loop Through All Given Term Codes --->
        <cfloop index="i" from="1" to="#ArrayLen(termCodeArray)#">
        	<!--- Handle Test Case 2 --->
    		<cfif variables.academicTermValidator.isValidTermCode(termCodeArray[i])>
				<!--- Prepare Each Term Code for DB Operation --->                
                <cfset termCodeArray[i] = "'"&termCodeArray[i]&"'" />
            </cfif>        	            
        </cfloop>
                
        <cftransaction action="begin" isolation="serializable">
            <cftry>
				<!--- Execute DB Changes --->
                <cfquery datasource="#variables.dataSource#" name="uActivation" >
                    UPDATE und_finaid.academicterm
                    <!--- Active Flag --->
                    SET active = <cfif active>1<cfelse>0</cfif>
                    <cfif ArrayLen(termCodeArray) eq 1>
                        WHERE termCode = '#Trim(arguments.termCodes)#' 
                    <cfelse>
                        WHERE termCode IN (#ArrayToList(termCodeArray,',')#)
                    </cfif>
                </cfquery>
                
                <!--- Catch & Process Exceptions --->
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
        	</cftry>
		</cftransaction>         	
	</cffunction>

	<!--- Delete Academic Term Record(s) --->    
    <cffunction name="deleteAcademicTerm" access="public" output="no" returntype="void" hint="Delete Academic Term Record(s)" description="Delete Academic Term Record(s)">
    	<!--- Input: List of Term Codes To Delete --->
        <cfargument name="termCodes" required="yes" type="string" hint="Comma Delimited Term Code List" />
        <!------------------------------------------------------ Test Cases ---------------------------------------------------------
			1. termCodes is Empty String											-->	Throw Exception
			2. termCodes contains any Invalid Term Code								--> Throw Exception 
			4. termCodes contains term not in DB									-->	Ignore 
			5. termCodes contains code(s) in use in the system	(Cannot be Deleted)	-->	Throw Exception & Dont Update Any 
		---------------------------------------------------------------------------------------------------------------------------->

		<!--- Convert List to Array for easier handelling--->        
		<cfset termCodeArray = ListToArray('#Trim(arguments.termCodes)#',',') />
        
		<!--- Handle Test Case 1 --->
        <cfif ArrayLen(termCodeArray) eq 0>
            <cfthrow type="IllegalTermCodeException" message="Missing Academic Term Selection" 
            		detail="No Academic Term selected for Deletion" extendedinfo="#arguments.termCodes#" />
		</cfif>                  
        
		<!--- Loop Through All Given Term Codes --->
        <cfloop index="i" from="1" to="#ArrayLen(termCodeArray)#">
        	<!--- Handle Test Case 2 --->
    		<cfif variables.academicTermValidator.isValidTermCode(termCodeArray[i])>
				<!--- Prepare Each Term Code for DB Operation --->                
                <cfset termCodeArray[i] = "'"&termCodeArray[i]&"'" />
            </cfif>        	            
        </cfloop>

        <cftransaction action="begin" isolation="serializable">
            <cftry>
                <!--- Execute DB Changes --->
                <cfquery name="dAcademicTerms" datasource="#variables.dataSource#">
                    DELETE FROM und_finaid.academicterm
                    <cfif ArrayLen(termCodeArray) eq 1>
                        WHERE termCode = '#Trim(arguments.termCodes)#' 
                    <cfelse>
                        WHERE termCode IN (#ArrayToList(termCodeArray,',')#)
                    </cfif>
                </cfquery>
                
                <!--- Catch & Process Exceptions --->
                
                <!---- Database Exceptions --->
                <cfcatch type="database">
                    <!--- Roll Back Transaction --->
                    <cftransaction action="rollback" isolation="serializable" />
                    <!--- MySQL Dependent Processing --->
                    <cfswitch expression="#cfcatch.NativeErrorCode#">
						<!--- Foreign Key Constraint Violation --->
                        <cfcase value="1451">
                            <cfthrow type="CannotDeleteAcademicTermException" message="Cannot Delete Academic Term" 
                            		extendedinfo="#ArrayToList(termCodeArray,',')#" 
                            		detail="Academic Term cannot be deleted at this time due to being in use in the system. Any such term record can only be deactivated." />
                        </cfcase>
                        <!--- Unknown Native Error Code --->
                        <cfdefaultcase>
							<!--- Rollback Transaction --->
                            <cftransaction action="rollback" isolation="serializable" />
                            <!--- Rethrow For Upper Level Processing --->
                            <cfrethrow />
                        </cfdefaultcase>
                    </cfswitch>
                </cfcatch>
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
            </cftry>
        </cftransaction>        
    </cffunction>  
    
    <!--- Get Item Types As AutoComplete JSON --->
	<cffunction name="getAcademicTermsAsAutoCompleteJSON" access="public" output="no" returntype="string" 
    			hint="Searches & Returns JSON of Academic Terms" description="Searches & Returns JSON of Academic Terms">
		<!--- Input: Academic Term Search Term --->
		<cfargument name="searchTerm" type="string" required="yes" hint="" />    
        <!--- Input: Active Flag --->
        <cfargument name="active" required="no" type="boolean" hint="" />
        <!---------- Test Cases ----------
			1. Search Term is Blank 
		--------------------------------->
        
        <cfif Trim(arguments.searchTerm) eq ''>
			<!--- Case 1. : Return All Existing Academic Terms in DB --->
            <cfquery datasource="#variables.dataSource#" name="qAcademicTerm">
                SELECT termCode,semester,academicYear
                FROM und_finaid.academicterm            
                <cfif StructKeyExists(arguments,'active')>
                    WHERE active = <cfif arguments.active>1<cfelse>0</cfif>
                </cfif>       
            </cfquery>
		<cfelse>
			<!--- Convert Search Term White Spaces into Commas --->
            <cfset searchTermArray = ListToArray(REReplace(arguments.searchTerm,'\s',','),',') />
            <!--- Loop Through Array to Prepare for Query --->
            <cfloop index="i" from="1" to="#ArrayLen(searchTermArray)#">
            	<cfset searchTermArray[i] = '"%'&searchTermArray[i]&'%"' />
            </cfloop>
            
            <!--- Search Academic Term Records --->
            <cfquery datasource="#variables.dataSource#" name="qAcademicTerm">
                SELECT termCode,semester,academicYear
                FROM und_finaid.academicterm            
                <!--- Compare search terms ONLY against term codes --->
                WHERE termCode LIKE #searchTermArray[1]# 
                <cfloop index="i" from="2" to="#ArrayLen(searchTermArray)#">
                    OR termCode LIKE #searchTermArray[i]# 
                </cfloop>
                <cfif StructKeyExists(arguments,'active')>
                    AND active = <cfif arguments.active>1<cfelse>0</cfif>
                </cfif>       
            </cfquery>
        </cfif>		
                	           
        <!--- Loop Query to Create Structure in Autocomplete Format  --->
		<cfset academicTermResultsArray = ArrayNew(1) />
        <cfloop query="qAcademicTerm">
        	<cfset academicTermResult = StructNew() />
			<cfset academicTermResult['termID']=' '&termCode />
			<cfset academicTermResult['season']=semester />
            <cfset academicTermResult['ay']=academicYear />
            <cfset academicTermResult['value']=' '&termCode />
            <cfset academicTermResult['id']= ' '&termCode />
            <cfset ArrayAppend(academicTermResultsArray,academicTermResult) />
        </cfloop>
        
        <!--- Serialize & Return Search Results --->
        <cfreturn SerializeJSON(academicTermResultsArray) />
	</cffunction>                
</cfcomponent> 