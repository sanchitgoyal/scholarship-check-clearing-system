<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods
	
		1. 
	
---------------------------------------------------------------------------------------------------->

<cfcomponent displayname="TagManager" bindingname="TagManager" hint="Check Tags Manager CFC" output="no">
	<!--- Constructor --->
    <cffunction name="init" access="public" returntype="TagManager" output="no" hint="CFC Initializer" description="CFC Initializer">
		<!--- Input: Coldfusion Data Source --->
        <cfargument name="dataSource" required="yes" type="string" hint="ColdFusion DataSource">
        <!--- Set Data Source Reference --->
        <cfset variables.dataSource = arguments.dataSource />
        <!--- Instantiate Check Spec Validator CFC ---> 
        <cfset variables.checkValidator = CreateObject("component","gears.studentfinancialaid.specs.CheckSpec") />     
        <!--- Instantiate Check Tag Validator CFC ---> 
        <cfset variables.checkTagValidator = CreateObject("component","gears.studentfinancialaid.specs.CheckTagSpec") />     
        <!--- Return This Instance --->   
        <cfreturn this />
    </cffunction>

    <!--- Get Tags From DB As Query --->
    <cffunction name="getTagsAsQuery" access="public" output="no" returntype="query" hint="Returns Query Version of Active/InActive Tags" 
    			description="Takes Input: active/inactive and returns Query Version of Tags Accordingly">
		<!--- Input: Fetch active ones ? --->
        <cfargument name="active" type="boolean" required="yes" hint="Active?" />
        <!--- Input: Search Term --->
        <cfargument name="searchTerm" type="string" required="no" default="" hint="Search Term" />
        <!----------------- Test Cases -------------------
			1. Table is Empty 	--> Empty Query Returns
			2. Search Term is Empty --> Return All Tags 
		------------------------------------------------->
        
        <!--- Split Search Term into Individual Search Words --->
        <cfset searchWords = ListToArray(REReplace(arguments.searchTerm,'\s',','),',',false) />
        <!--- Search Tags --->
        <cfquery name="qTags" datasource="#variables.dataSource#">
        	SELECT tag
            FROM und_finaid.tags
            WHERE active = <cfif active>1<cfelse>0</cfif>
            <!--- Cover Case 2 --->
            <cfif ArrayLen(searchWords) neq 0> 
            	AND tag LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#searchWords[1]#%" />
                <cfloop index="i" from="2" to="#ArrayLen(searchWords)#">
                	OR tag LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#searchWords[i]#%" />
                </cfloop> 
            </cfif>    
            ORDER BY tag DESC
        </cfquery> 
        <!--- Return Search Results --->
        <cfreturn qTags />
	</cffunction>

    <!--- Get Tags From DB in Autocomplete Format --->
    <cffunction name="getTagsAsAutoCompleteJSON" access="public" output="no" returntype="string" 
    			hint="Returns JQuery Autocomplete Version of Active Tags" description="Returns JQuery Autocomplete Version of Active Tags">
        <!--- Input: Search Term --->
        <cfargument name="searchTerm" type="string" required="no" default="" hint="Search Term" />
        <!----------------- Test Cases -------------------
			1. Search Term is Empty --> Return All Tags 
		------------------------------------------------->

		<!--- Query Tags on Search Term --->
        <!--- Covers Case 2 --->
		<cfset queriedTags = getTagsAsQuery(active=true,searchTerm=arguments.searchTerm) />
                
        <!--- Loop Query to Create Autocomplete Format Structure --->
		<cfset tagsArray = ArrayNew(1) />
        <cfloop query="queriedTags">
        	<cfset tagStruct = StructNew() />
			<cfset tagStruct['id']=tag />
			<cfset tagStruct['label']=tag />
            <cfset tagStruct['value']=tag />                                	
            <cfset ArrayAppend(tagsArray,tagStruct) />
        </cfloop>
        
        <!--- Serialize & Return Tags ---> 
        <cfreturn SerializeJSON(tagsArray) />
	</cffunction>
    
    <!--- Get Check's Tags As Query --->
    <cffunction name="getCheckTagsAsQuery" access="public" output="false" returntype="query" 
    			hint="Returns Query Version of Tags on Check" description="Returns Query Version of Tags on Check">
        <!--- Input: Check Deposit Number --->        
        <cfargument name="depositNumber" type="string" required="true" hint="Check's Deposit Number" />
        <!------------ Test Cases -------------------
			1. Invalidly Formatted Deposit Number
		--------------------------------------------->
        <cftry>
            <!--- Cover Case 1 --->
            <cfif variables.checkValidator.isValidDepositNumber(arguments.depositNumber)>
            	<!--- Query Selected Check's Tags --->
                <cfquery datasource="#variables.dataSource#" name="qCheckTags">
                    SELECT tag
                    FROM und_finaid.checktags 
                    WHERE depositNumber =  <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.depositNumber#" />
                </cfquery>
                <!--- Return Search Results As Query --->
                <cfreturn qCheckTags />
            </cfif>      
                
            <!--- Catch & Process Exceptions --->  
            
            <!--- Illegal Deposit Number --->
            <cfcatch type="IllegalDepositNumberFormatException">
            	<!--- Rethrown for Upper Level Processing --->
                <cfrethrow />
            </cfcatch>            
        </cftry>
    </cffunction>

    <!--- Get Check's Tags As List --->
    <cffunction name="getCheckTagsAsList" access="public" output="false" returntype="string" 
    			hint="Returns List Version of Tags on Check" description="Returns List Version of Tags on Check">
        <!--- Input: Check Deposit Number --->
        <cfargument name="depositNumber" type="string" required="true" hint="Check's Deposit Number" />
        <!------------ Test Cases ----------------
			1. Invalid Formatted Deposit Number
		------------------------------------------>
        
        <!--- Initialize Local Variables --->
        <cfset var qCheckTags = '' />

        <cftry>
            <!--- Handle Case 1 --->
            <cfif variables.checkValidator.isValidDepositNumber(arguments.depositNumber)>    
                <!--- Get Tags as Query --->
                <cfset qCheckTags = getCheckTagsAsQuery(arguments.depositNumber) />
                <!--- Convert & Return Tag Query to Tag List --->
                <cfreturn valueList(qCheckTags.tag,',') />
            </cfif>   
                   
            <!--- Catch & Process Exceptions --->  
            
            <!--- Illegal Deposit Number --->
            <cfcatch type="IllegalDepositNumberFormatException">
            	<!--- Rethrown for Upper Level Processing --->
                <cfrethrow />
            </cfcatch>            
        </cftry>
    </cffunction>    

	<!--- Create Tags --->
	<cffunction name="createCheckTag" access="public" output="no" returntype="void" hint="Creates Check Tag on DB" description="Creates Check Tag on DB">
        <!--- Input: Check Tag Object --->        
		<cfargument name="checkTag" type="gears.studentfinancialaid.CheckTag" required="yes" hint="Check Tag Component Object" />
        <!----------------------- Test Cases ------------------
			1. Inserted Tag has text same as an existing one
		------------------------------------------------------->
        <cftransaction action="begin" isolation="serializable">
            <cftry>
                <!--- Insert Tag in DB --->
                <cfquery name="iCheckTag" datasource="#variables.dataSource#" result="test">
                    INSERT INTO und_finaid.tags
                    VALUES (
                    	<cfqueryparam cfsqltype="cf_sql_varchar" value="#Trim(arguments.checkTag.getTagText())#" />,
						<cfif arguments.checkTag.isActive()>1<cfelse>0</cfif>
                    )
                </cfquery>
                
                <!--- Catch & Process Exceptions --->
                
                <!--- Database Exceptions --->
                <cfcatch type="database">
                    <!--- Rollback Transaction --->
                    <cftransaction action="rollback" isolation="serializable" />
                    <!--- MySQL Dependent Code --->
                    <cfswitch expression="#cfcatch.NativeErrorCode#">
                        <!--- Duplicacy in Tag Data --->
                        <cfcase value="1062">
                            <!--- Cover Case 1 --->
                            <cfthrow type="DuplicateCheckTagException" message="Check Tags Cannot Be Duplicated"  
                                detail="A duplicate Check Tag was found in database with same text" extendedinfo="#arguments.checkTag.getTagText()#" />
                        </cfcase>
                        <!--- Unknown Database Exception --->
                        <cfdefaultcase>
                            <!--- Rethrown for Upper Level Processing --->
                            <cfrethrow />
                        </cfdefaultcase>
                    </cfswitch>        
                </cfcatch>
                <!--- Unknown Exception --->
                <cfcatch type="any">
                    <!--- Rollback Transaction --->
                    <cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrown for Upper Level Processing --->
                    <cfrethrow />                
                </cfcatch>
            </cftry>
        </cftransaction>
	</cffunction>   

   <!--- Edit Tag Records --->
   <cffunction name="editCheckTag" access="public" output="no" returntype="void" hint="Edits Check Tags on DB" description="Edits Check Tags on DB">     	
		<!--- Input: Original Tag Text --->
        <cfargument name="originalCheckTagText" required="yes" type="string" hint="Original Tag Text" />
        <!--- Input: New Tag Data --->
        <cfargument name="checkTag" required="yes" type="gears.studentfinancialaid.CheckTag" hint="CheckTag Object with New Data to Update" />
       <!----------------------- Test Cases ----------------------
	   		1. originalCheckTagText is not a valid tag text 
			2. Inserted Tag has text same as an existing one
		--------------------------------------------------------->	
        
        <!--- Cover Case 1 --->
        <cfif variables.checkTagValidator.isValidTagText(arguments.originalCheckTagText)>
            <cftransaction action="begin" isolation="serializable">
                <cftry>
                    <!--- Update Check Tag --->
                    <cfquery name="uCheckTag" datasource="#variables.dataSource#">
                        UPDATE und_finaid.tags
                        SET tag = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Trim(arguments.checkTag.getTagText())#" />,
                        active = <cfif arguments.checkTag.isActive()>1<cfelse>0</cfif>
                        WHERE tag = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.originalCheckTagText#" />
                    </cfquery>
                    
                    <!--- Catch & Process Exceptions --->
    
                    <!--- Database Exceptions --->
                    <cfcatch type="database">
                        <!--- Rollback Transaction --->
                        <cftransaction action="rollback" isolation="serializable" />
                        <!--- MySQL Dependent Code --->
                        <cfswitch expression="#cfcatch.NativeErrorCode#">
                            <!--- Duplicacy in Tag Data --->
                            <cfcase value="1062">
                                <!--- Cover Case 2 --->
                                <cfthrow type="DuplicateCheckTagException" message="Check Tags Cannot Be Duplicated" 
                                    detail="A duplicate Check Tag was found in database with same text" extendedinfo="#arguments.checkTag.getTagText()#" />
                            </cfcase>
                            <!--- Unknown Database Exception --->
                            <cfdefaultcase>
                                <!--- Rethrown for Upper Level Processing --->
                                <cfrethrow />
                            </cfdefaultcase>
                        </cfswitch>        
                    </cfcatch>
                    <!--- Unknown Exception --->
                    <cfcatch type="any">
                        <!--- Rollback Transaction --->
                        <cftransaction action="rollback" isolation="serializable" />
                        <!--- Rethrown for Upper Level Processing --->
                        <cfrethrow />                
                    </cfcatch>
                </cftry>
            </cftransaction>
		</cfif>            
	</cffunction>

   <!--- Activate/Deactivate Tag Records --->
   <cffunction name="setCheckTagActive" access="public" output="no" returntype="void" hint="Activates/Deactivates Check Tags on DB" description="Activates/Deactivates Check Tags on DB">        <!--- Input: List of Check Tags --->    	
		<cfargument name="checkTagTextList" required="yes" type="string" hint="Check Tag Text List" />
        <!--- Input: Flag for Activation/Deactivation --->
        <cfargument name="active" required="yes" type="boolean" hint="true/false" />
        <!--------------------- Test Cases --------------------------
            1. checkTagTextList is Empty String 
            2. Any checkTagTextList tag is not properly formatted
        ------------------------------------------------------------->
        
        <!--- Convert Argument List for Array for Easier Processing --->
        <cfset checkTagTextArray = listToArray("#trim(arguments.checkTagTextList)#", ',') />

        <!--- Cover Case 1 --->
        <cfif arrayLen(checkTagTextArray) eq 0>
            <cfthrow type="IllegalTagTextException" message="Missing Check Tag Selection" 
                    detail="Select atleast one check tag for Activation/Deactivation" extendedinfo="#arguments.checkTagTextList#" />                    
        </cfif>
		
        <!--- Parse Individual Tags in List --->        
        <cfloop index="i" from="1" to="#arrayLen(checkTagTextArray)#">
            <!--- Cover Case 2 --->
			<cfset variables.checkTagValidator.isValidTagText(checkTagTextArray[i]) />
        </cfloop>

        <cftransaction action="begin" isolation="serializable">
            <cftry>
            	<!--- Activate/Deactivate Tags --->
                <cfquery name="uCheckTag" datasource="#variables.dataSource#" result="test123">
                    UPDATE und_finaid.tags
                    SET active = <cfif active>1<cfelse>0</cfif>
                    WHERE tag IN (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#arrayToList(checkTagTextArray, ',')#" />)
                </cfquery>
                                    
				<!--- Catch & Process Exceptions --->
    
                <!--- Unknown Exception --->
                <cfcatch type="any">
                    <!--- Rollback Transaction --->
                    <cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrown for Upper Level Processing --->
                    <cfrethrow />                
                </cfcatch>
            </cftry>
        </cftransaction>
	</cffunction>

   <!--- Deletes Tag Records --->
   <cffunction name="deleteCheckTags" access="public" output="no" returntype="void" hint="Deletes Check Tags on DB" description="Deletes Check Tags on DB">     	
		<!--- Input: List of Check Tags --->
        <cfargument name="checkTagTextList" required="yes" type="string" hint="Check Tag Text List" />
        <!--------------------- Test Cases --------------------------
            1. checkTagTextList is Empty String 
            2. Any checkTagTextList tag is not properly formatted
        ------------------------------------------------------------->
        
        <!--- Convert Argument List for Array for Easier Processing --->
        <cfset checkTagTextArray = listToArray("#trim(arguments.checkTagTextList)#", ',') />

        <!--- Cover Case 1 --->
        <cfif arrayLen(checkTagTextArray) eq 0>
            <cfthrow type="IllegalTagTextException" message="Missing Check Tag Selection" 
                    detail="Select atleast one check tag for Deletion" extendedinfo="#arguments.checkTagTextList#" />                    
        </cfif>
		
        <!--- Parse Individual Tags in List --->        
        <cfloop index="i" from="1" to="#arrayLen(checkTagTextArray)#">
            <!--- Cover Case 2 --->
			<cfset variables.checkTagValidator.isValidTagText(checkTagTextArray[i]) />
        </cfloop>

        <cftransaction action="begin" isolation="serializable">
            <cftry>
            	<!--- Execute DB Changes --->
                <cfquery name="dCheckTag" datasource="#variables.dataSource#" result="test">
                    DELETE FROM und_finaid.tags
                    WHERE tag IN (<cfqueryparam cfsqltype="cf_sql_varchar" list="yes" value="#arrayToList(checkTagTextArray, ',')#" />)
                </cfquery>
                
				<!--- Catch & Process Exceptions --->
    
                <!--- Catch DB Exceptions --->
                <cfcatch type="database">
                    <!--- Roll Back Transaction --->
                    <cftransaction action="rollback" isolation="serializable" />
                    <!--- MySQL Dependent Processing --->
                    <cfswitch expression="#cfcatch.NativeErrorCode#">
                        <!--- Foreign Key Constraint Violation --->
                        <cfcase value="1451">
                            <cfthrow type="CannotDeleteCheckTagException" message="Cannot Delete Check Tag" extendedinfo="#arrayToList(checkTagTextArray, ',')#" 
                            detail="Check Tag cannot be deleted at this time due to being in use in the system. Any such term record can only be deactivated." />
                        </cfcase>
						<!--- Unknown Database Exception --->
                        <cfdefaultcase>
                            <!--- Rethrown for Upper Level Processing --->
                            <cfrethrow />
                        </cfdefaultcase>
                    </cfswitch>
                </cfcatch>
                
                <!--- Unknown Exception --->
                <cfcatch type="any">
                    <!--- Rollback Transaction --->
                    <cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrown for Upper Level Processing --->
                    <cfrethrow />                
                </cfcatch>
            </cftry>
        </cftransaction>
	</cffunction>
</cfcomponent> 
