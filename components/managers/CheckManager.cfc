<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalDepositNumberFormatException
		2. UnknownTermCodeValueException
		3. UnknownStudentRecordIDValueException
		4. UnknownDepositNumberValueException
		5. UnknownItemTypeCodeException
	
---------------------------------------------------------------------------------------------------->

<cfcomponent displayname="CheckManager" bindingname="CheckManager" hint="Check Manager CFC" output="no">
	<!--- Constructor --->
    <cffunction name="init" access="public" output="no" returntype="CheckManager" hint="CFC Initializer" description="CFC Initializer">
    	<cfargument name="dataSource" type="string" required="yes" />
        
        <!--- Copy Data Source --->    
        <cfset variables.dataSource = arguments.dataSource />

        <!--- Instantiate Spec Checker ---> 
        <cfset variables.checkSpecChecker = CreateObject("component","gears.studentfinancialaid.specs.CheckSpec") />
        <!--- Instantiate Check Comment Manager --->
        <cfset variables.checkCommentManager = CreateObject("component","gears.studentfinancialaid.managers.CheckCommentManager").init(variables.dataSource) />
        <!--- Instantiate Tag Manager --->
        <cfset variables.tagManager = CreateObject("component","gears.studentfinancialaid.managers.TagManager").init(variables.dataSource) />

        <cfreturn this />                                
    </cffunction>  

	<!--- Get Check Record --->
    <cffunction name="getCheckRecord" access="public" output="no" returntype="gears.studentfinancialaid.Check" 
    			hint="Returns Check Object" description="Gets Check Object Corresponding to Deposit Number Argument">
        <!--- Input: Check Deposit Number --->        
        <cfargument name="depositNumber" required="yes" type="string" hint="Check Deposit Number" />    
        <!--------- Test Cases ------------
			1. Invalid Deposit Number  
		---------------------------------->		

        <!--- Instantiate Local Variables --->
        <cfset var checkInstance = '' />
        <cfset var checkTagsList = '' />
        <cfset var disbursementsJSON = '' />
        <cfset var commentsJSON = '' />

		<!--- Validate Deposit Number : Throws IllegalDepositNumberFormatException --->
        <cfif variables.checkSpecChecker.isValidDepositNumber(depositNumber)>
            <!--- Query Main Check Fields --->
            <cfquery datasource="#variables.dataSource#" name="qCheckMain">
                SELECT 	checkNumber, depositedOn, receivedOn, checkTotal, 
                        createdOn, createdBy, lastModifiedOn, lastModifiedBy, itemTypeCode                            
                FROM und_finaid.check
                WHERE depositNumber = '#arguments.depositNumber#'
            </cfquery>

			<!--- Check if Record Exists --->
			<cfif qCheckMain.recordCount eq 0>
            	<cfthrow type="IllegalDepositNumberFormatException" message="Unknown Check Record Reference" 
                		detail="No check record could be identified in the database by the deposit number: #arguments.depositNumber#" extendedinfo="#arguments.depositNumber#" />
            </cfif>
                            
            <!--- Get Check Tags List --->
            <cfset checkTagsList = variables.tagManager.getCheckTagsAsList(arguments.depositNumber) />

            <!--- Get Check Disbursements JSON --->
            <cfset disbursementsJSON = getCheckDisbursementsAsJSON(arguments.depositNumber) />

            <!--- Get Check Comments JSON --->
            <cfset commentsJSON = variables.checkCommentManager.getCheckCommentsAsJSON(arguments.depositNumber) />

            <!--- Create Check Instance --->
            <cfset checkInstance = createObject("component","gears.studentfinancialaid.Check")
                                    .init(
                                        depositNumber = arguments.depositNumber,
                                        createdBy = qCheckMain.createdBy[1],
                                        lastModifiedBy = qCheckMain.lastModifiedBy[1],
                                        createdOn = qCheckMain.createdOn[1],
                                        lastModifiedOn = qCheckMain.lastModifiedOn[1],
                                        depositedOn = qCheckMain.depositedOn[1],
                                        receivedOn = qCheckMain.receivedOn[1],
                                        itemTypeCode = qCheckMain.itemTypeCode[1],
                                        tagList = checkTagsList, 
                                        checkNumber = qCheckMain.checkNumber[1],
                                        checkTotal = qCheckMain.checkTotal[1],
                                        disbursementsJSON = disbursementsJSON, 
                                        commentsJSON = commentsJSON                                            
                                    ) />

            <cfreturn checkInstance />
        </cfif>            
	</cffunction>
    
	<!--- Create Check --->
    <cffunction name="createCheck" access="public" output="yes" returntype="void" hint="Creates New Check in DB" description="Creates New Check in DB">
		<cfargument name="check" type="gears.studentfinancialaid.Check" required="yes" hint="Check to Create on DB" />
		<!-------- Test Cases ---------
			1. None
		------------------------------->
        <cftransaction action="begin" isolation="serializable">
            <cftry>
            
            	<!--- Insert Main Details --->
                
                <cfquery datasource="#variables.dataSource#" name="iCheck" result="insertionResult">
                    INSERT INTO und_finaid.check
                    (checkNumber,depositedOn,receivedOn,checkTotal,createdOn,createdBy,lastModifiedOn,lastModifiedBy,itemTypeCode)            
                    VALUES (
                    		<!--- Check Number --->
                            #check.getCheckNumber()#,
                            <!--- Deposit Date --->
                            '#DateFormat(check.getDepositedOn(),"yyyy-mm-dd")#',
                            <!--- Receipt Date --->
                            '#DateFormat(check.getReceivedOn(),"yyyy-mm-dd")#',
                            <!--- Check Total --->
                            #check.getCheckTotal()#,
                            <!--- Creation Date --->
                            '#DateFormat(now(),"yyyy-mm-dd")#',
                            <!--- Creator --->
                            #session.userID#,
                            <!--- Last Modification Date --->
                            '#DateFormat(now(),"yyyy-mm-dd")#',
                            <!--- Last Modifier --->
                            #session.userID#,#check.getItemTypeCode()#
                    )
                </cfquery>

				<!--- Update/Create Deposit Number --->
                
                <cfquery datasource="#variables.dataSource#" name="uCheck">
                    UPDATE und_finaid.check
                    SET depositNumber = 'SFA0#insertionResult.Generated_Key#'
                    WHERE depositID = #insertionResult.Generated_Key#
                </cfquery>
                
                <!--- Insert Tags --->
                
                <cfset tagArray = ListToArray(check.getTagList(),",") />
                <!--- Prepare Tag List for SQL --->
                <cfloop index="i" from="1" to="#ArrayLen(tagArray)#">
                	<cfset tagArray[i] = "'" & Trim(tagArray[i]) & "'" />
                </cfloop>
                <cfif ArrayLen(tagArray) gt 0>                	
                	<cfquery datasource="#variables.dataSource#" name="iCheckTags">
                    	INSERT INTO und_finaid.checktags
                        SELECT 'SFA0#insertionResult.Generated_Key#', tag
                        FROM und_finaid.tags
                        <cfif ArrayLen(tagArray) eq 1>
                        	WHERE tag = #ArrayToList(tagArray,",")#
                        <cfelse> 
                        	WHERE tag IN (#ArrayToList(tagArray,",")#)
                        </cfif>
                    </cfquery>
                </cfif>

				<!--- Insert Disbursements --->
                
				<cfif IsJSON(check.getDisbursementsJSON())>
                    <cfset disbCollection = DeserializeJSON(check.getDisbursementsJSON()) />
                    <cfset disbursement = CreateObject("component","gears.studentfinancialaid.CheckDisbursement") />
                    <!--- Loop Through Disbursements --->
                    <cfloop index="i" from="1" to="#ArrayLen(disbCollection)#">
                    	<!--- Set Check Disbursement Object --->
						<cfset disbursement.setIDStudent(disbCollection[i].idStudent) />
                        <cfset disbursement.setTermCode(Trim(disbCollection[i].termCode)) />
                        <cfset disbursement.setAmount(disbCollection[i].amount) />
                        <cfset disbursement.setAuthDisb(disbCollection[i].authDisb) />
                        <cfset disbursement.setVouchGen(disbCollection[i].vouchGen) />
                        <!--- Execute DB Changes --->
                        <cfquery datasource="#variables.dataSource#" name="iCheckDisbursement">
                            INSERT INTO und_finaid.checkdisbursement
                            VALUES (
                            	<!--- Check Deposit Number --->
                                'SFA0#insertionResult.Generated_Key#',
                                <!--- Disbursed Student --->
                                #disbursement.getIDStudent()#,
                                <!--- Academic Term --->
                                '#disbursement.getTermCode()#',
                                <!--- Disbursed Amount --->
                                #disbursement.getAmount()#
                                <!--- Authorization Flag --->
                                ,<cfif disbursement.isAuthDisb()>1<cfelse>0</cfif>
                                <!--- Voucher Generation Flag --->
                                ,<cfif disbursement.isVouchGen()>1<cfelse>0</cfif>
                            ) 
                        </cfquery>
                    </cfloop>
                </cfif>                
                
                <!--- Catch & Process Exceptions --->
                
                <!---- Database Exceptions --->
                <cfcatch type="database">       
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- MySQL Dependent Code --->
                    <cfswitch expression="#cfcatch.NativeErrorCode#">
                    
                        <!--- Foreign Key Constraint Violation --->
                        <cfcase value="1452">
                            <!--- Get Type of Foreign Key --->
                            <cfset erroredForeignKey = REMatch('FOREIGN KEY \((\S)*\)',cfcatch.queryError) />
                            <cfswitch expression="#erroredForeignKey[1]#">
                                <!--- Term Code --->
                                <cfcase value="FOREIGN KEY (`termCode`)">
                                    <cfthrow type="UnknownTermCodeValueException" message="Unknown Academic Term" 
                                            detail="Academic Term selected for disbursement in not a known term" extendedinfo="" />
                               	</cfcase>
                                <!--- idStudent --->
                                <cfcase value="FOREIGN KEY (`idStudent`)">
                                    <cfthrow type="UnknownStudentRecordIDValueException" message="Unknown Student Record" 
                                            detail="Student Record selected for disbursement is not a known Student Record" extendedinfo="" />                                                                                
                                </cfcase>
                                <!--- Deposit Number --->                                    
                                <cfcase value="FOREIGN KEY (`depositNumber`)">
                                    <cfthrow type="UnknownDepositNumberValueException" message="Unknown Check Record" 
                                            detail="Check Selected for Disbursement in not a known Check Record" extendedinfo="" />                                    
								</cfcase>
                                <!--- Item Type Code --->
                                <cfcase value="FOREIGN KEY (`itemTypeCode`)">
                                    <cfthrow type="UnknownItemTypeCodeException" message="Unknown Item type" 
                                            detail="Item Type Selected to be Associated with the Check is not a known Item Type" extendedinfo="" />                                    
                                </cfcase>                                
								<!--- Unknown Case --->
                                <cfdefaultcase>
                                    <!--- Rethrow For Upper Level Processing --->
                                    <cfrethrow />
                                </cfdefaultcase>
                            </cfswitch>
                        </cfcase>
                        
                        <!--- Unexpected Native Error Code --->
                        <cfdefaultcase>
                        	<!--- Rethrow For Upper Level Processing --->
                        	<cfrethrow />
                        </cfdefaultcase>
                    </cfswitch>
                </cfcatch>            
                                                       
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
        	</cftry>        
		</cftransaction>
	</cffunction>               
    
	<!--- Modify Check --->
    <cffunction name="modifyCheck" access="public" output="yes" returntype="void" hint="Modifies A Check in DB" description="Modifies A Check in DB">
		<cfargument name="check" type="gears.studentfinancialaid.Check" required="yes" hint="Check to Modify on DB" />
		<!-------- Test Cases ---------
			1. None 
		------------------------------->
        <cftransaction action="begin" isolation="serializable">
            <cftry>
            
				<!--- Modify Main Details --->
                
                <cfquery datasource="#variables.dataSource#" name="uCheck">
                    UPDATE und_finaid.check
                    SET 
                    	<!--- Check Number --->
                    	checkNumber = #check.getCheckNumber()#,
                        <!--- Check Deposit Date --->
                    	depositedOn = '#DateFormat(check.getDepositedOn(),"yyyy-mm-dd")#',
                    	<!--- Check Received Date --->
                        receivedOn = '#DateFormat(check.getReceivedOn(),"yyyy-mm-dd")#',
                    	<!--- Check Total --->
                        checkTotal = #check.getCheckTotal()#,
                    	<!--- Last Modification Date --->
                        lastModifiedOn = '#DateFormat(now(),"yyyy-mm-dd")#',
                    	<!--- Modifying User --->
                        lastModifiedBy = #session.userID#,
                    	<!--- Item Type --->
                        itemTypeCode = #check.getItemTypeCode()#                    
                    WHERE depositNumber = '#check.getDepositNumber()#'
                </cfquery>
                
                <!--- Modify Tags --->
                
                <!--- Extract Tag List --->
                <cfset tagArray = ListToArray(check.getTagList(),",") />
                <!--- Prepare List for SQL --->
                <cfloop index="i" from="1" to="#ArrayLen(tagArray)#">
                	<cfset tagArray[i] = "'" & Trim(tagArray[i]) & "'" />
                </cfloop>
                <!--- Verify Emptyness --->
                <cfif ArrayLen(tagArray) gt 0>     
                	<!--- Delete Existing Ones --->           	
                    <cfquery datasource="#variables.dataSource#" name="uCheckTags1">
                        DELETE FROM und_finaid.checktags
						WHERE depositNumber = '#check.getDepositNumber()#'
                    </cfquery>
                    <!--- Insert New Ones --->
                	<cfquery datasource="#variables.dataSource#" name="uCheckTags2">
                    	INSERT INTO und_finaid.checktags
                        SELECT '#check.getDepositNumber()#', tag
                        FROM und_finaid.tags
                        <cfif ArrayLen(tagArray) eq 1>
                        	WHERE tag = #ArrayToList(tagArray,",")#
                        <cfelse> 
                        	WHERE tag IN (#ArrayToList(tagArray,",")#)
                        </cfif>
                    </cfquery>
                </cfif>

				<!--- Modify Disbursements --->
                
                <!--- Delete Old Ones --->
				<cfquery datasource="#variables.dataSource#" name="uCheckDisbursement1">
					DELETE FROM und_finaid.checkdisbursement
                    WHERE depositNumber = '#check.getDepositNumber()#'
                </cfquery>
				<!--- Insert New Ones --->
				<cfif IsJSON(check.getDisbursementsJSON())>
                    <cfset disbCollection = DeserializeJSON(check.getDisbursementsJSON()) />
                    <cfset disbursement = CreateObject("component","gears.studentfinancialaid.CheckDisbursement") />
                    <!--- Loop Through Disbursement Collection --->
                    <cfloop index="i" from="1" to="#ArrayLen(disbCollection)#">
                    	<!--- Set Check Disbursement Object  --->
						<cfset disbursement.setIDStudent(disbCollection[i].idStudent) />
                        <cfset disbursement.setTermCode(Trim(disbCollection[i].termCode)) />
                        <cfset disbursement.setAmount(disbCollection[i].amount) />
                        <cfset disbursement.setAuthDisb(disbCollection[i].authDisb) />
                        <cfset disbursement.setVouchGen(disbCollection[i].vouchGen) />
                        <!--- Insert Disbursement --->
                        <cfquery datasource="#variables.dataSource#" name="uCheckDisbursement2">
                            INSERT INTO und_finaid.checkdisbursement
                            VALUES (
                                '#check.getDepositNumber()#',#disbursement.getIDStudent()#,
                                '#disbursement.getTermCode()#',#disbursement.getAmount()#
                                ,<cfif disbursement.isAuthDisb()>1<cfelse>0</cfif>
                                ,<cfif disbursement.isVouchGen()>1<cfelse>0</cfif>
                            ) 
                        </cfquery>
                    </cfloop>
                </cfif>                
                
                <!--- Catch & Process Exceptions --->
                
                <!---- Database Exceptions --->
                <cfcatch type="database">       
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- MySQL Dependent Code --->
                    <cfswitch expression="#cfcatch.NativeErrorCode#">
                    
                        <!--- Foreign Key Constraint Violation --->
                        <cfcase value="1452">
                            <!--- Get Type of Foreign Key --->
                            <cfset erroredForeignKey = REMatch('FOREIGN KEY \((\S)*\)',cfcatch.queryError) />
                            <cfswitch expression="#erroredForeignKey[1]#">
                                <!--- Term Code --->
                                <cfcase value="FOREIGN KEY (`termCode`)">
                                    <cfthrow type="UnknownTermCodeValueException" message="Unknown Academic Term" 
                                            detail="Academic Term selected for disbursement in not a known term" extendedinfo="" />
                               	</cfcase>
                                <!--- idStudent --->
                                <cfcase value="FOREIGN KEY (`idStudent`)">
                                    <cfthrow type="UnknownStudentRecordIDValueException" message="Unknown Student Record" 
                                            detail="Student Record selected for disbursement is not a known Student Record" extendedinfo="" />                                                                                
                                </cfcase>
                                <!--- Deposit Number --->                                    
                                <cfcase value="FOREIGN KEY (`depositNumber`)">
                                    <cfthrow type="UnknownDepositNumberValueException" message="Unknown Check Record" 
                                            detail="Check Selected for Disbursement in not a known Check Record" extendedinfo="" />                                    
								</cfcase>
                                <!--- Item Type Code --->
                                <cfcase value="FOREIGN KEY (`itemTypeCode`)">
                                    <cfthrow type="UnknownItemTypeCodeException" message="Unknown Item type" 
                                            detail="Item Type Selected to be Associated with the Check is not a known Item Type" extendedinfo="" />                                    
                                </cfcase>
								<!--- Unknown Case --->
                                <cfdefaultcase>
                                    <!--- Rethrow For Upper Level Processing --->
                                    <cfrethrow />
                                </cfdefaultcase>
                            </cfswitch>
                        </cfcase>
                        
                        <!--- Unexpected Native Error Code --->
                        <cfdefaultcase>
                        	<!--- Rethrow For Upper Level Processing --->
                        	<cfrethrow />
                        </cfdefaultcase>
                    </cfswitch>
                </cfcatch>            
                                                       
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
            </cftry>   
		</cftransaction>
	</cffunction>     
    
    <!--- Delete Check Record --->
    <cffunction name="deleteCheck" access="public" output="no" returntype="void" hint="Deletes A Check Record" description="Deletes A Check Record">
    	<!--- Input: Check Deposit Number --->
		<cfargument name="depositNumber" type="string" required="yes" hint="Check Deposit Number" />
		<!------------------ Test Cases -----------------
			1. Invalid format for check deposit number 
			2. Check Record Not Found
		------------------------------------------------>
        <cftransaction action="begin" isolation="serializable">
            <cftry>
				<!--- Modify Main Check Details --->
                <cfquery datasource="#variables.dataSource#" name="dCheck">
                    DELETE FROM und_finaid.check
                    WHERE depositNumber = '#arguments.depositNumber#'
                </cfquery>
                
                <!--- Catch & Process Exceptions --->
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                	<!--- Rollback Transaction --->
                	<cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
            </cftry>   
		</cftransaction>
	</cffunction>   

    <!--- Get Check Disbursements As Query --->
    <cffunction name="getCheckDisbursementsAsQuery" access="public" output="false" returntype="query" 
    			hint="Returns check's disbursements as Query" description="Returns Query Version of all Disbursements Related to a Particular Check">
        <!--- Input: Check Deposit Number --->
        <cfargument name="depositNumber" type="string" required="true" hint="Check's Deposit Number" />
        <!------------ Test Cases ----------------
			1. Invalid Formatted Deposit Number
		------------------------------------------>           
        <cftry>
            <!--- Handle Case 1 : Throws IllegalDepositNumberFormatException--->
            <cfif variables.checkSpecChecker.isValidDepositNumber(arguments.depositNumber)>
                <!--- Query Check Disbursements --->
                <cfquery datasource="#variables.dataSource#" name="qCheckDisbursements">
                    SELECT  disbursementsTable.idStudent as idStudent,
                    		studentTable.emplID as emplID,
                            CONCAT(studentTable.lastName,', ',studentTable.firstName) as studentName,
                            disbursementsTable.termCode as termCode,
                            disbursementsTable.amount as amount,
                            disbursementsTable.authDisb as authDisb, 
                            disbursementsTable.vouchGen as vouchGen
                    FROM und_finaid.student as studentTable, und_finaid.checkdisbursement disbursementsTable
                    WHERE disbursementsTable.depositNumber = '#arguments.depositNumber#'
                    AND disbursementsTable.idStudent = studentTable.idStudent
                </cfquery>        
                <cfreturn qCheckDisbursements />
            </cfif>          
                
			<!--- Catch & Process Exceptions --->
                
			<!--- Unknown Exceptions --->
            <cfcatch type="any"> 
                <!--- Rollback Transaction --->
                <cftransaction action="rollback" isolation="serializable" />
                <!--- Rethrow For Upper Level Processing --->
                <cfrethrow />
            </cfcatch>
        </cftry>
    </cffunction> 

    <!--- Get Check Disbursements As JSON --->
    <cffunction name="getCheckDisbursementsAsJSON" access="public" output="false" returntype="string" 
    			hint="Returns check's disbursements as JSON " description="Returns JSON Version of all Disbursements Related to a Particular Check">
        <!--- Input: Check Deposit Number --->
        <cfargument name="depositNumber" type="string" required="true" hint="Check's Deposit Number" />
        <!------------ Test Cases ----------------
            1. Invalid Formatted Deposit Number     
        ------------------------------------------>   

        <!--- Initialize Local Variables --->
        <cfset var qCheckDisbursements = '' />
        <cfset var disbursementsCollection = arrayNew(1) />
        <cfset var disbursement = {} />

        <cftry>
            <!--- Handle Case 1 : Throws IllegalDepositNumberFormatException --->
            <cfif variables.checkSpecChecker.isValidDepositNumber(arguments.depositNumber)>   
                <!--- Get Tags as Query --->
                <cfset qCheckDisbursements = getCheckDisbursementsAsQuery(arguments.depositNumber) />
                <!--- Create Disbursement Collection by Looping Through Query --->
                <cfloop query="qCheckDisbursements">
                    <cfset disbursement = structNew() />
                    <cfset disbursement['idStudent'] = qCheckDisbursements.idStudent />
					<!--- Accomodate Empl ID Serialization for Leading Zero Problem --->
					<cfset disbursement['emplID'] = ' '&qCheckDisbursements.emplID />                    
                    <cfset disbursement['studentName'] = qCheckDisbursements.studentName />
					<!--- Accomodate Term Code Serialization for Leading Zero Problem --->	
                    <cfset disbursement['termCode'] = ' '&qCheckDisbursements.termCode />
                    <cfset disbursement['amount'] = qCheckDisbursements.amount />
                    <cfset disbursement['authDisb'] = qCheckDisbursements.authDisb />
                    <cfset disbursement['vouchGen'] = qCheckDisbursements.vouchGen />
                    <cfset arrayAppend(disbursementsCollection,disbursement) />
                </cfloop>

                <!--- JSONify the Disbursements Collection --->
                <cfreturn serializeJSON(disbursementsCollection) />
            </cfif>         
             
			<!--- Catch & Process Exceptions --->
                
			<!--- Unknown Exceptions --->
            <cfcatch type="any"> 
                <!--- Rollback Transaction --->
                <cftransaction action="rollback" isolation="serializable" />
                <!--- Rethrow For Upper Level Processing --->
                <cfrethrow />
            </cfcatch>
        </cftry>
    </cffunction>
    
    

</cfcomponent>	

<!----------------------------- Work Shop --------------------------------------

    <!--- Get Check Disbursements Report --->
    <cffunction name="getCheckDisbursementTermReport" access="public" output="false" returntype="query" hint="Returns Term Report for Authorized/Outstanding/Vouchered Disbursements" description="Returns Term Report for Authorized/Outstanding/Vouchered Disbursements">
        <cfargument name="termCode" type="string" required="true" hint="Academic Term Code" />
        <cfargument name="reportType" type="string" required="true" hint="Report Type Picker" />
        <!-----------------------------   Test Cases ----------------------------------------
            1. termCode is invalid format 
            2. reportType is not one of three options -> authorized, vouchered, outstanding            
        ------------------------------------------------------------------------------------->            

        <!---- To Do : Insert Check for Case 1 --->
        <!---- Check for Case 2 --->        
        <cfif arguments.reportType neq 'authorized' and arguments.reportType neq 'vouchered' and arguments.reportType neq 'outstanding'>            
            <!--- Throw IllegalReportTypeException Exception --->
            <cfthrow type="IllegalReportTypeException" message="Illegal Report Type" 
                    detail="Report Type (authorized/vouchered/Outstanding) is a Mandatory Filter" extendedinfo="#arguments.reportType#" />  
        </cfif>

        <cftry>                                
            <!--- Access Database & Generate Report --->
            <cfquery name="qReport" datasource="#variables.dataSource#">
                SELECT  CONCAT(CONCAT(CONCAT(CONCAT(std.lastName,', '),std.firstName),' '),std.middleInitial) as student,
                        std.emplID as emplID,
                        chkDisb.termCode as term,
                        chkDisb.amount as disbursementAmount,
                        chk.checkNumber as checkNumber,
                        itype.description as description,
                        chk.checkTotal as checkAmount,
                        date_format(chk.depositedOn ,'%m-%d-%Y') as depositDate,
                        chk.depositNumber as depositNumber,
                        itype.itemTypeCode as itemType,
                        itype.fund as fund,
                        itype.department as department,
                        itype.department as account
                FROM    und_finaid.checkdisbursement chkDisb,
                        und_finaid.check chk,
                        und_finaid.student std,
                        und_finaid.itemTypes itype
                WHERE   chkDisb.idStudent = std.idStudent
                AND     chkDisb.depositNumber = chk.depositNumber
                AND     chk.itemTypeCode = itype.itemTypeCode  
                AND     chkDisb.termCode = '#arguments.termCode#'                
                <cfswitch expression="#arguments.reportType#">
                    <cfcase value="authorized">
                        AND     chkDisb.authDisb = 1 AND chkDisb.vouchGen = 0
                    </cfcase>
                    <cfcase value="outstanding">
                        AND     chkDisb.authDisb = 0 AND chkDisb.vouchGen = 0 
                    </cfcase>
                    <cfcase value="vouchered">
                        AND     chkDisb.authDisb = 0 AND chkDisb.vouchGen = 1
                    </cfcase>                                        
                </cfswitch>            
            </cfquery>

            <!--- Catch & Process Exceptions --->            
            <cfcatch type="any">
                <!--- Unknown Exception --->
                <cfrethrow />
            </cfcatch>
        </cftry>

        <!--- Return Report as Query --->
        <cfreturn qReport />

    </cffunction>

------------------------------------------------------------------------>
