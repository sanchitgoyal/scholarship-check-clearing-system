<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods

		1. IllegalDepositNumberFormatException
		
---------------------------------------------------------------------------------------------------->

<cfcomponent displayname="CheckCommentManager" bindingname="CheckCommentManager" hint="Check Comment Manager CFC" output="no">
	<!--- Constructor --->
    <cffunction name="init" access="public" output="no" returntype="CheckCommentManager" hint="CFC Initializer" description="CFC Initializer">
    	<!--- Input: Coldfusion Data Source --->
        <cfargument name="dataSource" type="string" required="yes" hint="ColdFusion Data Source" />
        <!--- Copy Data Source Referene --->
        <cfset variables.dataSource = arguments.dataSource />
        <!--- Instantiate Check Spec Validator ---> 
        <cfset variables.checkSpecValidator = CreateObject("component","gears.studentfinancialaid.specs.CheckSpec") />
        <!--- Return This Reference --->
        <cfreturn this />                                
    </cffunction>  

    <!--- Get Check Comments As Query --->
    <cffunction name="getCheckCommentsAsQuery" access="public" output="no" returntype="query" hint="Get Check Comments As Query" description="Get Check Comments As Query">
        <!--- Input: Check Deposit Number --->
        <cfargument name="depositNumber" type="string" required="yes" hint="Check Deposit Number" />
        <!------------ Test Cases ----------------
			1. Invalid Formatted Deposit Number
		------------------------------------------>           
        <cftry>
            <!--- Handle Case 1 : Throws IllegalDepositNumberFormatException --->
            <cfif variables.checkSpecValidator.isValidDepositNumber(arguments.depositNumber)>
                <!--- Query Check Comments --->
                <cfquery datasource="#variables.dataSource#" name="qCheckComments">
                    SELECT  commentTable.gearsUserID as userID,
                            CONCAT(peopleTable.LastName,', ',peopleTable.FirstName) as userName,
                            CONVERT(commentTable.comment USING 'ASCII') as text,
                            commentTable.commentedOn as date 
                    FROM gears.people as peopleTable, und_finaid.checkcomments as commentTable 
                    WHERE peopleTable.UserID = commentTable.gearsUserID
                    AND commentTable.depositNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.depositNumber#" /> 
                    ORDER BY date DESC 
                </cfquery>
                <!--- Return Queried Comments --->
                <cfreturn qCheckComments />
            </cfif>          
                
			<!--- Catch & Process Exceptions --->
            
            <!--- Unknown Exceptions --->
            <cfcatch type="any"> 
                <!--- Rollback Transaction --->
                <cftransaction action="rollback" isolation="serializable" />
                <!--- Rethrow For Upper Level Processing --->
                <cfrethrow />
            </cfcatch>
        </cftry>
    </cffunction> 
    
    <!--- Get Check Comments As JSON --->
    <cffunction name="getCheckCommentsAsJSON" access="public" output="false" returntype="string" hint="Get Check Comments As JSON" description="Get Check Comments As JSON">
        <!--- Input: Check Deposit Number --->
        <cfargument name="depositNumber" type="string" required="yes" hint="Check Deposit Number" />
		<!------------ Test Cases ----------------
			1. Invalid Formatted Deposit Number
		------------------------------------------>   

        <!--- Initialize Local Variables --->
        <cfset var qCheckComments = '' />
        <cfset var commentsCollection = arrayNew(1) />
        <cfset var comment = {} />

        <cftry>
            <!--- Handle Case 1 : Throws IllegalDepositNumberFormatException --->
            <cfif variables.checkSpecValidator.isValidDepositNumber(arguments.depositNumber)>   
                <!--- Get Tags as Query --->
                <cfset qCheckComments = getCheckCommentsAsQuery(arguments.depositNumber) />
                <!--- Create Comments Collection by Looping Through Query --->
                <cfloop query="qCheckComments">
                    <cfset comment = structNew() />
                    <cfset comment['userID'] = qCheckComments.userID />
                    <cfset comment['userName'] = qCheckComments.userName />
                    <cfset comment['text'] = qCheckComments.text />
                    <cfset comment['date'] = qCheckComments.date />
                    <cfset arrayAppend(commentsCollection,comment) />
                </cfloop>

                <!--- JSONify the Comments Collection --->
                <cfreturn serializeJSON(commentsCollection) />
            </cfif>          
                
			<!--- Catch & Process Exceptions --->
            
            <!--- Unknown Exceptions --->
            <cfcatch type="any"> 
                <!--- Rollback Transaction --->
                <cftransaction action="rollback" isolation="serializable" />
                <!--- Rethrow For Upper Level Processing --->
                <cfrethrow />
            </cfcatch>
        </cftry>
    </cffunction>        
    

	<!--- Create Check Comment on DB --->
    <cffunction name="createCheckComment" access="public" output="false" returntype="void" hint="Creates Comment on Check" description="Creates Comment on Check">
		<!--- Input: Check Deposit Number --->
        <cfargument name="depositNumber" type="string" required="yes" hint="Check's Deposit Number" />
        <!--- Input: Check Comment Object --->
        <cfargument name="checkComment" type="gears.studentfinancialaid.CheckComment" required="yes" hint="CheckComment CFC Object" />
		<!-------------------- Test Cases ------------------------
			1. Invalid Check Deposit Number Passed as Argument 
		--------------------------------------------------------->			

        <cftransaction action="begin" isolation="serializable">
            <cftry>
            	<!--- Handle Case 1 : Throws IllegalDepositNumberFormatException --->
                <cfif variables.checkSpecValidator.isValidDepositNumber(arguments.depositNumber)>
                	<!--- Execute DB Changes --->
                    <cfquery datasource="#variables.dataSource#" name="iCheckComment">
                        INSERT INTO und_finaid.checkcomments (depositNumber,gearsUserID,commentedOn,comment)
                        VALUES (
							<!--- Check Deposit Number --->
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.depositNumber#" /> ,
                            <!--- Commenting User --->
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#checkComment.getUserID()#" /> ,
                            <!--- Comment Date/Time --->
                            <cfqueryparam cfsqltype="cf_sql_timestamp" value="#checkComment.getDate()#" /> ,
                            <!--- Comment Text --->
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#checkComment.getText()#" />
                        )
                    </cfquery>
                </cfif>       
                   
				<!--- Catch & Process Exceptions --->
                
                <!--- Illegal Check Deposit Number --->
                <cfcatch type="IllegalDepositNumberFormatException" >
                    <!--- Rollback Transaction --->
                    <cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
                
                <!--- Unknown Exceptions --->
                <cfcatch type="any"> 
                    <!--- Rollback Transaction --->
                    <cftransaction action="rollback" isolation="serializable" />
                    <!--- Rethrow For Upper Level Processing --->
                    <cfrethrow />
                </cfcatch>
            </cftry>
		</cftransaction>
	</cffunction>                

</cfcomponent>
