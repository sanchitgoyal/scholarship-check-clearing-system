<!------------------------------- Component Documentation -------------------------------------------

	Summary of Exceptions Thrown By Methods
		
		1. IllegalTagTextFormatException
----------------------------------------------------------------------------------------------------> 

<cfcomponent bindingname="CheckTag" displayname="CheckTag" hint="Check Tag CFC" output="no">
<!--- Constructor --->
	<cffunction name="init" access="public" returntype="CheckTag" hint="Check Tag CFC Initializer" description="Check Tag CFC Initializer">
		<cfargument name="tagText" type="string" required="yes" hint="Tag Text" />
        <cfargument name="active" required="no" default="yes" type="boolean" hint="yes/no" />

        <!--- Instantiate Spec Object --->
        <cfset variables.checkTagValidator = CreateObject("component","gears.studentfinancialaid.specs.CheckTagSpec") />

        <cfset setTagText(tagText) />
        <cfset setActive(active) />
        <cfreturn this />
    </cffunction>
    
<!--- Getters --->
    
    <!--- Tag Text --->
    <cffunction name="getTagText" output="no" access="public" returntype="string" hint="Returns Tag Text" description="Returns Tag Text">
    	<cfreturn #variables.tagText# />
    </cffunction>
    
    <!--- Active Flag --->
    <cffunction name="isActive" output="no" access="public" returntype="boolean" hint="Is Tag Active?" description="Is Tag Active?">
    	<cfreturn variables.active />
    </cffunction>
    
<!--- Setters --->
    
    <!--- Tag Text --->
    <cffunction name="setTagText" output="no" access="public" returntype="void" hint="Sets Tag Text" description="Sets Tag Text">
		<cfargument name="tagText" type="string" required="yes" hint="Tag Text" />
		<!----------------------- Test Cases ---------------------------
			1. Tag text is not in acceptable format 
		--------------------------------------------------------------->        
        <cfif variables.checkTagValidator.isValidTagText(arguments.tagText)>
        	<cfset variables.tagText = arguments.tagText />
        </cfif>    
    </cffunction>
    
    <!--- Active Flag --->
    <cffunction name="setActive" output="no" access="public" returntype="void" hint="Sets Active Status of Tag" description="Sets Active Status of Tag">
    	<cfargument name="active" type="boolean" required="yes" hint="yes/no" />
        <cfset variables.active = arguments.active />
    </cffunction>
</cfcomponent>