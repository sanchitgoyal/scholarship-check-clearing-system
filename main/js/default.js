$(document).ready(function() {

	/************************************
	*	Item Type AutoCompletes
	************************************/

	if($('.itemtype').length>0){

		// All Item Types 
		$( ".itemtype" ).autocomplete(
										{
											minLength: 0,	
											dataType: 'json',										
											source: '../utils/autocompleteSource.cfm?context=itemTypes'
										}
									)
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.data( "item.autocomplete", item )
			.append( "<a>" + item.id +/*+ "<br>" + item.desc +*/ "</a>" )
			.appendTo( ul );
		};	
	}

	if($('.student').length!=0){
		$( ".student" ).autocomplete({minLength: 2,source: /*students*/'../utils/autocompleteSource.cfm?context=student'})
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.data( "item.autocomplete", item )
			.append( "<a>" + item.lastname + ", "+ item.firstname + "<br> Empl ID:" + item.id + "</a>" )
			.appendTo( ul );
		};
	}

	if($('.term.autocomplete').length!=0){
		$( ".term.autocomplete" ).autocomplete({minLength: 0,source: '../utils/autocompleteSource.cfm?context=academicTerms'})
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.data( "item.autocomplete", item )
			.append( "<a>"/*Term ID: " */+ item.termID /*+ "<br>Season: "+ item.season + "<br>Academic Year: " + item.ay*/ + "</a>" )
			.appendTo( ul );
		};		
	}
	
	$(".student,.itemtype,.term.autocomplete").bind({
		click: function(){
			$( this ).autocomplete( "search", "" );	
		}
	});	
});
