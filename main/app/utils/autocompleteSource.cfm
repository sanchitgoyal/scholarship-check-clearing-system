<cftry>
	<!--- Determine AutoComplete Type --->
    <cfswitch expression="#URL.context#">
    	<!--- Item Type Auto Complete --->
        <cfcase value="itemTypes">
            <!--- Instantiate Manager --->
            <cfset itemTypeManagerObject = CreateObject("component","gears.studentfinancialaid.managers.ItemTypeManager").init(session.application_store.getAppDSN()) />  
            <cfoutput>#itemTypeManagerObject.getItemTypesAsAutoCompleteJSON(searchTerm=term,active=true)#</cfoutput>
		</cfcase>
        <!--- Check Tags Auto Complete --->
        <cfcase value="tags">
        	<!--- Instantiate Manager --->
            <cfset tagManagerObject = CreateObject("component","gears.studentfinancialaid.managers.TagManager").init(session.application_store.getAppDSN()) />
            <!--- Publish AutoComplete JSON --->
            <cfoutput>#tagManagerObject.getTagsAsAutoCompleteJSON(searchTerm=term)#</cfoutput>
        </cfcase>
        <!--- Student Auto Complete --->
        <cfcase value="student">
        	<!--- Instantiate Manager --->        
            <cfset studentManagerObject = CreateObject("component","gears.studentfinancialaid.managers.StudentManager").init(session.application_store.getAppDSN()) />
            <!--- Publish AutoComplete JSON --->            
			<cfoutput>#studentManagerObject.getStudentRecordsAsAutoCompleteJSON(searchTerm=term,active=true)#</cfoutput>
        </cfcase>
        <!--- Student Auto Complete --->
        <cfcase value="academicTerms">
        	<!--- Instantiate Manager --->        
            <cfset academicTermManager = CreateObject("component","gears.studentfinancialaid.managers.AcademicTermManager").init(session.application_store.getAppDSN()) />
            <!--- Publish AutoComplete JSON --->            
			<cfoutput>#academicTermManager.getAcademicTermsAsAutoCompleteJSON(searchTerm=term,active=true)#</cfoutput>
        </cfcase>        
    </cfswitch>
    <!--- Catch & Process Exceptions --->
    <cfcatch type="any">
    	<!--- Mail Exception to Web Team --->
		<cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
    </cfcatch>
</cftry>