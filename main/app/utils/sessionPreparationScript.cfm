<!---------------------------------------- Session Variable Initializations ----------------------------------------->

<!--- AppConfig CFC Object --->
<cfif NOT StructKeyExists(session,"appConfig")>
    <cfset session.appConfig = CreateObject("component","gears.studentfinancialaid.utils.AppConfig").init() />
</cfif>

<!--- session.appConfig.getCFCFullPath("StudentManager") --->

<!--- Mailer CFC Object --->
<cfif NOT StructKeyExists(session,"mailerObject")>
    <cfset session.mailerObject = CreateObject("component","gears.studentfinancialaid.utils.Mailers") />
</cfif>

<!------------------------------------------------------------------------------------------------------------------->