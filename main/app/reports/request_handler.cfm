<!--- //////// Process request --->
<cf_util_getpostdata result="data"/>
<!--- perform given action --->

<cfif structKeyExists(data,'action')>
	<cfoutput>
		<cfset data.sessionUserID = session.userID />
        <cfswitch expression="#data.action#">
            <cfcase value="getCheckReports">
                <cfscript>
					data.keysToRemove = ArrayNew(1);					
					if (structKeyExists(data, 'otherFilters') AND isArray(data.otherFilters)) {
						for (x = 1; x <= arrayLen(data.otherFilters); x++) {
							if (data.otherFilters[x].name EQ 'reportType') {
								data.reportType = data.otherFilters[x].match;
								ArrayAppend(data.keysToRemove,x);
							}
							else if (data.otherFilters[x].name EQ 'termCodes') {
								data.termCodes = data.otherFilters[x].match;
								ArrayAppend(data.keysToRemove,x);
							}
						}
						if (structKeyExists(data, 'keysToRemove')){							
							for (i=ArrayLen(data.keysToRemove); i>0; i--){
								ArrayDeleteAt(data.otherFilters,data.keysToRemove[i]);							
							}
						}
					}
                    getCheckReports(data);
                </cfscript>
            </cfcase>
                        
            <cfdefaultcase>
                ERROR: Unknown action.
            </cfdefaultcase>		
        </cfswitch>
	</cfoutput>
<cfelse>
	ERROR: no action was provided.
</cfif>

<!--- //////// --->
<cffunction name="getCheckReports">
	<cfargument name="data" type="struct" required="yes" />
	<cfset var columns = arrayNew(1) />
	<cfset var z = structNew() />
    
	<cfscript>
		z = {name = 'student'};
        z.combinedFields = ['std.lastName','std.firstName','std.middleName'];
		arrayAppend(columns, z);
		z = {name = 'emplID'};
        z.combinedFields = ['std.emplID'];
		arrayAppend(columns, z);
		z = {name = 'term'};
        z.combinedFields = ['chkDisb.termCode'];
		arrayAppend(columns, z);
		z = {name = 'disbursementAmount'};
        z.combinedFields = ['chkDisb.amount'];
		arrayAppend(columns, z);
		z = {name = 'checkNumber'};
        z.combinedFields = ['chk.checkNumber'];
		arrayAppend(columns, z);		
		z = {name = 'description'};
        z.combinedFields = ['itype.description'];
		arrayAppend(columns, z);		
		z = {name = 'checkAmount'};
        z.combinedFields = ['chk.checkTotal'];
		arrayAppend(columns, z);		
		z = {name = 'depositDate', searchable='false'};
		arrayAppend(columns, z);		
		z = {name = 'depositNumber'};
        z.combinedFields = ['chk.depositNumber'];
		arrayAppend(columns, z);		
		z = {name = 'itemType'};
        z.combinedFields = ['itype.itemTypeCode'];
		arrayAppend(columns, z);	
		z = {name = 'fund'};
        z.combinedFields = ['itype.fund'];
		arrayAppend(columns, z);		
		z = {name = 'department'};
        z.combinedFields = ['itype.department'];
		arrayAppend(columns, z);		
		z = {name = 'account'};
        z.combinedFields = ['itype.account'];
		arrayAppend(columns, z);		
	</cfscript>    
    	
	<cftry>
		<cf_util_datatablesresponse columns="#columns#" data="#data#" dataSource="#session.application_store.getAppDSN()#" result="dTableResponse">
        <cfoutput>
            SELECT 	CONCAT(CONCAT(CONCAT(CONCAT(ifnull(std.lastName,''),', '),ifnull(std.firstName,'')),' '),ifnull(std.middleName,'')) as student,
                    std.emplID as emplID,
                    chkDisb.termCode as term,
                    chkDisb.amount as disbursementAmount,
                    chk.checkNumber as checkNumber,
                    itype.description as description,
                    chk.checkTotal as checkAmount,
                    date_format(chk.depositedOn ,'%m-%d-%Y') as depositDate,
                    chk.depositNumber as depositNumber,
                    itype.itemTypeCode as itemType,
                    itype.fund as fund,
                    itype.department as department,
                    itype.account as account
            FROM 	und_finaid.checkdisbursement chkDisb,
                    und_finaid.check chk,
                    und_finaid.student std,
                    und_finaid.itemTypes itype
            WHERE 	chkDisb.idStudent = std.idStudent
            AND 	chkDisb.depositNumber = chk.depositNumber
            AND 	chk.itemTypeCode = itype.itemTypeCode        
            <cfif StructKeyExists(data,'termCodes')>
                AND	chkDisb.termCode IN (#Trim(data.termCodes)#) 	
            </cfif>
			<cfif StructKeyExists(data,'reportType')>
            	<cfswitch expression="#data.reportType#">
                	<cfcase value="authorized">
                    	AND chkDisb.authDisb = 1 AND chkDisb.vouchGen = 0
                    </cfcase>
                	<cfcase value="outstanding">
                    	AND chkDisb.authDisb = 0 AND chkDisb.vouchGen = 0 
                    </cfcase>
                	<cfcase value="vouchered">
                    	AND chkDisb.authDisb = 0 AND chkDisb.vouchGen = 1
                    </cfcase>                                        
                </cfswitch>
            </cfif>            
        </cfoutput>
		</cf_util_datatablesresponse>
        
        <cf_util_dTablesResponseLoop data="#dTableResponse#">
			<cfscript>
				emplID = ' #emplID#';
				term = ' #term#';
            </cfscript>        	        	
        </cf_util_dTablesResponseLoop>        
	
		<cfoutput>true|#serializeJSON(dTableResponse)#</cfoutput>

		<cfcatch type="any">
			<cfoutput>false|<cfdump var="#cfcatch#"/></cfoutput>
		</cfcatch>
	</cftry>
</cffunction>