<!--- Check for Existence of Needed Session Variables --->
<cfif structKeyExists(session, "appConfig") and structKeyExists(session,"mailerObject")>  
	<cftry>
        <!--- Instantiate Form Processing Result Struct --->
        <cfset variables.reportFormProcessResult = { type= "", title="", detail="" } />
        
        <!--- Process Form Based on Action --->
        
        <!--- Generate Action --->
        <cfif FORM.action eq 'generate'> 
			<!--- Proper Form Submission --->
            <cfif StructKeyExists(FORM,'reportType') and StructKeyExists(FORM,'termCodes')>
                <!--- Instantiate Check Disbursement Report CFC --->
                <cfset checkDisbursementReport = CreateObject("component",session.appConfig.getCFCFullPath("CheckDisbursementReport"))
                                                    .init(dataSource=session.application_store.getAppDSN()) />		
                <!--- Generate Report Object --->
                <cfset checkDisbursementRecords = checkDisbursementReport.getAsQuery(termCodes=FORM.termCodes,reportType=FORM.reportType) />
                <!--- Redirect to Generated Report --->
                <cfset reportName = "CheckDisbursementReport#dateFormat(now(), 'yyyy-mm-dd')#.xls" />
                <cfspreadsheet action="write" filename="#expandPath(reportName)#" query="checkDisbursementRecords" sheetname="Term Disbursement Report" overwrite=true /> 
                <cfheader name="Content-Disposition" value="attachment; filename=CheckDisbursementReport#dateFormat(now(), 'short')#.xls">
                <cfcontent type="application/vnd.msexcel" file="#expandPath(reportName)#" deleteFile="yes">

            <!--- Form Submission Missing "Report Type" --->    
            <cfelseif NOT StructKeyExists(FORM,'reportType') and StructKeyExists(FORM,'termCodes')>
                <cfset variables.reportFormProcessResult.title = "Missing Report Type" />
                <cfset variables.reportFormProcessResult.detail = "Please Select A Valid Report Type : Authorized | Outstanding | Vouchered" />
                <cfset variables.reportFormProcessResult.type = 'error' />   
            </cfif> 
        </cfif>
        
		<!--- Catch & Process Exceptions --->  
        
        <!--- Illegal Term Code --->
        <cfcatch type="IllegalTermCodeException">
            <!--- Set Result Struct ---> 
            <cfset variables.reportFormProcessResult.type = "error" />
            <cfset variables.reportFormProcessResult.title = cfcatch.Message />
            <cfset variables.reportFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
        <!--- Unknown Report Type --->
        <cfcatch type="UnknownReportTypeException">
            <!--- Set Result Struct ---> 
            <cfset variables.reportFormProcessResult.type = "error" />
            <cfset variables.reportFormProcessResult.title = cfcatch.Message />
            <cfset variables.reportFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
		<!--- Unknown Exceptions --->
        <cfcatch type="any"> 
            <!--- Inform Web Team --->
            <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
            
            <!--- Set Result Struct --->
            <cfset variables.reportFormProcessResult.type = "error" />
            <cfset variables.reportFormProcessResult.title = "Unknown Error!" />
            <cfset variables.reportFormProcessResult.detail = "Unknown error was encountered while processing your request. Web team has been notified of the issue. Please try again after sometime." />
        </cfcatch>
    </cftry>    
    
<!--- Handle Session Issue! --->
<cfelse>
	<cfset variables.reportFormProcessResult.type = "error" />
    <cfset variables.reportFormProcessResult.title = "Session Problem!" />
    <cfset variables.reportFormProcessResult.detail = "There was a problem encountered with session while processing your request. Please try again after logging out and logging back in the system. If the problem persists, please contact the web team for resolution." />
</cfif>    
