<!--- set encoding --->
<cfprocessingdirective pageencoding="utf-8">

<!--- //////// show gears shell --->
<cf_gui_buildpage includeOtherJS="studentfinancialaid/plugins/jquery.tagsinput.js,studentfinancialaid/default.js" includeDataTables="true" includeOtherCSS="studentfinancialaid/plugins/jquery.tagsinput.css,studentfinancialaid/test.css">

	<!--- code / page content goes here --->

	<!--- ColdFusion Object Initializations --->

	<!--- Prepare Session --->
	<cfinclude template="../utils/sessionPreparationScript.cfm" />

	<!--- Process Form Submission --->
    <cfif IsStruct(FORM) and NOT StructIsEmpty(FORM)>
		<cfinclude template="_excelReportGenerator.cfm" />
        <!--- Reset Default Form Values --->
        <cfset defaultFormValues['termCodes'] = FORM.termCodes />
		<cfset defaultFormValues['reportType'] = (StructKeyExists(FORM,'reportType') ? FORM.reportType : "") /> 
    <cfelse>
    	<!--- Default Values --->
        <cfset defaultFormValues['termCodes'] = "" />
        <cfset defaultFormValues['reportType'] = "" /> 
    </cfif>       
	
    <script type="text/javascript">
		$(document).ready(function(){
			$('#reportTypeSelector').buttonset();

			$('#termCodes').tagsInput({
				autocomplete_url:'../utils/autocompleteSource.cfm?context=academicTerms&active=true',
				autocomplete:{selectFirst:true,width:'100px',autoFill:true, minLength: 0},
				defaultText: 'add a term'
			});

			/******************************
			*		Data Table
			******************************/
			$("#checkListings tbody tr").live({
				dblclick:  function( e ) {
					$('.row_selected').removeClass('row_selected');
					$(this).addClass('row_selected');
					var rowData = $('#checkListings').dataTable().fnGetData(this);
					window.open('../checktracker/addmodifycheck.cfm?depositno='+$.trim(rowData[8])); 
				}
			});		
			
			/********************************
			*		Button 
			*********************************/
			$('#reportGenerator').click(function() { 	
				setOtherFilters();
				$('#checkListings').dataTable().fnDraw(); 
				return false;
			});					
		});
		
		function setOtherFilters() {
			var formValues = $('#reportFilterForm').serializeArray();
			otherFilters = [];
			jQuery.each(formValues, function(i, field){
				if (field.value != '*' && field.value != '') {
					field.match = field.value; 
					field.useHaving = false;
					otherFilters.push(field);
				}
			});
		}
	</script>
    
	<cfoutput>
    <!--- Print Message Banner --->
    <cfif StructKeyExists(variables,'reportFormProcessResult')>
        <div class="#variables.reportFormProcessResult.type#">
            <h3>#variables.reportFormProcessResult.title#</h3>
            #variables.reportFormProcessResult.detail#
        </div>
	</cfif>
    
        
	<div class="row">
        <fieldset class="panel">
            <legend>Report Filter</legend>
            <form id="reportFilterForm" name="reportFilterForm" action="" method="post">
                <div class="four columns">
                    <input type="text" name="termCodes" id="termCodes" value="#defaultFormValues.termCodes#" />
                </div>
                
                <div class="four columns">                
                    <div id="reportTypeSelector">
                        <input type="radio" name="reportType" id="authorized" value="authorized" <cfif defaultFormValues.reportType eq 'authorized'>checked="checked"</cfif> /><label for="authorized">Authorized</label>
                        <input type="radio" name="reportType" id="outstanding" value="outstanding" <cfif defaultFormValues.reportType eq 'outstanding'>checked="checked"</cfif> /><label for="outstanding">Outstanding</label>
                        <input type="radio" name="reportType" id="vouchered" value="vouchered" <cfif defaultFormValues.reportType eq 'vouchered'>checked="checked"</cfif> /><label for="vouchered">Vouchered</label>
                    </div>
                </div>
                                        
                <div class="four columns row">            
                    <div class="four columns">  
                        <input type="submit" id="reportGenerator" value="Preview" />
                    </div>
                    <div class="four columns">  
                        <input type="submit" id="excelGenerator" name="action" value="Generate" />                
                    </div>
                    <div class="four columns">  
                        <input type="reset" id="filterEraser" value="Clear Filters" />                
                    </div>                
                </div>
            </form>
        </fieldset>
	</div>
	</cfoutput>    
            
    <div class="row">
        <cfset columns = arrayNew(1)/>
        <cfscript>
            z = {name = 'Student'};
            arrayAppend(columns, z);
            z = {name = 'EmplID'};
            arrayAppend(columns, z);
            z = {name = 'Term'};
            arrayAppend(columns, z);
            z = {name = 'Disbursement Amt($)'};
            arrayAppend(columns, z);
            z = {name = 'Scholarship Check'};
            arrayAppend(columns, z);		
            z = {name = 'Description'};
            arrayAppend(columns, z);		
            z = {name = 'Check Amt($)'};
            arrayAppend(columns, z);		
            z = {name = 'Deposit Date'};
            arrayAppend(columns, z);		
            z = {name = 'Deposit No.'};
            arrayAppend(columns, z);		
            z = {name = 'Item Type'};
            arrayAppend(columns, z);	
            z = {name = 'Fund'};
            arrayAppend(columns, z);		
            z = {name = 'Department'};
            arrayAppend(columns, z);		
            z = {name = 'Account'};
            arrayAppend(columns, z);		
        </cfscript>
        <cf_gui_datatables 
            columns="#columns#" 
            columnSortOnLoad="0" sortDirOnLoad="DESC"
            displayRowCount="25"
            createTable="true" tableName="checkListings"
            useAjax="true" requestAction="getCheckReports"
        />
    </div>
</cf_gui_buildpage>