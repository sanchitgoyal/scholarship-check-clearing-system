<!--- set encoding --->
<cfprocessingdirective pageencoding="utf-8">

<!--- //////// show gears shell --->
<cf_gui_buildpage includeDataTables="true"
	includeOtherCSS="studentfinancialaid/plugins/jquery.tagsinput.css,studentfinancialaid/test.css"
    includeOtherJS="studentfinancialaid/plugins/jquery.tagsinput.js,studentfinancialaid/default.js"
>
	<!--- ColdFusion Object Initializations --->
    
	<!--- Mailer CFC Object --->
    <cfif NOT StructKeyExists(session,"mailerObject")>
        <cfset session.mailerObject = CreateObject("component","gears.studentfinancialaid.utils.Mailers") />
    </cfif>

	<!--- code / page content goes here --->
    <script type="text/javascript">
		$(document).ready(function(){
			/*********************************
			*		Initializations 
			**********************************/
			$('.datepicker').datepicker();
			
			$('#check-tags').tagsInput({
				autocomplete_url:'../utils/autocompleteSource.cfm?context=tags',
				autocomplete:{selectFirst:true,width:'100px',autoFill:true}
			});
			
			
			
			/*********************************
			*		Event Handlers 
			**********************************/
			$( ".itemtype" ).autocomplete({
				focus: function( event, ui ) {
					$('#ItemType').val(ui.item.id);
				},
				select: function( event, ui ) {
					$('#ItemType').val(ui.item.id);
					return false;
				}
			});		
						
			$( ".student" ).autocomplete({
				focus: function( event, ui ) {					
					$('#nameSelectedStudent').val(ui.item.lastname+','+ui.item.firstname);
				},
				select: function( event, ui ) {
					$('#student-selector').val(ui.item.lastname+','+ui.item.firstname);
					$('#idStudent').val(ui.item.recordID);
					return false;
				}
			});		

			<!------------------------------------------
						Action Handlers		
			-------------------------------------------->
			
			/******************************
			*		Select Boxes 
			*******************************/
			$('#depositDateOperator,#receiptDateOperator').bind({
				change: function(){
					if($(this).attr('id')=='depositDateOperator' )
					{
						if($(this).val()=='between'){
							$('#depositDate1,#depositDate2').removeAttr('disabled');
							$('#depositDate2').removeClass('hidden');
						}						
						else if($(this).val()=='*'){
							$('#depositDate1,#depositDate2').attr('disabled','disabled');
							$('#depositDate2').addClass('hidden');
							$('#depositDate1,#depositDate2').val('');
						}
						else {
							$('#depositDate1,#depositDate2').removeAttr('disabled');							
							$('#depositDate2').addClass('hidden');
						}
						

					}
					else {
						if($(this).val()=='between'){
							$('#receiptDate1,#receiptDate2').removeAttr('disabled');							
							$('#receiptDate2').removeClass('hidden');
						}
						else if($(this).val()=='*'){
							$('#receiptDate1,#receiptDate2').attr('disabled','disabled');
							$('#receiptDate1,#receiptDate2').val('');
							$('#receiptDate2').addClass('hidden');
						}						
						else {
							$('#receiptDate1,#receiptDate2').removeAttr('disabled');														
							$('#receiptDate2').addClass('hidden');
						}						
					}
				}
			});
			
			/******************************
			*		Data Table
			******************************/
			$("#checkListings tbody tr").live({
				dblclick:  function( e ) {
					$('.row_selected').removeClass('row_selected');
					$(this).addClass('row_selected');
					var rowData = $('#checkListings').dataTable().fnGetData(this);
					window.open('addmodifycheck.cfm?depositno='+$.trim(rowData[1])); 
				}
			});		
			
			/********************************
			*		Form 
			*********************************/
			$('#checkFilterForm').submit(function() { 
				setOtherFilters();
				$('#checkListings').dataTable().fnDraw(); 
				return false;
			});
									
			
		});
		
		function setOtherFilters() {
			var formValues = $('#checkFilterForm').serializeArray();
			otherFilters = [];
			jQuery.each(formValues, function(i, field){
				if (field.value != '*' && field.value != '') {
					field.match = field.value; 
					field.useHaving = false;
					otherFilters.push(field);
				}
			});
		}
		
	</script>
    <form id="checkFilterForm">
	    <div class="row">
	    	<!--- Filter Panel Containing Dates --->
	        <div class="section four columns">
	            <legend>Date Records</legend>
	            <!--- Deposit Date Subsection --->
	            <label>Deposit Date</label>
	            <div class="sub-section row" id="deposit-date">
	            	<div class="four columns">            	               
	                    <select id="depositDateOperator" name="depositDateOperator">
	                    	<option value="*">Select...</option>
	                    	<option value="on">On</option>
	                        <option value="before">Before</option>
	                        <option value="after">After</option>
	                        <option value="between">Between</option>
	                    </select>
					</div>
	            	<div class="eight columns">            	
	                    <input type="text" id="depositDate1" name="depositDate1" class="datepicker date1" style="width:80%" placeholder="Deposit Date #1" disabled="disabled" />
	                    <input type="text" id="depositDate2" name="depositDate2" class="datepicker date2 hidden" style="width:80%" placeholder="Deposit Date #2" disabled="disabled"/>
					</div>                
	            </div>
	            <!--- Reciept Date Subsection --->
	            <label>Receipt Date</label>  
	            <div class="sub-section row" id="reciept-date">            	          
	            	<div class="four columns">            	               
	                    <select id="receiptDateOperator" name="receiptDateOperator">
	                    	<option value="*">Select...</option>                    
	                    	<option value="on">On</option>
	                        <option value="before">Before</option>
	                        <option value="after">After</option>
	                        <option value="between">Between</option>
	                    </select>
					</div>
	                
	            	<div class="eight columns">            	
	                    <input type="text" id="receiptDate1" name="receiptDate1" class="datepicker date1" style="width:80%" placeholder="Receipt Date #1" disabled="disabled"/>
		                <input type="text" id="receiptDate2" name="receiptDate2" class="datepicker date2 hidden" style="width:80%" placeholder="Receipt Date #2" disabled="disabled"/>                    
					</div>
	            </div>   
	            
	            <div class="sub-section row" id="numbers">      
	                <label for="check-no">Check Number</label>
	                <input type="text" id="check-no" name="checkNumber" value="" style="width:80%"/>
	            </div>     
	        </div>      
	        
	        <div class="section three columns">
	        	<legend>Check Identifiers</legend>
	            <!---
	            <label for="employee">Created/Last Modified By</label>
	            <select id="employee" style="width:100%">
	            	<option value="*">Select Employe...</option>
	            	<option value="sanchit.goyal">Goyal,Sanchit</option>
	            </select>
				--->
	            <label for="student-selector">Student</label>
	            <input type="text" class="student" id="student-selector" style="width: 100%;"  />    
	            <input type="hidden"  name="idStudent" id='idStudent' />
	        </div>
	                
	        <div class="section four columns offset-by-one">
	        	<legend>Financial Information</legend>
	            <label for="item-type">Item Type</label>
	            <input type="text" class="itemtype" id="ItemType" name="itemType" style="width: 100%;"/>   
	            
                <label for="check-tags">Check Tags</label>
	            <input type="text" id="check-tags" name="tagList" value="" />
				<br />
	            <button class="medium awesome blue" type="submit">Filter Results</button> 
	            <button class="medium awesome blue" type="reset">Clear Form</button>            
	        </div>    
	    </div>
    </form>

    
    <cfset columns = arrayNew(1)/>
	<cfscript>
		z = {name="Deposit Date"}; arrayAppend(columns,z);
		z = {name="Deposit Number"}; arrayAppend(columns,z);
		z = {name="Check Number"}; arrayAppend(columns,z);
		z = {name="Item Type"}; arrayAppend(columns,z);		
		z = {name="Item Description"}; arrayAppend(columns,z);				
	</cfscript>
	<cf_gui_datatables 
		columns="#columns#" 
		columnSortOnLoad="0" sortDirOnLoad="DESC"
		displayRowCount="25"
		createTable="true" tableName="checkListings"
		useAjax="true" requestAction="getAllChecks"
	/>
</cf_gui_buildpage>