<!--- //////// Process request --->
<cf_util_getpostdata result="data"/>
<!--- perform given action --->

<cfif structKeyExists(data,'action')>
	<cfoutput>
		<cfset data.sessionUserID = session.userID />
        <cfswitch expression="#data.action#">
            <cfcase value="getAllChecks">
                <cfscript>
					data.keysToRemove = ArrayNew(1);					
					if (structKeyExists(data, 'otherFilters') AND isArray(data.otherFilters)) {
						for (x = 1; x <= arrayLen(data.otherFilters); x++) {
							if (data.otherFilters[x].name EQ 'tagList') {
								data.tagList = data.otherFilters[x].match;
								ArrayAppend(data.keysToRemove,x);
							}
							else if ((data.otherFilters[x].name EQ 'depositDate1' or data.otherFilters[x].name EQ 'depositDate2') and IsDate(data.otherFilters[x].match)){
								if(NOT StructKeyExists(data,'depositDateFilter')){
									data.depositDateFilter = StructNew();
								}
								data.depositDateFilter[data.otherFilters[x].name] = data.otherFilters[x].match;
								ArrayAppend(data.keysToRemove,x);
							}
							else if ((data.otherFilters[x].name EQ 'receiptDate1' or data.otherFilters[x].name EQ 'receiptDate2') and IsDate(data.otherFilters[x].match)){
								if(NOT StructKeyExists(data,'receiptDateFilter')){
									data.receiptDateFilter = StructNew();
								}
								data.receiptDateFilter[data.otherFilters[x].name] = data.otherFilters[x].match;
								ArrayAppend(data.keysToRemove,x);
							}
							else if (data.otherFilters[x].name EQ 'depositDateOperator') {
								if(NOT StructKeyExists(data,'depositDateFilter')){
									data.depositDateFilter = StructNew();
								}
								data.depositDateFilter[data.otherFilters[x].name] = data.otherFilters[x].match;
								ArrayAppend(data.keysToRemove,x);
							}
							else if (data.otherFilters[x].name EQ 'receiptDateOperator') {
								if(NOT StructKeyExists(data,'receiptDateFilter')){
									data.receiptDateFilter = StructNew();
								}
								data.receiptDateFilter[data.otherFilters[x].name] = data.otherFilters[x].match;
								ArrayAppend(data.keysToRemove,x);
							}						
							else if (data.otherFilters[x].name EQ 'idStudent') {
								if(NOT StructKeyExists(data,'studentFilter')){
									data.studentFilter = StructNew();
								}
								data.studentFilter[data.otherFilters[x].name] = data.otherFilters[x].match;
								ArrayAppend(data.keysToRemove,x);
							}							
						}
						if (structKeyExists(data, 'keysToRemove')){							
							for (i=ArrayLen(data.keysToRemove); i>0; i--){
								ArrayDeleteAt(data.otherFilters,data.keysToRemove[i]);							
							}
						}
					}
                    getAllChecks(data);
                </cfscript>
            </cfcase>
                        
            <cfdefaultcase>
                ERROR: Unknown action.
            </cfdefaultcase>		
        </cfswitch>
	</cfoutput>
<cfelse>
	ERROR: no action was provided.
</cfif>

<!--- //////// --->
<cffunction name="getAllChecks">
	<cfargument name="data" type="struct" required="yes" />
	<cfset var columns = arrayNew(1) />
	<cfset var z = structNew() />
	        
	<cfscript>
		z = {name = 'DepositDate'};
		arrayAppend(columns, z);
		z = {name = 'DepositNumber'};
		arrayAppend(columns, z);
		z = {name = 'CheckNumber'};
		arrayAppend(columns, z);
		z = {name = 'ItemType'};
		arrayAppend(columns, z);
		z = {name = 'ItemDesc'};
		arrayAppend(columns, z);		
	</cfscript>
	
	<cftry>
		<cf_util_datatablesresponse columns="#columns#" data="#data#" dataSource="#session.application_store.getAppDSN()#" result="dTableResponse">
        <cfoutput>
            SELECT depositDate, depositNumber, checkNumber, itemType, itemDesc
            FROM 
            (
            	(
                    SELECT 	date_format(chk.depositedOn,'%M %d, %Y') AS depositDate, 
                            chk.depositNumber AS depositNumber, 
                            chk.checkNumber AS checkNumber, 
                            chk.itemTypeCode AS itemType, 
                            itype.description AS itemDesc
                    FROM und_finaid.check chk, und_finaid.itemtypes itype
                    WHERE chk.itemTypeCode = itype.itemTypeCode		
                    <!--- Deposit Date Filteration --->
                    <cfloop list="depositDate,receiptDate" delimiters="," index="dateType">
                        <cfif StructKeyExists(data,dateType&'Filter') and IsStruct(data[dateType&'Filter']) and StructKeyExists(data[dateType&'Filter'],dateType&'Operator')>					
                            <cfset dateTypeStruct = data[dateType&'Filter'] />
                            <cfif dateType eq 'depositDate'><cfset dbColumnName = 'depositedOn' /><cfelse><cfset dbColumnName = 'receivedOn' /></cfif>
                                                
                            <cfswitch expression="#dateTypeStruct[dateType&'Operator']#">
                                <cfcase value="on">
                                    <cfif StructKeyExists(dateTypeStruct,dateType&'1')>
                                        AND date(chk.#dbColumnName#) = '#DateFormat(dateTypeStruct[dateType&"1"],"yyyy-mm-dd")#'
                                    </cfif>                                    
                                </cfcase>
                                <cfcase value="before">
                                    <cfif StructKeyExists(dateTypeStruct,dateType&'1')>
                                        AND date(chk.#dbColumnName#) < '#DateFormat(dateTypeStruct[dateType&"1"],"yyyy-mm-dd")#'
                                    </cfif>                                    
                                </cfcase>
                                <cfcase value="after">
                                    <cfif StructKeyExists(dateTypeStruct,dateType&'1')>
                                        AND date(chk.#dbColumnName#) > '#DateFormat(dateTypeStruct[dateType&"1"],"yyyy-mm-dd")#'
                                    </cfif>                                    
                                </cfcase>
                                <cfcase value="between">
                                    <cfif StructKeyExists(dateTypeStruct,dateType&'1') and StructKeyExists(dateTypeStruct,dateType&'2')>
                                        AND date(chk.#dbColumnName#) BETWEEN '#DateFormat(dateTypeStruct[dateType&"1"],"yyyy-mm-dd")#' AND '#DateFormat(dateTypeStruct[dateType&"2"],"yyyy-mm-dd")#'
                                    </cfif>                                    
                                </cfcase>                                                                                    
                            </cfswitch>					
                        </cfif>
                    </cfloop>
            	) mainTable
				<!--- Tag Filteration --->
                <cfif StructKeyExists(data,'tagList')>
                INNER JOIN 
                (
                    SELECT DISTINCT depositNumber
                    FROM und_finaid.checktags
                <cfif ListLen(data.tagList,',') eq 1>
                    WHERE tag = '#data.tagList#'
                <cfelse>
                    <cfset data.tagList = ListToArray(data.tagList,',') />
                    <cfloop index="i" from="1" to="#ArrayLen(data.tagList)#">	
                        <cfset data.TagList[i] = "'" & data.TagList[i] & "'" /> 
                    </cfloop>
                    WHERE tag IN (#ArrayToList(data.tagList,',')#)
                </cfif>	
                ) tagFilterResults USING (depositNumber)
                </cfif>
				<!--- Student Filteration --->
                <cfif StructKeyExists(data,'studentFilter') and IsStruct(data.studentFilter) and StructKeyExists(data.studentFilter,'idStudent') and IsNumeric(data.studentFilter.idStudent)>
                INNER JOIN 
                (
                    SELECT DISTINCT depositNumber
                    FROM und_finaid.checkdisbursement
                    WHERE idStudent = #data.studentFilter.idStudent#
                ) studentFilterResults USING (depositNumber)        	
                </cfif>
            )    
            WHERE TRUE = TRUE        
        </cfoutput>
		</cf_util_datatablesresponse>
	
		<cfoutput>true|#serializeJSON(dTableResponse)#</cfoutput>

		<cfcatch type="any">
			<cfoutput>false|<cfdump var="#cfcatch#"/></cfoutput>
		</cfcatch>
	</cftry>
</cffunction>