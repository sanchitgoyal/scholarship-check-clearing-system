<!--- Check for Existence of Needed Session Variables --->
<cfif structKeyExists(session, "appConfig") and structKeyExists(session,"mailerObject")>    

    <cftry>
        <cfif (NOT StructIsEmpty(FORM)) and StructKeyExists(FORM,'disbursementsJSON')
                and StructKeyExists(FORM,'idStudent')
                and StructKeyExists(FORM,'termCode')
                and StructKeyExists(FORM,'newVal')
        >	    		
    
			<!--- Instantiate Message Structure --->
            <cfset messageStruct = StructNew() />
        
            <!--- Check Disbursement JSON Validity --->
            <cfif NOT IsJSON(FORM.disbursementsJSON)>
                <!--- Throw Exception --->
                <cfthrow type="InvalidDisbursementJSONException" message="Invalid Disbursement Records" detail="Invalid Disbursement Records" extendedinfo="#FORM.disbursementsJSON#" />
            </cfif>
            
            <!--- Deserialize JSON --->
            <cfset disbursementsCollection = DeserializeJSON(FORM.disbursementsJSON) />
            
            <!--- Extract Disbursement to Edit from FORM --->
            <cfset editedDisbursement = StructNew() />
            <cfset editedDisbursement['idStudent'] = Trim(FORM.idStudent) />
            <cfset editedDisbursement['termCode'] = Trim(FORM.termCode) />
            
            <!--- Loop Through Disbursements in JSON --->    
            <cfloop index="i" from="1" to="#ArrayLen(disbursementsCollection)#">
                <!--- Locate Disbursement to Edit in JSON --->
                <cfif editedDisbursement['idStudent'] eq Trim(disbursementsCollection[i]['idStudent'])
                    and editedDisbursement['termCode'] eq Trim(disbursementsCollection[i]['termCode']) >
                        <!--- Set New Amount Value --->
                        <cfset disbursementsCollection[i][FORM.field] = FORM.newVal />
                    <cfbreak />
                </cfif>    
            </cfloop>
            
            <!--- Prepare Message Struct --->
            <cfset messageStruct['type'] = 'success'/>
            <cfset messageStruct['title'] = 'Disbursement Edited' />
            <cfset messageStruct['detail'] = 'Disbursement was successfully edited' />
            <cfset messageStruct['disbursementsJSON'] = SerializeJSON(disbursementsCollection) />    
        
        </cfif>    
        
		<!--- Catch & Process Exceptions --->
                
        <!--- Unknown Error --->
        <cfcatch type="any">
            <!--- Inform Web Team --->
            <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
            <!--- Prepare Message Struct --->
            <cfset messageStruct['type'] = 'error'/>
            <cfset messageStruct['title'] = 'Unknown Error' />
            <cfset messageStruct['detail'] = 'Unknown error encountered while processing your request. Web Team has been notified of the issue' />        
        </cfcatch>
    </cftry>
    
    <!--- Publish Message Structure --->
	<cfoutput>#SerializeJSON(messageStruct)#</cfoutput>
    
</cfif>