<cftry>
	<!--- Check for Form Submission --->
	<cfif NOT StructIsEmpty(FORM) and StructKeyExists(FORM,'action')>
		<!--- Instantiate Check Manager CFC Object --->
		<cfset checkManager = CreateObject("component","gears.studentfinancialaid.managers.CheckManager")
								.init(dataSource=session.application_store.getAppDSN()) />
		<!--- Switch b/w FORM Actions --->
		<cfswitch expression="#FORM.action#">
        	<!--- Creation / Modification --->
        	<cfcase value="save">
            	<cfif StructKeyExists(FORM,'depositNumber') and StructKeyExists(FORM,'depositedOn') and StructKeyExists(FORM,'depositedOn') 
					and StructKeyExists(FORM,'receivedOn') and StructKeyExists(FORM,'itemTypeCode') and StructKeyExists(FORM,'tagList') 
					and StructKeyExists(FORM,'checkNumber') and StructKeyExists(FORM,'checktotal') and StructKeyExists(FORM,'disbursementsJSON') 
					and StructKeyExists(FORM,'commentsJSON')>            
                    <!--- Creation --->	
					<cfif Trim(FORM.depositNumber) eq ''>    
						<!--- Create Check Object --->        
                        <cfset check = CreateObject("component","gears.studentfinancialaid.Check")
                                        .init(									
                                                depositedOn = FORM.depositedON,
                                                receivedOn = FORM.receivedON,
                                                itemTypeCode = FORM.itemTypeCode,
                                                tagList = FORM.tagList,
                                                checkNumber = FORM.checkNumber,
                                                checktotal = FORM.checkTotal,
                                                disbursementsJSON = FORM.disbursementsJSON,
                                                commentsJSON = FORM.commentsJSON
                                        ) />  
						<!--- Create Check --->                                        
                        <cfset checkManager.createCheck(check) />   
						<!--- Set Success Message --->	 
   						<cfset variables.messageBanner.type = 'success' />
                        <cfset variables.messageBanner.title = 'Success!' />
						<cfset variables.messageBanner.detail = 'New Check Created Successfully' />
     
                    <!--- Modification --->
                    <cfelse>
						<!--- Create Check Object --->        
                        <cfset check = CreateObject("component","gears.studentfinancialaid.Check")
                                        .init(		
												depositNumber = FORM.depositNumber,							
                                                depositedOn = FORM.depositedON,
                                                receivedOn = FORM.receivedON,
                                                itemTypeCode = FORM.itemTypeCode,
                                                tagList = FORM.tagList,
                                                checkNumber = FORM.checkNumber,
                                                checktotal = FORM.checkTotal,
                                                disbursementsJSON = FORM.disbursementsJSON,
                                                commentsJSON = FORM.commentsJSON
                                        ) />  
						<!--- Modify Check --->                                        
                        <cfset checkManager.modifyCheck(check) />     
						<!--- Set Success Message --->	 
   						<cfset variables.messageBanner.type = 'success' />
                        <cfset variables.messageBanner.title = 'Success!' />
						<cfset variables.messageBanner.detail = 'Check #FORM.depositNumber# Saved Successfully' />
                    </cfif>    
				</cfif>                                                  
            </cfcase>
			<!--- Deletion --->
            <cfcase value="delete">            
            	<cfset checkManager.deleteCheck(FORM.depositNumber) />            	
				<!--- Set Success Message --->	 
                <cfset variables.messageBanner.type = 'success' />
                <cfset variables.messageBanner.title = 'Success!' />
                <cfset variables.messageBanner.detail = 'Check #FORM.depositNumber# Deleted Successfully' />
            </cfcase>
		</cfswitch>     

		<!--- Clear FORM After Success --->
        <cfset StructClear(FORM) />   
        <cfset variables.depositNo = '' />    
	</cfif>
    <cfcatch type="IllegalCheckTotalFormatException">
        <cfset variables.messageBanner.type = 'error' />
		<cfset variables.messageBanner.title = cfcatch.Message />
        <cfset variables.messageBanner.detail = cfcatch.Detail />
    </cfcatch>
    <cfcatch type="IllegalItemTypeCodeException">
        <cfset variables.messageBanner.type = 'error' />
		<cfset variables.messageBanner.title = cfcatch.Message />
        <cfset variables.messageBanner.detail = cfcatch.Detail />
    </cfcatch>
    <cfcatch type="IllegalTagTextFormatException">
        <cfset variables.messageBanner.type = 'error' />
		<cfset variables.messageBanner.title = cfcatch.Message />
        <cfset variables.messageBanner.detail = cfcatch.Detail />
    </cfcatch>    
    <cfcatch type="IllegalCheckNumberFormatException">
        <cfset variables.messageBanner.type = 'error' />
		<cfset variables.messageBanner.title = cfcatch.Message />
        <cfset variables.messageBanner.detail = cfcatch.Detail />
    </cfcatch>
    <cfcatch type="UnknownTermCodeValueException">
        <cfset variables.messageBanner.type = 'error' />
		<cfset variables.messageBanner.title = cfcatch.Message />
        <cfset variables.messageBanner.detail = cfcatch.Detail />
    </cfcatch>
    <cfcatch type="UnknownItemTypeCodeException">
        <cfset variables.messageBanner.type = 'error' />
		<cfset variables.messageBanner.title = cfcatch.Message />
        <cfset variables.messageBanner.detail = cfcatch.Detail />
    </cfcatch>
    <cfcatch type="EmptyDisbursementJSONException">
        <cfset variables.messageBanner.type = 'error' />
		<cfset variables.messageBanner.title = cfcatch.Message />
        <cfset variables.messageBanner.detail = cfcatch.Detail />
    </cfcatch>
    <cfcatch type="application">
    	<cfswitch expression="#cfcatch.type#">
        	<cfcase value="numeric">
            	<cfswitch expression="#cfcatch.arg#">
                	<cfcase value="ITEMTYPECODE">
                        <cfset variables.messageBanner.type = 'error' />
                        <cfset variables.messageBanner.title = 'Item Type Should be Numeric' />
                        <cfset variables.messageBanner.detail = 'Item Type should be Non-Zero, Non-Negative, 12 Digit Integer' />
                    </cfcase>
                	<cfcase value="CHECKNUMBER">
                        <cfset variables.messageBanner.type = 'error' />
                        <cfset variables.messageBanner.title = 'Check Number Should be Numeric' />
                        <cfset variables.messageBanner.detail = 'Check Number should be Non-Zero, Non-Negative Integer' />
                    </cfcase>
                	<cfcase value="CHECKTOTAL">
                        <cfset variables.messageBanner.type = 'error' />
                        <cfset variables.messageBanner.title = 'Check Total Should be Numeric' />
                        <cfset variables.messageBanner.detail = 'Check Total should be Non-Zero, Non-Negative Number' />
                    </cfcase>
                    <cfdefaultcase>
						<cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') /> 
                        <cfset variables.messageBanner.type = 'error' />
                        <cfset variables.messageBanner.title = 'Unknown Error Encountered' />
                        <cfset variables.messageBanner.detail = 'Unknown error was encountered while processing your request. Web team has been notified of the issue. Please try again after sometime' />
                    </cfdefaultcase>
                </cfswitch>
            </cfcase>
        	<cfcase value="date">
            	<cfswitch expression="#cfcatch.arg#">
                	<cfcase value="DEPOSITEDON">
                        <cfset variables.messageBanner.type = 'error' />
                        <cfset variables.messageBanner.title = 'Check Deposit Date Not a Valid Date' />
                        <cfset variables.messageBanner.detail = 'Check Deposit Date does not Qualify as a Valid Calendar Date' />
                    </cfcase>
                	<cfcase value="RECEIVEDON">
                        <cfset variables.messageBanner.type = 'error' />
                        <cfset variables.messageBanner.title = 'Check Received Date Not a Valid Date' />
                        <cfset variables.messageBanner.detail = 'Check Received Date does not Qualify as a Valid Calendar Date' />
                    </cfcase>
                    <cfdefaultcase>
						<cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') /> 
                        <cfset variables.messageBanner.type = 'error' />
                        <cfset variables.messageBanner.title = 'Unknown Error Encountered' />
                        <cfset variables.messageBanner.detail = 'Unknown error was encountered while processing your request. Web team has been notified of the issue. Please try again after sometime' />
                    </cfdefaultcase>
                </cfswitch>
            </cfcase>
            <cfdefaultcase>
				<cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') /> 
                <cfset variables.messageBanner.type = 'error' />
                <cfset variables.messageBanner.title = 'Unknown Error Encountered' />
                <cfset variables.messageBanner.detail = 'Unknown error was encountered while processing your request. Web team has been notified of the issue. Please try again after sometime' />
            </cfdefaultcase>
        </cfswitch>
    </cfcatch>
    <cfcatch type="any">
		<cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') /> 
        <cfset variables.messageBanner.type = 'error' />
		<cfset variables.messageBanner.title = 'Unknown Error Encountered' />
        <cfset variables.messageBanner.detail = 'Unknown error was encountered while processing your request. Web team has been notified of the issue. Please try again after sometime' />
    </cfcatch>
</cftry>