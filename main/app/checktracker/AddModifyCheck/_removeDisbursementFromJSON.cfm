<!--- Check for Existence of Needed Session Variables --->
<cfif structKeyExists(session, "appConfig") and structKeyExists(session,"mailerObject")>    

    <cftry>
        <cfif (NOT StructIsEmpty(FORM)) and StructKeyExists(FORM,'disbursementsJSON') and StructKeyExists(FORM,'idStudent') and StructKeyExists(FORM,'termCode')>	    		
        
			<!--- Instantiate Message Structure --->
            <cfset messageStruct = StructNew() />
    
            <!--- Check Disbursement JSON Validity --->
            <cfif NOT IsJSON(FORM.disbursementsJSON)>
                <!--- Throw Exception --->
                <cfthrow type="InvalidDisbursementJSONException" message="Invalid Disbursement Records" detail="Invalid Disbursement Records" extendedinfo="#FORM.disbursementsJSON#" />
            </cfif>
        
            <!--- Deserialize JSON --->
            <cfset disbursementsCollection = DeserializeJSON(FORM.disbursementsJSON) />
            
            <!--- Extract Disbursement to Delete from FORM --->
            <cfset deletedDisbursement = StructNew() />
            <cfset deletedDisbursement['idStudent'] = Trim(FORM.idStudent) />
            <cfset deletedDisbursement['termCode'] = Trim(FORM.termCode) />
            
            <!--- Loop Through Disbursements in JSON --->    
            <cfloop index="i" from="1" to="#ArrayLen(disbursementsCollection)#">
                <!--- Locate Disbursement to Delete in JSON --->
                <cfif deletedDisbursement['idStudent'] eq Trim(disbursementsCollection[i]['idStudent'])
                    and deletedDisbursement['termCode'] eq Trim(disbursementsCollection[i]['termCode']) >
                    <!--- Delete Disbursement --->
                    <cfset ArrayDeleteAt(disbursementsCollection,i) />
                    <cfbreak />
                </cfif>    
            </cfloop>
        
            <!--- Prepare Message Struct --->
            <cfset messageStruct['type'] = 'success'/>
            <cfset messageStruct['title'] = 'Disbursement Created' />
            <cfset messageStruct['detail'] = 'Your Disbursement was successfully Created on the check' />
            <cfset messageStruct['disbursementsJSON'] = SerializeJSON(disbursementsCollection) />
            <cfset messageStruct['idStudent'] = FORM.idStudent />
            <cfset messageStruct['termCode'] = FORM.termCode />
        
        </cfif>    
        
		<!--- Catch & Process Exceptions --->
                
        <!--- Unknown Error --->
        <cfcatch type="any">
            <!--- Inform Web Team --->
            <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
            <!--- Prepare Message Struct --->
            <cfset messageStruct['type'] = 'error'/>
            <cfset messageStruct['title'] = 'Unknown Error' />
            <cfset messageStruct['detail'] = 'Unknown error encountered while processing your request. Web Team has been notified of the issue' />        
        </cfcatch>
    </cftry>
    
    <!--- Publish Message Structure --->
	<cfoutput>#SerializeJSON(messageStruct)#</cfoutput>
    
</cfif>	

