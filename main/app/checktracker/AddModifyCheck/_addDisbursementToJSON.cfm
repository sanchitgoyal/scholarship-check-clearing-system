<!--- Check for Existence of Needed Session Variables --->
<cfif structKeyExists(session, "appConfig") and structKeyExists(session,"mailerObject")>    

    <cftry>
        <cfif (NOT StructIsEmpty(FORM)) and StructKeyExists(FORM,'disbursementsJSON')
                and StructKeyExists(FORM,'idStudent')
                and StructKeyExists(FORM,'emplID')
                and StructKeyExists(FORM,'studentName')
                and StructKeyExists(FORM,'termCode')
                and StructKeyExists(FORM,'amount')
        >	    		
            <!--- Verify & Convert Disbursements JSON to Struct --->
            <cfif IsJSON(FORM.disbursementsJSON)>
                <!--- Correct Format: Deserialize --->
                <cfset disbursementsCollection = DeserializeJSON(FORM.disbursementsJSON) />
            <cfelse>
                <!--- Incorrect Format/Empty: Create New Collection --->
                <cfset disbursementsCollection = ArrayNew(1) />    	        
            </cfif>
            
            <!--- Loop Through Check Disbursements --->
            <cfloop index="disbursement"  array=#disbursementsCollection#>
            	<!--- Verify if Duplicate Exists --->
                <cfif disbursement.idStudent eq FORM.idStudent and disbursement.termCode eq FORM.termCode>
                	<!--- Throw Exception --->
                    <cfthrow type="DuplicateDisbursementRecordException" message="Multiple Disbursements for Same Student in Same Term"
                            detail="A student cannot be given multiple disbursements in the same term" extendedinfo="" />
                </cfif>
            </cfloop>        
            
            <!--- Create Disbursement Struct from FORM --->
            <cfset newDisbursement = StructNew() />
            <cfset newDisbursement['idStudent'] = FORM.idStudent />
            <cfset newDisbursement['emplID'] = FORM.emplID />    
            <cfset newDisbursement['studentName'] = FORM.studentName />
            <cfset newDisbursement['termCode'] = ' '&FORM.termCode />
            <cfset newDisbursement['amount'] = FORM.amount />
            <cfset newDisbursement['authDisb'] = StructKeyExists(FORM,'authDisb') and (FORM.authDisb eq 'checked' or FORM.authDisb eq 'true')/>
            <cfset newDisbursement['vouchGen'] = StructKeyExists(FORM,'vouchGen') and (FORM.vouchGen eq 'checked' or FORM.vouchGen eq 'true')/>
            <cfset arrayAppend(disbursementsCollection,newDisbursement) />
                    
			<!--- Prepare Message Struct --->
            <cfset messageStruct['type'] = 'success'/>
            <cfset messageStruct['title'] = 'Disbursement Created' />
            <cfset messageStruct['detail'] = 'Your Disbursement was successfully Created on the check' />
            <cfset messageStruct['disbursementsJSON'] = SerializeJSON(disbursementsCollection) />
        </cfif>    
            
        <!--- Catch & Process Exceptions --->
        
        <!--- Multiple Disbursements for Same Student & Term --->        
        <cfcatch type="DuplicateDisbursementRecordException">
            <!--- Prepare Message Struct --->
            <cfset messageStruct['type'] = 'error'/>
            <cfset messageStruct['title'] = cfcatch.Message />
            <cfset messageStruct['detail'] = cfcatch.Detail />        
        </cfcatch>
                
        <!--- Unknown Error --->
        <cfcatch type="any">
            <!--- Inform Web Team --->
            <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
            <!--- Prepare Message Struct --->
            <cfset messageStruct['type'] = 'error'/>
            <cfset messageStruct['title'] = 'Unknown Error' />
            <cfset messageStruct['detail'] = 'Unknown error encountered while processing your request. Web Team has been notified of the issue' />        
        </cfcatch>
    </cftry>	
    
    <!--- Publish Message Structure --->
    <cfoutput>#SerializeJSON(messageStruct)#</cfoutput>
    
</cfif>	
	