<!--- Check for Existence of Needed Session Variables --->
<cfif structKeyExists(session, "appConfig") and structKeyExists(session,"mailerObject")>    

	<!--- Instantiate Check Comment Manager CFC --->
    <cfset checkCommentManager = CreateObject("component",session.appConfig.getCFCFullPath("CheckCommentManager")).init(session.application_store.getAppDSN()) />
    
    <!--- Instantiate Gears Handler CFC --->
    <cfset gearsHandler = CreateObject("component",session.appConfig.getCFCFullPath("GearsHandler")).init(session.application_store.getAppDSN()) />

	<!--- Instantiate Message Structure --->
    <cfset messageStruct = StructNew() />

    <cftry>
        <cfif StructKeyExists(FORM,'depositNumber') and StructKeyExists(FORM,'commentsJSON') and StructKeyExists(FORM,'newCommentText')>
			<!--- Create Check Comment Object --->
            <cfset checkComment = CreateObject("component",session.appConfig.getCFCFullPath("CheckComment")).init(userID = session.userID,text = FORM.newCommentText,date = now()) />                                
            <!--- Add Comment to Check @ DB --->                                
            <cfif Trim(FORM.depositNumber) neq ''>
                <cfset checkCommentManager.createCheckComment(depositNumber=FORM.depositNumber,checkComment=checkComment) />
            </cfif>
    
            <!--- Add Comment to Comments JSON --->
            <cfset commentsCollection = DeserializeJSON(FORM.commentsJSON) />
            
            <cfset newComment = structNew() />
            <cfset newComment['userID'] = checkComment.getUserID() />        
            <cfset newComment['userName'] = gearsHandler.getUserNameByID(checkComment.getUserID(),"lastname, firstname") />
            <cfset newComment['text'] = checkComment.getText() />
            <cfset newComment['date'] = checkComment.getDate() />
            <cfset ArrayPrepend(commentsCollection,newComment) />
            
            <!--- Prepare Message Struct --->
            <cfset messageStruct['type'] = 'success'/>
            <cfset messageStruct['title'] = 'Comment Posted' />
            <cfset messageStruct['detail'] = 'Your comment/note was successfully posted on the check' />
            <cfset messageStruct['commentsJSON'] = SerializeJSON(commentsCollection) />
                
        </cfif>
                
		<!--- Catch & Process Exceptions --->
        
        <!--- Illegal Check Deposit Number --->        
        <cfcatch type="IllegalDepositNumberFormatException">
            <!--- Prepare Message Struct --->
            <cfset messageStruct['type'] = 'error'/>
            <cfset messageStruct['title'] = 'Unrecognized Check!' />
            <cfset messageStruct['detail'] = 'Please post comment on correct check. Deposit number associated with this check was not recognized' />
        </cfcatch>
        
        <!--- Unknown Error --->
        <cfcatch type="any">
            <!--- Inform Web Team --->
            <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
            <!--- Prepare Message Struct --->
            <cfset messageStruct['type'] = 'error'/>
            <cfset messageStruct['title'] = 'Unknown Error' />
            <cfset messageStruct['detail'] = 'Unknown error encountered while processing your request. Web Team has been notified of the issue' />        
        </cfcatch>
    </cftry>
    
    <!--- Print Serialized Message Structure for JSON Call --->
	<cfoutput>#SerializeJSON(messageStruct)#</cfoutput>
    
</cfif>    
