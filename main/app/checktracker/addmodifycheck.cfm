<!--- set encoding --->
<cfprocessingdirective pageencoding="utf-8">

<!--- //////// show gears shell --->
<cf_gui_buildpage 
	includeOtherCSS="studentfinancialaid/plugins/jquery.tagsinput.css,studentfinancialaid/test.css"
    includeOtherJS="studentfinancialaid/plugins/jquery.tagsinput.js,studentfinancialaid/default.js"
>    
	<!--- Page Scope Variable Initializations --->
	
    <!--- Check Deposit Number --->
    <cfparam name="depositNo" type="string" default="" /> 
	<cfset variables.depositNo = depositNo />
	<!--- Message Banner ---
		Fields:
			1. type: success/error
			2. title: Message Heading
			2. detail: Message Detail
	------------------------------------>		
	<cfset variables.messageBanner = StructNew() />

	<!--- Prepare Session --->
	<cfinclude template="../utils/sessionPreparationScript.cfm" />

   	<!--- ColdFusion Object Initializations --->

	<!--- CheckManager CFC Object --->
    <cfset checkManager = CreateObject("component",session.appConfig.getCFCFullPath("CheckManager")).init(session.application_store.getAppDSN()) />
    <!--- ItemType Manager CFC Object --->
	<cfset itemTypeManager = CreateObject("component",session.appConfig.getCFCFullPath("ItemTypeManager")).init(session.application_store.getAppDSN()) />
    <!--- GearsHandler CFC Object --->
    <cfset gearsHandler = CreateObject("component",session.appConfig.getCFCFullPath("GearsHandler")).init(session.application_store.getAppDSN()) />

	<!--- Process Form Submission --->
	<cfif NOT StructIsEmpty(FORM)>
		<cfinclude template="AddModifyCheck/_checkFormProcessor.cfm" />
    </cfif>
	
	<!--- Form Initialization --->    
	
    <!--- Initializer Structure --->
	<cfset init = {
        createdBy = session.name,
        lastModifiedBy = session.name,
        createdOn = "#DateFormat(Now(),'mm/dd/yyyy')#",
        lastModifiedOn = "#DateFormat(Now(),'mm/dd/yyyy')#",
        depositedOn = "",
        receivedOn = "",
		itemTypeCode = '',
		tagList = '',
		checkNumber = '',
        checkTotal = '',
        disbursementsJSON = '',
        commentsJSON = '',
		itemTypeDetails = StructNew()
	} />
	<cfif Trim(variables.depositNo) neq ''>
        <cftry>
        	<!--- Get Selected Checks Initialization Details --->
            <cfset init = checkManager.getCheckRecord(variables.depositNo).toStruct() />
            
            <!--- Get Selected Item Type Details --->
            <cfset init.itemTypeDetails = itemTypeManager.getItemType(init.itemTypeCode) />
            
            <!--- Change Gears User ID into User Names --->
            <cfset init.createdBy = gearsHandler.getUserNameByID(init.createdBy,"firstname lastname") />
            <cfset init.lastModifiedBy = gearsHandler.getUserNameByID(init.lastModifiedBy,"firstname lastname") />                        
            
			<!--- Catch & Process Exceptions --->
            <cfcatch type="IllegalDepositNumberFormatException">
                <cfset variables.messageBanner.type = 'error' />
                <cfset variables.messageBanner.title = cfcatch.Message />
                <cfset variables.messageBanner.detail = cfcatch.Detail />
            </cfcatch>
            <cfcatch type="ItemTypeRecordNotFoundException">
                <cfset variables.messageBanner.type = 'error' />
                <cfset variables.messageBanner.title = cfcatch.Message />
                <cfset variables.messageBanner.detail = cfcatch.Detail />
            </cfcatch>        
            <cfcatch type="any">
                <cfset variables.messageBanner.type = 'error' />
                <cfset variables.messageBanner.title = "Unknown Error" />
                <cfset variables.messageBanner.detail = "Unknown Error Encountered" />    
                <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
            </cfcatch>
        </cftry>   
    </cfif>    
    
	<style type="text/css">
	    .ui-autocomplete { height: 75px; overflow-y: scroll; overflow-x: hidden;}
	</style>

    <script type="text/javascript">
		$(document).ready(function(){
			/************************
			*	Event Handlers 
			*************************/
			
			/*********** Text Fields ***************/
			$('.amt').live({
				'keyup keydown change': function(){
					var total = 0;
					try{
						$('.amt').each(function(index, element) {
							if(!isNaN($(element).val())){
								total = total + Number($(element).val());
							}
							else throw 'error';
						});
					}
					catch(error){
						total = 0;
					}
					
					$('#disbursementTotal').val(total);
				}
			});
			
			$('#item-type').bind({
				change: function(){
					if($(this).val()==0) {
						$('#item-type-desc').val('');
						$('#item-type-fund').val('');
						$('#item-type-acc').val('');
						$('#item-type-dept').val('');						
					}
				}
			});
			
			/********** AutoComplete Events *************/			
			$( ".itemtype" ).autocomplete({
				focus: function( event, ui ) {
					$('#item-type').val(ui.item.id);
					$('#item-type-desc').val(ui.item.desc);
					$('#item-type-fund').val(ui.item.fund);
					$('#item-type-acc').val(ui.item.acc);
					$('#item-type-dept').val(ui.item.dept);
				},
				select: function( event, ui ) {
					$('#item-type').val(ui.item.id);
					$('#item-type-desc').val(ui.item.desc);
					$('#item-type-fund').val(ui.item.fund);
					$('#item-type-acc').val(ui.item.acc);
					$('#item-type-dept').val(ui.item.dept);
					event.preventDefault();
					return false;
				}
			});		
			
			$( ".student.autocomplete" ).autocomplete({
				focus: function( event, ui ) {					
					$('#nameSelectedStudent').val(ui.item.lastname+','+ui.item.firstname);
				},
				select: function( event, ui ) {
					$('#nameSelectedStudent').val(ui.item.lastname+','+ui.item.firstname);
					$('#emplIDSelectedStudent').val(ui.item.id);
					$('#idSelectedStudent').val(ui.item.recordID);
					return false;
				}
			});		
			
			$( ".term.autocomplete" ).autocomplete({
				focus: function( event, ui ) {					
					$('#term-selector').val($.trim(ui.item.termID));
				},
				select: function( event, ui ) {
					$('#term-selector').val($.trim(ui.item.termID));
					return false;
				}
			});	
			
			/******** Hidden Field Events  ********/
			$('#commentsJSON').bind({
				change: function(){
					var checkComments = $.parseJSON($('#commentsJSON').val());
					$('#comments-box').html('');
					for(i in checkComments){
						var commentHTML = '';
						commentHTML = 	commentHTML +'<div class="comment">';
						commentHTML =	commentHTML +'	<div class="head">';
						commentHTML =	commentHTML +'		<span class="user-name">'+checkComments[i].userName+'</span>';
						commentHTML =	commentHTML +'		<span class="date">'+checkComments[i].date+'</span>';
						commentHTML =	commentHTML +'	</div>';
						commentHTML =	commentHTML +'  <div class="body"><span class="comment-text">'+ checkComments[i].text + '</span></div>';
						commentHTML =	commentHTML +'</div>';

						$('#comments-box').append(commentHTML);					
					}
				}
			});
			
			/******** Button Events *******/
			$('#save-comment').bind({
				click: function(){
					$.post('AddModifyCheck/_saveComment.cfm',{
							depositNumber:$('#depositNumber').val(),
							commentsJSON:$('#commentsJSON').val(),
							newCommentText:$('#user-comment').val()
						},function(response){
							$('#user-comment').val('');
							try{
							var message = $.parseJSON($.trim(response));
							if(message.type == 'success'){
								$('#commentsJSON').val(message.commentsJSON).change();
							}
							else if(message.type == 'error'){
								alert(message.detail);
							}
							else {
								alert('Unknown Error');
							}
							}catch(err){
								alert(err);
							}
						});
				}
			});
			
			$('#addDisbursement').bind({
				click: function(){
					var emplID = $('#emplIDSelectedStudent').val();
					var idStudent =  $('#idSelectedStudent').val();
					var studentName = $('#nameSelectedStudent').val();
					var termCode = $('#term-selector').val();
					var amount = $('#stu-amt').val();
					var authDisb = $('#authDisb').attr('checked');
					var vouchGen = $('#vouch-gen').attr('checked');
					
					/* Validate Required Fields */
					if($.trim(idStudent) == '') {
						alert('Please Select A Student');
						return;
					}
					else if ($.trim(termCode)=='' ) {
						alert('Please Select A Term Code');
						return;						
					}
					else if ($.trim(amount)=='') {
						alert('Please Enter An Amount for Disbursement');
						return;
					}
					else if (isNaN($.trim(amount))){
						alert('Please Enter a Numeric Amount');
						return;
					}
					else if(authDisb=='checked' && vouchGen=='checked') {
						alert('Disbursement Authorization and Voucher Generation Cannot Happen at the Same Time');
						return;
					}
										
					$.post(
						'AddModifyCheck/_addDisbursementToJSON.cfm',
						{
							disbursementsJSON: $('#disbursementsJSON').val(),
							idStudent: idStudent,
							emplID: emplID,
							studentName: studentName,
							termCode: termCode, 
							amount: amount,
							authDisb: authDisb,
							vouchGen: vouchGen
						},
						function(response){
							var message = $.parseJSON($.trim(response));							
							if(message.type == 'success'){
								$('#studentDisbursementRecords').append(createStudentRecord(idStudent,emplID,studentName,termCode,amount,authDisb,vouchGen));
								$('#disbursementsJSON').val(message.disbursementsJSON);
								$('#nameSelectedStudent').val('');
								$('#idSelectedStudent').val('');
								$('#emplIDSelectedStudent').val('');
								$('#term-selector').val('');
								$('#stu-amt').val('');
								$('#authDisb').attr('checked',false);
								$('#vouch-gen').attr('checked',false);					
											
								var total = 0;
								try{
									$('.amt').each(function(index, element) {
										if(!isNaN($(element).val())){
											total = total + Number($(element).val());
										}
										else throw 'error';
									});
								}
								catch(error){
									total = 0;
								}
								
								$('#disbursementTotal').val(total);
								
							}
							else if(message.type == 'error'){
								alert(message.detail);
							}
							else {
								alert('Unknown Error');
							}							
						}
					).fail(function(a,b,c){
						alert('Unknown error encountered while processing your request.Please try again after sometime. Please contact the web team if the problem persists.');
					});
				}
			});	
			
			$('.deleteDisbursement').live({				
				click:function(){
					if(!confirm('Are you sure you want to delete this disbursement?')) return;
					$.post('AddModifyCheck/_removeDisbursementFromJSON.cfm',{
							disbursementsJSON: $('#disbursementsJSON').val(),
							idStudent: $(this).attr('val').split('|')[0],
							termCode: $.trim($(this).attr('val').split('|')[1])
						},
						function(response){		
							var message = $.parseJSON($.trim(response));
							if(message.type == 'success'){
								$('#disbursementsJSON').val(message.disbursementsJSON);
								$('.deleteDisbursement').each(function(){
									if( $(this).attr('val').split('|')[0]==message.idStudent && $.trim($(this).attr('val').split('|')[1])==message.termCode){
										$(this).parent().parent().parent().remove();
									}
								});
								
								var total = 0;
								try{
									$('.amt').each(function(index, element) {
										if(!isNaN($(element).val())){
											total = total + Number($(element).val());
										}
										else throw 'error';
									});
								}
								catch(error){
									total = 0;
								}
								
								$('#disbursementTotal').val(total);
								
							}
							else if(message.type == 'error'){
								alert(message.detail);
							}
							else {
								alert('Unknown Error');
							}		
						}
					).fail(function(a,b,c){
						alert('Unknown error encountered while processing your request.Please try again after sometime. Please contact the web team if the problem persists.');
					});
				}
			});			
			
			$('.amt,.authDisb,.vouchGen').live({				
				'change keyup keydown click':function(){
					var identifier = $(this).parent().parent().parent().data('identifier');
					var idStudent = $.trim(identifier.split('|')[0]);
					var termCode = $.trim(identifier.split('|')[1]);
					var newVal = '';
					var field = '';
					if($(this).hasClass('amt')) field = 'amount';
					if($(this).hasClass('authDisb')) field = 'authDisb';
					if($(this).hasClass('vouchGen')) field = 'vouchGen';	
					if(field=='amount') newVal = $(this).val();
					else newVal = $(this).attr('checked')=='checked';
														
					$.post('AddModifyCheck/_editDisbursement.cfm',{
							disbursementsJSON: $('#disbursementsJSON').val(),
							idStudent: idStudent,
							termCode: termCode,
							newVal: newVal,
							field: field
						},
						function(response){		
							var message = $.parseJSON($.trim(response));
							if(message.type == 'success'){
								$('#disbursementsJSON').val(message.disbursementsJSON);
							}
						}
					).fail(function(a,b,c){
						alert('Unknown error encountered trying to update disbursement field.Please try again, otherwise please contact the web team if the problem persists.');
					});
				}
			});			
			
			$('#save-check').bind({
				click: function(){
					if($.trim($('#deposit-date').val())=='') {
						alert('Deposit Date is a required field and cannot be Left Blank !');
						return false;
					}
					else if ($.trim($('#receipt-date').val())=='') {
						alert('Receipt Date is a required field and cannot be Left Blank !');
						return false;						
					}
					else if ($.trim($('#check-no').val())=='') {
						alert('Check Number is a required field and cannot be Left Blank !');
						return false;						
					}
					else if ($.trim($('#item-type').val())=='') {
						alert('Item Type is a required field and cannot be Left Blank !');
						return false;						
					}
					else if ($.trim($('#check-total').val())=='') {
						alert('Check Total is a required field and cannot be Left Blank !');
						return false;						
					}
					else if (Number($.trim($('#check-total').val()))!=Number($.trim($('#disbursementTotal').val()))) {
						alert('Disbursement Total has to match Check Total!');
						return false;						
					}					
				}
			});
			
			$('#del-check').bind({
				click: function(){
					if(!confirm('Are you sure you want to delete this check?')) return false;
				}
			})
			
			/***********************
			*	Initializations
			************************/
			 $( "#receipt-date" ).datepicker({
				//defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 1,
				onClose: function( selectedDate ) {
					$( "#deposit-date" ).datepicker( "option", "minDate", selectedDate );
				}
			});
			
			$( "#deposit-date" ).datepicker({
				//defaultDate: "+1w",
				changeMonth: true, 
				numberOfMonths: 1,
				onClose: function( selectedDate ) {
					$( "#receipt-date" ).datepicker( "option", "maxDate", selectedDate );
				}
			});			
			
			$('#check-tags').tagsInput({
				autocomplete_url:'../utils/autocompleteSource.cfm?context=tags&active=true',
				autocomplete:{selectFirst:true,width:'100px',autoFill:true}
			});
			
			$('#commentsJSON').change();						
		});
		
		
		function createStudentRecord(idStudent,emplID,studentName,termCode,amount,authDisb,vouchGen){
			var recordHTML = '<div class="row" data-identifier="'+idStudent+'|'+termCode+'">';//<h3>'+studentName+'</h3>';

				recordHTML = recordHTML + '<div class="record row">';
					recordHTML = recordHTML + '<div class="four columns">';
					if($.trim(emplID)=='')
						recordHTML = recordHTML + '<input type="text" value="'+studentName+'" readonly="readonly" />';
					else
						recordHTML = recordHTML + '<input type="text" value="'+studentName+' ('+$.trim(emplID)+')" readonly="readonly" />';	
					recordHTML = recordHTML + '</div>';
	
					recordHTML = recordHTML + '<div class="one columns">';
						recordHTML = recordHTML + '<input type="text" value="'+termCode+'" readonly="readonly" />';
					recordHTML = recordHTML + '</div>';
				
					recordHTML = recordHTML + '<div class="two columns">';
						recordHTML = recordHTML + '<input type="text" class="amt" value="'+amount+'"/>';	
					recordHTML = recordHTML + '</div>';
				
				recordHTML = recordHTML + '<div class="two columns">';

				if(authDisb=='checked'){
					recordHTML = recordHTML + '<input type="checkbox" class="authDisb" checked="checked" />';				
				}
				else {
					recordHTML = recordHTML + '<input type="checkbox" class="authDisb" />';									
				}
				recordHTML = recordHTML + '</div>';
								
				recordHTML = recordHTML + '<div class="two columns">';

				if(vouchGen=='checked'){
					recordHTML = recordHTML + '<input type="checkbox" class="vouchGen" checked="checked" />';				
				}
				else {
					recordHTML = recordHTML + '<input type="checkbox" class="vouchGen" />';									
				}				
				recordHTML = recordHTML + '</div>';
				
				recordHTML = recordHTML + '<div class="one columns">';
				recordHTML = recordHTML + '<button type="button" class="deleteDisbursement" val="'+idStudent+'|'+termCode+'"><img src="../../../images/studentfinancialaid/icons/delete.png" alt="delete" /></button>';
				recordHTML = recordHTML + '</div>';
				
				recordHTML = recordHTML + '</div></div>'; 
				return recordHTML;
		}
	</script>       
    
    <!--- Print Messages  --->
    <cfif StructKeyExists(variables,'messageBanner') and NOT StructIsEmpty(variables.messageBanner)>
		<cfoutput>
            <div class="<cfif StructKeyExists(variables.messageBanner,'type')>#messageBanner.type#</cfif>">
                <h2>#variables.messageBanner.title#</h2>
                #variables.messageBanner.detail#
            </div>
        </cfoutput>
 	</cfif>
        
    <form action="" target="_self" method="post">
        <div class="row panel">
            <fieldset class="two columns" id="deposit-id">
                <label for="depositNumber"> Deposit Number  </label>
                <!--- Check Deposit Number Display --->                
                <input type="text" id="depositNumber" name="depositNumber" tabindex="-1" readonly="readonly" value="<cfoutput>#variables.depositNo#</cfoutput>" />  
            </fieldset>
            <div class="two columns">
                <!---Form Creator Display Field --->
                <label for="created-by">Created By </label>
                <input type="text" id="created-by" readonly="readonly" tabindex="-1" value="<cfoutput>#init.createdBy#</cfoutput>" />
            </div>
            <div class="two columns">
                <!--- Form Creation Date Display Field --->
                <label for="created-on">Created On </label>
                <input type="text" id="created-on" readonly="readonly" tabindex="-1" value="<cfoutput>#DateFormat(init.createdOn,'mm/dd/yyyy')#</cfoutput>" />    
            </div>
            <div class="two columns">
                <!--- Last Check Modifier Display Field --->    
                <label for="modified-by">Last Modified By </label>
                <input type="text" id="modified-by" readonly="readonly" tabindex="-1" value="<cfoutput>#init.lastModifiedBy#</cfoutput>" />   
            </div>
            <div class="two columns">
                <!--- Last Modification Date Display Field --->
                <label for="modified-on">Last Modified On </label>
                <input type="text" id="modified-on" readonly="readonly" tabindex="-1" value="<cfoutput>#DateFormat(init.lastModifiedOn,'mm/dd/yyyy')#</cfoutput>" />
            </div>
            <div class="two columns">
                <!--- Button to Save Check --->
                <button id="save-check" name="action" value="save" class="" type="submit"><img src="../../../images/studentfinancialaid/icons/page_save.png" alt="save"/></button>
                <!--- Button to Delete Check --->
                <cfif variables.depositNo neq ''>
                    <!--- Button Not Displayed in Case of New Check --->
                    <cfoutput>
                        <button id="del-check" name="action" value="delete" class="" type="submit">
                            <img src="../../../images/studentfinancialaid/icons/page_delete.png" alt="delete"/>
                        </button> 
                    </cfoutput>              	
                </cfif> 
            </div>
        </div>
    
        <div class="row panel">
            <div class="two columns">
                <!--- Check Receipt Date Input Field --->
                <label for="receipt-date">Check Received On</label>
                <input type="datetime" id="receipt-date" name="receivedOn" required="required" tabindex="1" value="<cfoutput>#DateFormat(init.receivedOn,'mm/dd/yyyy')#</cfoutput>" />
            </div>        
            <div class="two columns">
                <!--- Check Deposit Date Input Field --->
                <label for="deposit-date">Check Deposit Date </label>
                <input type="datetime" id="deposit-date" name="depositedOn" required="required" tabindex="2" value="<cfoutput>#DateFormat(init.depositedOn,'mm/dd/yyyy')#</cfoutput>"/>		
            </div>
            <div class="two columns">
                <!--- Check Number Input Field --->
                <label for="check-no">Check Number</label>
                <input type="text" id="check-no" name="checkNumber" required="required" tabindex="3" value="<cfoutput>#init.checkNumber#</cfoutput>"/>				 
            </div>
            <div class="two columns">
                <!--- Check Total Input Field --->
                <label for="check-total">Check Total</label>
                <input type="text" id="check-total" name="checkTotal" required="required" tabindex="4" value="<cfoutput>#init.checkTotal#</cfoutput>"/>
                <!--- Hidden Fields --->
                <input type="hidden" id="disbursementsJSON" name="disbursementsJSON" value="<cfoutput>#HTMLEditFormat(init.disbursementsJSON)#</cfoutput>" />
                <input type="hidden" id="commentsJSON" name="commentsJSON" value="<cfoutput>#HTMLEditFormat(init.commentsJSON)#</cfoutput>" />
            </div>
        </div>  
       
        <div class="row">
            <fieldset class="panel eight columns">
                <legend>Check Classification</legend>              	
                <div class="row">
                    <div class="four columns">                    	
                        <label for="item-type">Item Type</label>           
                        <input type="text" class="itemtype" id="item-type" name="itemTypeCode" tabindex="5" value="<cfif IsObject(init.itemTypeDetails)>
																														<cfoutput>#init.itemTypeDetails.getItemTypeCode()#</cfoutput>
                                                                                                                    </cfif>" 
                            style="width: 100%;" required="required"/>                
                    </div>
                    <div class="eight columns">
                        <label for="item-type-desc">Description</label>
                        <input type="text" id="item-type-desc" readonly="readonly" tabindex="-1" value="<cfif IsObject(init.itemTypeDetails)>
                                                                                            				<cfoutput>#init.itemTypeDetails.getItemTypeDescription()#</cfoutput>
                                                                                        				</cfif>" 
                        />
                    </div>
                </div>
                <div class="row">
                    <div class="four columns">        
                        <label for="item-type-fund">Fund</label>
                        <input type="text" id="item-type-fund" readonly="readonly" tabindex="-1" value="<cfif IsObject(init.itemTypeDetails)>
                                                                                            <cfoutput>#init.itemTypeDetails.getFund()#</cfoutput>
                                                                                        </cfif>" 
                        />
                    </div>
                    <div class="four columns">
                        <label for="item-type-desc">Account</label>
                        <input type="text" id="item-type-acc" readonly="readonly" tabindex="-1" value="<cfif IsObject(init.itemTypeDetails)>
                                                                                            <cfoutput>#init.itemTypeDetails.getAccount()#</cfoutput>
                                                                                        </cfif>" 
                        />
                    </div>
                    <div class="four columns">            
                        <label for="item-type-desc">Department</label>
                        <input type="text" id="item-type-dept" readonly="readonly" tabindex="-1" value="<cfif IsObject(init.itemTypeDetails)>
                                                                                            <cfoutput>#init.itemTypeDetails.getDepartment()#</cfoutput>
                                                                                        </cfif>" 
                        />
                    </div>
                </div>
            </fieldset>
            <div class="panel four columns">      
                <label for="check-tags" class="hidden">Tags</label>
                <input type="text" id="check-tags" name="tagList" tabindex="6" value="<cfoutput>#init.tagList#</cfoutput>" />                
            </div>
        </div>     
    
        <fieldset class="panel">
            <legend>Check Disbursement</legend>         
            <div id="rec-control" class="row">
                <div class="four columns">
                    <label for="nameSelectedStudent">Student</label>
                    <input type="text" class="student autocomplete" id="nameSelectedStudent" tabindex="7"  <!---placeholder="Student"---> />                
                    <input type="hidden" id="emplIDSelectedStudent" />       
                    <input type="hidden" id="idSelectedStudent" />              
                </div>
                <div class="one columns">                
                    <label for="term-selector">Term</label>
                    <input type="text" class="term autocomplete" id="term-selector" tabindex="8" <!---placeholder="Term"---> maxlength="4"/>                       
                </div>
                <div class="two columns">                
                    <label for="stu-amt" >Amount</label>
                    <input type="text" id="stu-amt" value="" tabindex="9" <!---placeholder="Amount"--->/>
                </div>
                <div class="two columns">            
                    <input type="checkbox" id="authDisb" tabindex="10" value="something"/>
                    <label for="authDisb">Disbursement Authorized</label>
                </div>
                <div class="two columns">   
                    <input type="checkbox" id="vouch-gen" tabindex="11" value="something"/>
                    <label for="vouch-gen">Voucher Generated</label>
                </div>
                <div class="one columns">                
                    <button type="button" id="addDisbursement"><img src="../../../images/studentfinancialaid/icons/add.png" alt="Add" /></button>                    
                </div>
            </div> 
            <div id="studentDisbursementRecords">
                <cfset total = 0 />
                <cfif StructKeyExists(init,'disbursementsJSON') and Trim(init.disbursementsJSON) neq '' and IsJSON(init.disbursementsJSON)>
                    <cfloop index="disbursement" array=#DeserializeJSON(init.disbursementsJSON)#>
                        <cfset total = total + disbursement.amount />
                        <cfoutput>
                            <div data-identifier="#disbursement.idStudent#|#disbursement.termCode#">
                                <!---<h3>#disbursement.studentName#</h3>--->
                                <div class="record row">
                                    <div class="four columns">
                                        <input type="text" value="#disbursement.studentName# <cfif Trim(disbursement.emplID) neq ''>(#Trim(disbursement.emplID)#) </cfif>" readonly="readonly" />
                                    </div>
                                    <div class="one columns">
                                        <input type="text" value="#Trim(disbursement.termCode)#" readonly="readonly" />
                                    </div>
                                    <div class="two columns">
                                        <input type="text" class="amt" value="#disbursement.amount#"/>
                                    </div>
                                    <div class="two columns">
                                        <cfif disbursement.authDisb>
                                            <input type="checkbox" class="authDisb" checked="checked" />				
                                        <cfelse>
                                            <input type="checkbox" class="authDisb" />
                                        </cfif>
                                    </div>
                                    <div class="two columns">
                                        <cfif disbursement.vouchGen>
                                            <input type="checkbox" class="vouchGen" checked="checked" />
                                        <cfelse>
                                            <input type="checkbox" class="vouchGen" />
                                        </cfif>                
                                    </div>
                                    <div class="one columns">
                                        <button type="button" class="deleteDisbursement" val="#disbursement.idStudent#|#disbursement.termCode#">
                                            <img src="../../../images/studentfinancialaid/icons/delete.png" alt="delete" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </cfoutput>
                    </cfloop>
                </cfif>
            </div>             
            <div class="row">
                <div class="two columns offset-by-five">
                    <input type="text" readonly="readonly" id="disbursementTotal" value="<cfoutput>#total#</cfoutput>" />
                </div>
            </div>  
        </fieldset>                                   
    
        <!--- Enable Comment Functionality if Deposit No. is Set --->
        <cfif variables.depositNo neq ''>
            <fieldset class="panel row">
                <legend>Notes & Comments</legend>
                <div id="comments-box" class="comments-holder clearfix">
                    <!--- Display Related Comments if Check Selected / Comments Exist --->
                    <cfif StructKeyExists(Variables,"qComments") and IsQuery(variables.qComments)>
                        <cfloop query="variables.qComments">
                            <cfoutput>
                                <div class="comment">
                                    <div class="head">
                                        <span class="user-name">#userName#</span>
                                        <span class="date">#DateFormat(date,'MMM dd, yyyy')#</span>
                                    </div>
                                    <div class="body"> <span class="comment-text"> #comment#</span></div>
                                </div>
                            </cfoutput>                	    
                        </cfloop>
                    </cfif>                
                </div>
                <div class="add-comment">
                    <h3>Enter your comment:</h3>
                    <textarea id="user-comment"></textarea>
                    <input type="button" class="awesome blue right" value="Publish Comment" id="save-comment">
                </div>
            </fieldset>
        </cfif>            
    </form> 

	<div id="debug"></div>

</cf_gui_buildpage>