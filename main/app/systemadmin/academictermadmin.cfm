<!--- set encoding --->
<cfprocessingdirective pageencoding="utf-8">

<!--- //////// show gears shell --->
<cf_gui_buildpage includeOtherJS="studentfinancialaid/default.js" includeDataTables="true" includeOtherCSS="studentfinancialaid/test.css">
	<!--- Prepare Session --->
	<cfinclude template="../utils/sessionPreparationScript.cfm" />

	<!--- Page Scope Variable Initializations --->
    <cfset variables.academicTermFormMessageBanner = '' />
    
    <!--- Process Form Submissions --->
    <cfif NOT StructIsEmpty(FORM) and StructKeyExists(FORM,"action")>
   		<cfinclude template="AcademicTermAdmin/_academicTermFormProcessor.cfm" />
    </cfif>

	<!--- code / page content goes here --->
	<script type="text/javascript">
		$(document).ready(function(){
			$('#radio').buttonset();
			
			 $( "#fromDate" ).datepicker({
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 2,
				onClose: function( selectedDate ) {
					$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
					}
			});
			$( "#toDate" ).datepicker({
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 2,
				onClose: function( selectedDate ) {
					$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
					}
			});
						
			<!------------------------------------------
						Action Handlers		
			-------------------------------------------->
			
			/******************************
			*		Data Table
			******************************/
			$("#termListings tbody tr").live({
				dblclick:  function( e ) {
					$('.row_selected').removeClass('row_selected');
					
					if ( $(this).hasClass('row_selected') ) {
						$(this).removeClass('row_selected');
					}
					else {
						$('#termListings').dataTable().$('tr.row_selected').removeClass('row_selected');
						$(this).addClass('row_selected');
						var rowData = $('#termListings').dataTable().fnGetData(this);
						$('input[name=originalTermCode]').val($.trim(rowData[0])).change();
						$('input[name=termcode]').val($.trim(rowData[0]));												
						$('select[name=semester]').val(rowData[1]);
						$('input[name=ay]').val(rowData[2]);
						$('input[name=fromDate]').val(rowData[3]);
						$('input[name=toDate]').val(rowData[4]);						
					}
					$('#selectedTermCodes').val(getSelectedTermCodes());
				},
				click: function(e){
					if(!(e.ctrlKey||e.metaKey)) {
						$('.row_selected').removeClass('row_selected');						
					}	
					$(this).toggleClass('row_selected');
					$('#selectedTermCodes').val(getSelectedTermCodes());					
				}
			});					
			
			/****************************
			*		Text Field Changes
			*****************************/
			$('input[name=originalTermCode]').live({
				change: function(){
					if($(this).val()=='') $('#save-term').attr('disabled','disabled');
					else $('#save-term').removeAttr('disabled');
				}
			});
			
			/****************************
			*		Button Clicks
			*****************************/
						
			$('#delete-term').click(function(){				
				$('.message').addClass('hidden');
				
				if(confirm("Are you sure about deleting Academic Terms?")){
					return true;
				}
				else return false;
			});				
		});
		
		function getSelectedTermCodes(){
			var selectedTerms = '';
			for(var i=0;i<$('.row_selected').length-1;i++){
				var rowData = $('#termListings').dataTable().fnGetData($('.row_selected')[i]);
				selectedTerms += $.trim(rowData[0]) + ',';					
			}				
			if($('.row_selected').length!=0) selectedTerms += $.trim($('#termListings').dataTable().fnGetData($('.row_selected')[i])[0]);								
			
			
			return selectedTerms;
		}
	</script>
    
    <!--- Print Message Banner --->
    <cfif StructKeyExists(variables,'academicTermFormProcessResult')>
    	<cfoutput>
        	<div class="#variables.academicTermFormProcessResult.type#">
            	<h3>#variables.academicTermFormProcessResult.title#</h3>
                #variables.academicTermFormProcessResult.detail#
            </div>
        </cfoutput>    
    </cfif>

    <div class="row">   
        <fieldset class="five columns panel">
            <legend>Academic Terms</legend> 
            <form id="academicTermForm" method="post" action="">   
            	<div class="row">
                    <div id="radio" class="five columns offset-by-eight">
                        <input id="option1" type="radio" name="active" value="yes" checked="checked" /><label for="option1">Active</label>
                        <input id="option2" type="radio" name="active" value="no" /><label for="option2">Disabled</label>                            
                    </div>      
                </div>
                <div class="row">
                    <div class="two columns">
                      <label class="hidden">Term Code</label>
                      <input name="termcode" type="text" style="width:100%" value="" maxlength="4" placeholder="Code"/>    
                      <input type="hidden" name="originalTermCode" value="" />         
                      <input type="hidden" id="selectedTermCodes" name="selectedTermCodes" value="" />           
                    </div>
                
                    <div class="three columns offset-by-one">
                        <label class="hidden">Semester</label>
                        <select name="semester">
                        		<option value="Semester" disabled="disabled" selected="true">Semester</option>
                            <option value="Fall">Fall</option>
                            <option value="Spring">Spring</option>
                            <option value="Summer">Summer</option>                                        
                        </select>
                    </div>
                    <div class="six columns">
                        <label class="hidden">Academic Year</label>
                        <input type="text" name="ay" value="" placeholder="Academic Year" />
                    </div>  
                </div> 
                         
                <div class="row">
                    <div class="six columns ">
                        <label for="fromFate" class="hidden">From</label>
                        <input type="text" id="fromDate" name="fromDate" placeholder="From" />
                    </div>
                    <div class="six columns ">
                        <label for="toFate" class="hidden">To</label>
                        <input type="text" id="toDate" name="toDate" placeholder="To" />                    
                    </div>
                </div>
            
                <br />
                
                <div id="controls">  
                    <button id="add-term" type="submit" class="awesome medium light green" name="action" value="add">Add</button>
                    <button id="save-term" type="submit" class="awesome medium light green" name="action" value="save" disabled="disabled">Save</button>
                    <button id="deactivate-term" type="submit" class="awesome medium light green" name="action" value="deactivate">Deactivate</button>
                    <button id="activate-term" type="submit" class="awesome medium light green" name="action" value="activate">Activate</button>
                    <button id="delete-term" type="submit" class="awesome medium light green" name="action" value="delete">Delete</button>
                </div> 
            </form>
           
        </fieldset>
                
        <div class="six columns">
            <cfset columns = arrayNew(1)/>
            <cfscript>
                z = {name="Code"}; arrayAppend(columns,z);
                z = {name="Semester"}; arrayAppend(columns,z);
                z = {name="Academic Year"}; arrayAppend(columns,z);
				z = {name="StartDate"}; arrayAppend(columns,z);
				z = {name="EndDate"}; arrayAppend(columns,z);	
				z = {name="Is Active?"}; arrayAppend(columns,z);	
				z = {name="Is In Use?"}; arrayAppend(columns,z);		
            </cfscript>
            <cf_gui_datatables 
                columns="#columns#" 
                columnSortOnLoad="0" sortDirOnLoad="DESC"
                displayRowCount="25"
                createTable="true" tableName="termListings"
                useAjax="true" requestAction="getAcademicTerms"
            />    
        </div>
	</div>    
    <div id="debug">
    </div>
    
</cf_gui_buildpage>