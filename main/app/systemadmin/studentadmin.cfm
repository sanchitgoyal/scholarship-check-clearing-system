<!--- set encoding --->
<cfprocessingdirective pageencoding="utf-8">

<!--- //////// show gears shell --->
<cf_gui_buildpage includeOtherJS="studentfinancialaid/default.js" includeOtherCSS="studentfinancialaid/plugins/jquery.tagsinput.css,studentfinancialaid/test.css" includeDataTables="true">
	<!--- code / page content goes here --->
    
	<!--- Prepare Session --->
	<cfinclude template="../utils/sessionPreparationScript.cfm" />

	<!--- Process Form Submission --->
    <cfif NOT StructIsEmpty(FORM) and StructKeyExists(FORM,'action')>
    	<cfinclude template="StudentAdmin/_studentFormProcessor.cfm" />
	</cfif>


	<!--- JQuery Main Block --->
	<script type="text/javascript">
		$(document).ready(function(){
			/******************************
			*		Initializers
			******************************/

			$( "#radio" ).buttonset();	
			
			/********* Action Handlers *******/
			
			/******************************
			*		Text Field
			******************************/
			$('#studentID').bind({
				change: function(){
					$('#save-profile').removeAttr('disabled');
				}
			});
			
			/******************************
			*		Data Table
			******************************/
			$("#studentListings tbody tr").live({
				dblclick:  function( e ) {
					
					$('#studentListings').dataTable().$('tr.row_selected').removeClass('row_selected');
					$(this).addClass('row_selected');
					
					var rowData = $('#studentListings').dataTable().fnGetData(this);
					$('#studentID').val(rowData[0]).change();
					$('#emplid').val($.trim(rowData[1]));
					$('#firstname').val(rowData[2]);												
					$('#middleName').val(rowData[3]);
					$('#lastname').val(rowData[4]);
					if(rowData[5]==false){
						$('input:radio[name=active]:nth(1)').attr('checked',true).change()
					}
					else{
						$('input:radio[name=active]:nth(0)').attr('checked',true).change()
					}
					
					$('#selectedTableRows').val(rowData[0]);					
				},
				click: function(e){
					if(!(e.ctrlKey||e.metaKey)) {
						$('.row_selected').removeClass('row_selected');	
						$('#selectedTableRows').val('');					
					}	
					$(this).toggleClass('row_selected');
					
					var rowData = $('#studentListings').dataTable().fnGetData(this);
					if($('#selectedTableRows').val() == ''){
						$('#selectedTableRows').val(rowData[0]);
					}
					else {
						$('#selectedTableRows').val($('#selectedTableRows').val()+','+rowData[0]);
					}
				}
			});			
			
			/*****************************
			*		Buttons
			*****************************/
			$('#delete-profile').bind({
				click: function(){
					$('.success,.error').addClass('hidden');					
					if(confirm("Are you sure about deleting Student?")){
						return true;
					}else return false;
				}
			});

			$('#merge-profiles').bind({
				click: function(){
					$('.success,.error').addClass('hidden');					
					if(confirm("Are you sure about merging student records?")){
						return true;
					}else return false;
				}
			});
		});
	</script>
    	
    <form action="" method="post" enctype="multipart/form-data">
    	<div id="controls">            
            <!---<input type="text" class="student" id="active-profile" /> --->
            
            <button type="submit" id="add-profile" class="awesome medium light green" name="action" value="add">Add</button>
            <button type="submit" id="save-profile" class="awesome medium light green" name="action" value="save" disabled="disabled">Save</button>
            <button type="submit" id="merge-profiles" class="awesome medium light green" name="action" value="merge">Merge</button>
            <button type="submit" id="delete-profile" class="awesome medium light green" name="action" value="delete">Delete</button>
            <button type="submit" id="export-students" class="awesome medium light green" name="action" value="export">Export</button>
        </div>
        
		<hr />
        <!--- Print a Message Banner --->
		<cfif StructKeyExists(variables,'studentFormProcessResult')>
            <cfoutput>
                <div class="#variables.studentFormProcessResult.type#">
                    <h3>#variables.studentFormProcessResult.title#</h3>
                    #variables.studentFormProcessResult.detail#
                </div>
            </cfoutput>    
        </cfif>

        <br />

        <div class="row">
            <div class="five columns">
            	<div class="panel row">
                    <div class="row">
                        <input type="hidden" id="studentID" name="studentID" value="0" />  
                        <input type="hidden" id="selectedTableRows" name="selectedTableRows" value="" />     
                    </div>
            
                    <div class="row">
                        <div class="five columns">
                            <label for="emplid" class="hidden">UND ID</label>
                            <input class="nomodify" type="text" id="emplid" name="emplid" value="" placeholder="UND ID"/>    
                        </div>
                        <div class="five columns offset-by-two">
                        
                            <div id="radio">
                                <input type="radio" id="radio1" name="active" checked="checked" value="yes" /><label for="radio1">Active</label>
                                <input type="radio" id="radio2" name="active"  value="no"/><label for="radio2">Disabled</label>
                            </div>
                        </div>                   
                    </div>
                    
                    <div class="row">        
                        <div class="four columns">
                            <label for="firstname" class="hidden">First Name</label>
                            <input class="nomodify" type="text" id="firstname" name="firstname" value="" style="width:100%" placeholder="First Name"/>
                        </div>
                        <div class="four columns">
                            <label for="lastname" class="hidden">Middle Name</label>
                            <input class="nomodify" type="text" id="middleName" name="middleName" value="" style="width:100%" placeholder="Middle Initial"/>            	
                        </div>
                        <div class="four columns">                
                            <label for="lastname" class="hidden">Last Name</label>
                            <input class="nomodify" type="text" id="lastname" name="lastname" value="" style="width:100%" placeholder="Last Name"/>
                        </div>
                    </div>
				</div>     
                <div class="panel row">
                    <div class="ten columns">
                        <input type="file" name="studentDataImportFile" id="studentDataImportFile" style="width:100%" />  
                    </div>
                    <div class="two columns">    
                        <button type="submit" class="awesome medium light green" name="action" value="import">Import</button>
                    </div>
                </div>
            </div>
			<div class="seven columns">
				<cfset columns = arrayNew(1)/>
                <cfscript>
                    z = {name="Record ID"}; arrayAppend(columns,z);
                    z = {name="Employee ID"}; arrayAppend(columns,z);
                    z = {name="First Name"}; arrayAppend(columns,z);
                    z = {name="Middle Name"}; arrayAppend(columns,z);
                    z = {name="Last Name"}; arrayAppend(columns,z);		
                    z = {name="Is Active?"}; arrayAppend(columns,z);	
                    z = {name="Is In Use?"}; arrayAppend(columns,z);		
                </cfscript>
                <cf_gui_datatables 
                    columns="#columns#" 
                    columnSortOnLoad="0" sortDirOnLoad="DESC"
                    displayRowCount="25"
                    createTable="true" tableName="studentListings"
                    useAjax="true" requestAction="getStudentListings"
                />               
            </div>
		</div>            
    </form>
</cf_gui_buildpage>