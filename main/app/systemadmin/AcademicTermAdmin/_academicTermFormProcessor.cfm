<!--- Check for Existence of Needed Session Variables --->
<cfif structKeyExists(session, "appConfig") and structKeyExists(session,"mailerObject")>    
	<cftry>
		<!--- Instantiate Academic Term Manager CFC Object --->
        <cfset academicTermManagerObject = CreateObject("component",session.appConfig.getCFCFullPath("AcademicTermManager")).init(session.application_store.getAppDSN()) />
        <!--- Instantiate Form Processing Result Struct --->
        <cfset variables.academicTermFormProcessResult = { type= "", title="", detail="" } />
    
        <!--- Process Form for Add/Save Actions if Required Fields Exist --->
        <cfif (FORM.action eq 'add' or FORM.action eq 'save') 
                and StructKeyExists(FORM,"termCode") 
                and StructKeyExists(FORM,"semester")
                and StructKeyExists(FORM,"ay")
                and StructKeyExists(FORM,"fromDate")
                and StructKeyExists(FORM,"toDate")
                and StructKeyExists(FORM,"active")>
			<!--- Instantiate Academic Term Object --->
            <cfset academicTerm = CreateObject("component",session.appConfig.getCFCFullPath("AcademicTerm"))
                                    .init(FORM.termCode,FORM.semester,FORM.ay,FORM.fromDate,FORM.toDate,FORM.active) />
            <!--- Add/Save Academic Term Data ---> 
            <cfswitch expression="#FORM.action#">
                <cfcase value="add">
                    <!--- Add Term --->
                    <cfset academicTermManagerObject.createAcademicTerm(academicTerm) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.academicTermFormProcessResult.type = "success" />
                    <cfset variables.academicTermFormProcessResult.title = "Record Created!" />
                    <cfset variables.academicTermFormProcessResult.detail = "Academic Term Record Created Successfully" />
                </cfcase>
                <cfcase value="save">
                    <!--- Save Term --->
                    <cfset academicTermManagerObject.editAcademicTerm(FORM.originalTermCode,academicTerm) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.academicTermFormProcessResult.type = "success" />
                    <cfset variables.academicTermFormProcessResult.title = "Record Saved!" />
                    <cfset variables.academicTermFormProcessResult.detail = "Academic Term Record Saved Successfully" />
                </cfcase>
            </cfswitch>    
        <cfelseif (FORM.action eq 'deactivate' or FORM.action eq 'activate' or FORM.action eq 'delete') and StructKeyExists(FORM,"selectedTermCodes")>
			<!--- Activate/Deactivate/Delete Academic Term Data --->   
            <cfswitch expression="#FORM.action#">
                <cfcase value="activate">	
                    <!--- Activate Term --->
                    <cfset academicTermManagerObject.setAcademicTermActive(FORM.selectedTermCodes,true) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.academicTermFormProcessResult.type = "success" />
                    <cfset variables.academicTermFormProcessResult.title = "Record(s) Activated!" />
                    <cfset variables.academicTermFormProcessResult.detail = "Academic Term Record(s) Activated Successfully" />
                </cfcase>
                <cfcase value="deactivate">
                    <!--- Deactivate Term --->
                    <cfset academicTermManagerObject.setAcademicTermActive(FORM.selectedTermCodes,false) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.academicTermFormProcessResult.type = "success" />
                    <cfset variables.academicTermFormProcessResult.title = "Record(s) Deactivated!" />
                    <cfset variables.academicTermFormProcessResult.detail = "Academic Term Record(s) Deactivated Successfully" />
                </cfcase>
                <cfcase value="delete">
                    <!--- Delete Term --->
                    <cfset academicTermManagerObject.deleteAcademicTerm(FORM.selectedTermCodes) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.academicTermFormProcessResult.type = "success" />
                    <cfset variables.academicTermFormProcessResult.title = "Record Deleted!" />
                    <cfset variables.academicTermFormProcessResult.detail = "Academic Term Record(s) Deleted Successfully" />
                </cfcase>                        
            </cfswitch>
        </cfif>
        
		<!--- Catch & Process Exceptions --->  
        
        <!--- Illegal Term Code --->
        <cfcatch type="IllegalTermCodeException">
            <!--- Set Result Struct ---> 
            <cfset variables.academicTermFormProcessResult.type = "error" />
            <cfset variables.academicTermFormProcessResult.title = cfcatch.Message />
            <cfset variables.academicTermFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
		<!--- Illegal Semester --->
        <cfcatch type="IllegalTermSemesterException">
            <!--- Set Result Struct --->
            <cfset variables.academicTermFormProcessResult.type = "error" />
            <cfset variables.academicTermFormProcessResult.title = cfcatch.Message />
            <cfset variables.academicTermFormProcessResult.detail = cfcatch.detail />
        </cfcatch>

        <!--- Illegal Academic Year --->
        <cfcatch type="IllegalAcademicYearException">
            <!--- Set Result Struct --->
            <cfset variables.academicTermFormProcessResult.type = "error" />
            <cfset variables.academicTermFormProcessResult.title = cfcatch.Message />
            <cfset variables.academicTermFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
        <!--- Illegal Term Begin Date --->
        <cfcatch type="IllegalTermBeginDateException">
            <!--- Set Result Struct --->
            <cfset variables.academicTermFormProcessResult.type = "error" />
            <cfset variables.academicTermFormProcessResult.title = cfcatch.Message />
            <cfset variables.academicTermFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
        <!--- Illegal Term End Date --->
        <cfcatch type="IllegalTermEndDateException">
            <!--- Set Result Struct --->
            <cfset variables.academicTermFormProcessResult.type = "error" />
            <cfset variables.academicTermFormProcessResult.title = cfcatch.Message />
            <cfset variables.academicTermFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
		<!--- Duplicate Academic Term Parameter --->
        <cfcatch type="DuplicateAcademicTermParameterException">
            <!--- Set Result Struct --->
            <cfset variables.academicTermFormProcessResult.type = "error" />
            <cfset variables.academicTermFormProcessResult.title = cfcatch.Message />
            <cfset variables.academicTermFormProcessResult.detail = cfcatch.detail />
        </cfcatch>

        <!--- Cannot Delete Academic Term --->
        <cfcatch type="CannotDeleteAcademicTermException">
            <!--- Set Result Struct --->                                
            <cfset variables.academicTermFormProcessResult.type = "error" />
            <cfset variables.academicTermFormProcessResult.title = cfcatch.Message />
            <cfset variables.academicTermFormProcessResult.detail = cfcatch.detail />
        </cfcatch>                                            
        
		<!--- Unknown Exceptions --->
        <cfcatch type="any"> 
            <!--- Inform Web Team --->
            <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
            <!--- Set Result Struct --->
            <cfset variables.academicTermFormProcessResult.type = "error" />
            <cfset variables.academicTermFormProcessResult.title = "Unknown Error!" />
            <cfset variables.academicTermFormProcessResult.detail = "Unknown error was encountered while processing your request. Web team has been notified of the issue. Please try again after sometime." />
        </cfcatch>
	</cftry>
<!--- Handle Session Issue! --->
<cfelse>
	<cfset variables.academicTermFormProcessResult.type = "error" />
    <cfset variables.academicTermFormProcessResult.title = "Session Problem!" />
    <cfset variables.academicTermFormProcessResult.detail = "There was a problem encountered with session while processing your request. Please try again after logging out and logging back in the system. If the problem persists, please contact the web team for resolution." />
</cfif>    




<!--------------------------  Workshop --------------------------------------->
<!---        
		<!--- Application Type Exceptions --->
        <cfcatch type="application">
            <cfswitch expression="#cfcatch.arg#">   
                <!--- Missing Begin Date --->
                <cfcase value="BEGINDATE">  
                    <cfset variables.academicTermFormMessageBanner = 
                        "<div class='error message'>
                            <h3>Begin Date Is A Required Field & Cannot Be Left Empty</h3>
                        </div>" />  
                    <cfset variables.academicTermFormProcessResult.type = "error" />
                    <cfset variables.academicTermFormProcessResult.title = "Missing Begin Date!" />
                    <cfset variables.academicTermFormProcessResult.detail = "Begin Date Is A Required Field & Cannot Be Left Empty" />
                </cfcase>
                <!--- Missing End Date --->
                <cfcase value="ENDDATE">    
                    <cfset variables.academicTermFormMessageBanner = 
                        "<div class='error message'>
                            <h3>End Date Is A Required Field & Cannot Be Left Empty</h3>
                        </div>" />   
                    <cfset variables.academicTermFormProcessResult.type = "error" />
                    <cfset variables.academicTermFormProcessResult.title = "Missing End Date!" />
                    <cfset variables.academicTermFormProcessResult.detail = "End Date Is A Required Field & Cannot Be Left Empty" />
                </cfcase>
                <!--- Unknown Application Exception --->
                <cfdefaultcase>
                    <!--- Inform Web Team --->
                    <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
                    <!--- Set Result Struct --->
                    <cfset variables.academicTermFormMessageBanner = 
                        "<div class='error message'>
                            Unknown error was encountered while processing your request. 
                            Web team has been notified of the issue. Please try again after sometime.
                        </div>" />   
                    <cfset variables.academicTermFormProcessResult.type = "error" />
                    <cfset variables.academicTermFormProcessResult.title = "Unknown Error!" />
                    <cfset variables.academicTermFormProcessResult.detail = "Unknown error was encountered while processing your request. Web team has been notified of the issue. Please try again after sometime." />
                </cfdefaultcase>
            </cfswitch>
        </cfcatch>
--->


