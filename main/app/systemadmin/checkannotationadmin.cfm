<!--- set encoding --->
<cfprocessingdirective pageencoding="utf-8">

<!--- //////// show gears shell --->
<cf_gui_buildpage
	includeOtherCSS="studentfinancialaid/plugins/jquery.tagsinput.css,studentfinancialaid/test.css"
    includeOtherJS="studentfinancialaid/plugins/jquery.tagsinput.js"
    includeDataTables="true"
>
	<!--- Prepare Session --->
	<cfinclude template="../utils/sessionPreparationScript.cfm" />

	<!--- Page Scope Variable Initializations --->
    <cfset variables.itemTypeFormMessageBanner = '' />

	<!--- TagManager CFC Object --->
	<cfset tagManagerObject = CreateObject("component",session.appConfig.getCFCFullPath("TagManager")).init(session.application_store.getAppDSN()) />    
    
    <!--- Process Form Submissions --->
    <cfif NOT StructIsEmpty(FORM) and StructKeyExists(FORM,"formContext")>
    	<cfif FORM.formContext eq 'tagForm'>
        	<!--- Process Check Tag Form --->
    		<cfinclude template="CheckTagAdmin/_tagFormProcessor.cfm" />
    	<cfelseif FORM.formContext eq 'itemTypeForm'>
        	<!--- Item Type Form --->
            <cfinclude template="ItemTypeAdmin/_itemTypeFormProcessor.cfm" />
        </cfif>
    </cfif>


	<script type="text/javascript">
		$(document).ready(function(){			
			/********* Initializers ***********/
			$('#tagActiveButton').buttonset();
			$('#itemTypeActiveButton').buttonset();
			$('#taglist').tabs();
			
			/********* Action Handlers *******/
			
			
			/******************************
			*		Data Table
			******************************/
			$("#itemTypeListings tbody tr").live({
				dblclick:  function( e ) {
					$('#itemTypeListings').dataTable().$('tr.row_selected').removeClass('row_selected');
					$(this).addClass('row_selected');
					
					var rowData = $('#itemTypeListings').dataTable().fnGetData(this);
					$('input[name=itemTypeOriginalCode]').val(rowData[0]).change();
					$('input[name=code]').val(rowData[0]);												
					$('input[name=desc]').val(rowData[1]);
					$('input[name=fund]').val(rowData[2]);
					$('input[name=dept]').val(rowData[3]);
					$('input[name=acc]').val(rowData[4]);
					if(rowData[5]) $('input:radio[name=itemActive]:nth(0)').attr('checked',true).change();		
					else $('input:radio[name=itemActive]:nth(1)').attr('checked',true).change();	
					
					$('input[name=selectedItemTypes]').val(rowData[0]);					
				},
				click: function(e){
					if(!e.ctrlKey) {
						$('.row_selected').removeClass('row_selected');		
						$('input[name=selectedItemTypes]').val('');				
					}	
					$(this).toggleClass('row_selected');
					
					var rowData = $('#itemTypeListings').dataTable().fnGetData(this);
					if($('#selectedItemTypes').val() == ''){
						$('#selectedItemTypes').val(rowData[0]);
					}
					else {
						$('#selectedItemTypes').val($('#selectedItemTypes').val()+','+rowData[0]);
					}				
				}
			});			
			
			/***************************
			*		Text Fields 
			***************************/

			$('#originalCheckTag').live({
				change: function(){
					if($.trim($(this).val())!=''){
						$('#saveTags').removeAttr('disabled');
					}
				}
			});		
				
			$('#itemTypeOriginalCode').live({
				change: function(){
					if($.trim($(this).val())!=''){
						$('#save-type').removeAttr('disabled');
					}
				}
			});	
						
			/****************************
			*		Select Fields
			****************************/
			$('select').bind({
				dblclick: function(){
					$('#selectedTag').val($(this).children(':selected').html());
					$('#originalCheckTag').val($(this).children(':selected').html()).change();
					if($(this).children(':selected').data('active')){
						$('input:radio[name=active]:nth(0)').attr('checked',true).change();
					}
					else $('input:radio[name=active]:nth(1)').attr('checked',true).change();
				}
			});
			
			/*****************************
			*		Buttons
			*****************************/
			$('#delete-type').bind({
				click: function(){
					$('.success,.error').addClass('hidden');					
					if(confirm("Are you sure about deleting Item Type?")){
						return true;
					}else return false;
				}
			});
			
			$('#deleteTags').bind({
				click: function(){
					$('.success,.error').addClass('hidden');					
					if(confirm("Are you sure about deleting Tag?")){
						return true;
					}else return false;
				}
			});
			
			
			$('[placeholder]').focus(function() {
			  var input = $(this);
			  if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			  }
			}).blur(function() {
			  var input = $(this);
			  if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			  }
			}).blur();
			
			$('[placeholder]').parents('form').submit(function() {
			  $(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
				  input.val('');
				}
			  })
			});
		});
	</script>

	<!--- code / page content goes here ---> 


	<!--- Publish Message Banner --->
    <cfif StructKeyExists(variables,'checkTagFormProcessResult')>
    	<cfoutput>
        	<div class="#variables.checkTagFormProcessResult.type#">
            	<h3>#variables.checkTagFormProcessResult.title#</h3>
                #variables.checkTagFormProcessResult.detail#
            </div>
        </cfoutput>    
    </cfif>

<form action="" method="post" >
	<input type="hidden" name="formContext" value="tagForm" />
	<div class="row">
            <fieldset class="panel five columns">
            	<legend>Check Tags</legend>
                <div class="row">
                    <div class="seven columns">
                        <input type="text" id="selectedTag" name="selectedTag" style="text-align:center" />
                        <input type="hidden" id="originalCheckTag" name="originalCheckTag" />
                    </div>
                    <div class="five columns">
						<div id="tagActiveButton">
                            <input type="radio" id="option1" name="active" value="yes" checked="checked" /><label for="option1">Active</label>
                            <input type="radio" id="option2" name="active" value="no" /><label for="option2">Disabled</label>                    
                        </div>    
                    </div>
				</div>
                <br /><br />
                <div id="controls">            
                    <button id="addTags" name="action" value="add" type="submit" class="awesome medium light green">Add</button>
                    <button id="saveTags" name="action" value="save" class="awesome medium light green" disabled="disabled">Save</button>
                    <button id="deactivateTags" name="action" value="deactivate" class="awesome medium light green">Deactivate</button>
                    <button id="activatetags" name="action" value="activate" class="awesome medium light green">Activate</button>
                    <button id="deleteTags" name="action" value="delete" class="awesome medium light green">Delete</button> 
                </div>          
            </fieldset>
               
        <div class="six columns" id="taglist" >
             <ul>
                <li><a href="#activeTags">Active Tags</a></li>
                <li><a href="#deactivatedTags">Deactivated tags</a></li>	           
            </ul>
            
            <div id="activeTags">            	
            	<cfset qActiveTags = tagManagerObject.getTagsAsQuery(true) />
                <select size="4" style="width:100%" multiple="multiple" name="selectedTags">
					<cfloop query="qActiveTags">
                    	<cfoutput><option value="#tag#" data-active="true">#tag#</option></cfoutput>
                    </cfloop>      
                </select>
            </div>
            
            <div id="deactivatedTags">
            	<cfset qInActiveTags = tagManagerObject.getTagsAsQuery(false) />
                <select size="15" style="width:100%" multiple="multiple" name="selectedTags">
					<cfloop query="qInActiveTags">
                    	<cfoutput><option value="#tag#" data-active="false">#tag#</option></cfoutput>
                    </cfloop> 
                </select>                 
            </div>                
        </div>
	</div>
</form>     

<!--- //////////////// --->

	<br />
	<hr />
    <br />

<!--- //////////////// --->
	
	<!--- Publish Message Banner --->
    <cfif StructKeyExists(variables,'itemTypeFormProcessResult')>
    	<cfoutput>
        	<div class="#variables.itemTypeFormProcessResult.type#">
            	<h3>#variables.itemTypeFormProcessResult.title#</h3>
                #variables.itemTypeFormProcessResult.detail#
            </div>
        </cfoutput>     
    </cfif>

    <div class="row">
    	<form action="" method="post">
			<input type="hidden" name="formContext" value="itemTypeForm" />
            <input type="hidden" name="itemTypeOriginalCode" id="itemTypeOriginalCode" value="" />
            <input type="hidden" id="selectedItemTypes" name="selectedItemTypes" value="" />
            <fieldset class="panel five columns">
                <legend>Item Types</legend>  
				<div class="row">
                    <div class="five columns offset-by-seven">
						<div id="itemTypeActiveButton">
                            <input type="radio" id="opt1" name="itemActive" value="yes" checked="checked" /><label for="opt1">Active</label>
                            <input type="radio" id="opt2" name="itemActive" value="no" /><label for="opt2">Disabled</label>                    
                        </div>    
                    </div>
                </div>
                <div class="row">              
                    <div class="four columns">
                        <label class="hidden">Code</label>
                        <input type="text" class="nomodify" name="code" value="" placeholder="Code"/>
                    </div>
                    <div class="eight columns">
                        <label class="hidden">Description</label>
                        <input class="nomodify" type="text" name="desc" value="" placeholder="Description" />
                    </div>
				</div>
                <div class="row">
                	<div class="four columns">
                        <label class="hidden">Fund</label>
                        <input class="nomodify" type="text" name="fund" value="" placeholder="Fund" />
					</div>
                    <div class="four columns">
                        <label class="hidden">Department</label>
                        <input class="nomodify" type="text" name="dept" value="" placeholder="Department" />
					</div>
                    <div class="four columns">
                        <label class="hidden">Account</label>
                        <input class="nomodify" type="text" name="acc" value="" placeholder="Account" />
					</div>
                </div>
                
                <br />
                
                <div id="controls">            
                    <button id="add-type" type="submit" name="action" value="add" class="awesome medium light green">Add</button>
                    <button id="save-type" type="submit" name="action" value="save" class="awesome medium light green" disabled="disabled">Save</button>
                    <button id="deactivate-type" type="submit" name="action" value="deactivate" class="awesome medium light green">Deactivate</button>
                    <button id="activate-type" type="submit" name="action" value="activate" class="awesome medium light green">Activate</button>
                    <button id="delete-type" type="submit" name="action" value="delete" class="awesome medium light green">Delete</button> 
                </div>
            </fieldset>
        </form>
        
        <div class="six columns">
			<cfset columns = arrayNew(1)/>
            <cfscript>
                z = {name="Code"}; arrayAppend(columns,z);
                z = {name="Description"}; arrayAppend(columns,z);
                z = {name="Fund"}; arrayAppend(columns,z);
                z = {name="Department"}; arrayAppend(columns,z);		
                z = {name="Account"}; arrayAppend(columns,z);	
				z = {name="Is Active?"}; arrayAppend(columns,z);	
				z = {name="In Use?"}; arrayAppend(columns,z);			
            </cfscript>
            <cf_gui_datatables 
                columns="#columns#" 
                columnSortOnLoad="0" sortDirOnLoad="DESC"
                <!---displayRowCount="25"--->
                createTable="true" tableName="itemTypeListings"
                useAjax="true" requestAction="getItemTypeDetails"
            />        
        </div>
    </div> 

</cf_gui_buildpage>