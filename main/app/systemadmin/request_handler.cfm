<!--- //////// Process request --->
<cf_util_getpostdata result="data"/>
<!--- perform given action --->

<cfif structKeyExists(data,'action')>
	<cfoutput>
	<cfset data.sessionUserID = session.userID />
	<cfswitch expression="#data.action#">
		<cfcase value="getAllChecks">
			<cfscript>
				getAllChecks(data);
			</cfscript>
		</cfcase>
		<cfcase value="getAcademicTerms">
			<cfscript>
				getAcademicTerms(data);
			</cfscript>
		</cfcase>        
        <cfcase value="getItemTypeDetails">
			<cfscript>
				getItemTypeDetails(data);
			</cfscript>        	
        </cfcase>
        <cfcase value="getStudentListings">
			<cfscript>
				getStudentListings(data);
			</cfscript>        	
        </cfcase>        
        			
		<cfdefaultcase>
			ERROR: Unknown action.
		</cfdefaultcase>		
		
	</cfswitch>
	</cfoutput>
<cfelse>
	ERROR: no action was provided.
</cfif>


<!--- //////// --->

<cffunction name="getAllChecks">
	<cfargument name="data" type="struct" required="yes" />
	<cfset var columns = arrayNew(1) />
	<cfset var z = structNew() />
	
	<cfscript>
		z = {name = 'DepositDate'};
		arrayAppend(columns, z);
		z = {name = 'DepositNumber'};
		arrayAppend(columns, z);
		z = {name = 'CheckNumber'};
		arrayAppend(columns, z);
		z = {name = 'ItemType'};
		arrayAppend(columns, z);
		z = {name = 'ItemDesc'};
		arrayAppend(columns, z);		
	</cfscript>
	
	<cftry>
		<cf_util_datatablesresponse columns="#columns#" data="#data#" dataSource="software_licensing" result="dTableResponse">
			<cfoutput>
                SELECT 	chk.depositedOn AS DepositedDate, 
                        chk.depositNumber AS DepositNumber, 
                        chk.checkNumber AS CheckNumber, 
                        chk.itemTypeCode AS ItemType, 
                        itype.description AS ItemDesc
                FROM und_finaid.check chk, und_finaid.itemtypes itype
                HAVING chk.itemTypeCode = itype.itemTypeCode			
			</cfoutput>
		</cf_util_datatablesresponse>
		
		<cfoutput>true|#serializeJSON(dTableResponse)#</cfoutput>

		<cfcatch type="any">
			<cfoutput>false|<cfdump var="#cfcatch#"/></cfoutput>
		</cfcatch>
	</cftry>
</cffunction>


<cffunction name="getItemTypeDetails">
	<cfargument name="data" type="struct" required="yes" />
	<cfset var columns = arrayNew(1) />
	<cfset var z = structNew() />
	
	<cfscript>
		z = {name = 'Code'};
        z.combinedFields = ['itemTypeCode'];
		arrayAppend(columns, z);

		z = {name = 'Description'};
		arrayAppend(columns, z);
		z = {name = 'Fund'};
		arrayAppend(columns, z);
		z = {name = 'Department'};
		arrayAppend(columns, z);
		z = {name = 'Account'};
		arrayAppend(columns, z);	
		z = {name = 'Active'};
		arrayAppend(columns, z);	
		z = {name = 'inUse', searchable='false'};
		arrayAppend(columns, z);		 						
	</cfscript>
	
	<cftry>
		<cf_util_datatablesresponse columns="#columns#" data="#data#" dataSource="#session.application_store.getAppDSN()#" result="dTableResponse">
			<cfoutput>
                SELECT itemTypeCode AS Code, description, fund, department, account, "yes" as inUse,
                        CASE WHEN active = '1' THEN 'yes' ELSE 'no' END  AS active
                FROM und_finaid.itemtypes itype
                WHERE EXISTS 
                (
                    SELECT depositNumber
                    FROM und_finaid.check chk
                    WHERE chk.itemTypeCode = itype.itemTypeCode
                )
                UNION
                SELECT itemTypeCode AS Code, description, fund, department, account, "no" as inUse,
                        CASE WHEN active = '1' THEN 'yes' ELSE 'no' END  AS active
                FROM und_finaid.itemtypes itype
                WHERE NOT EXISTS 
                (
                    SELECT depositNumber
                    FROM und_finaid.check chk
                    WHERE chk.itemTypeCode = itype.itemTypeCode
                )			
			</cfoutput>
		</cf_util_datatablesresponse>
		
		<cfoutput>true|#serializeJSON(dTableResponse)#</cfoutput>

		<cfcatch type="any">
			<cfoutput>false|<cfdump var="#cfcatch#"/></cfoutput>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name="getAcademicTerms">
	<cfargument name="data" type="struct" required="yes" />
	<cfset var columns = arrayNew(1) />
	<cfset var z = structNew() />
	<cfscript>
		z = {name = 'termCode'};
		arrayAppend(columns, z);
		z = {name = 'Semester'}; 
		arrayAppend(columns, z);
		z = {name = 'AcademicYear'};
		arrayAppend(columns, z);
		z = {name = 'StartDate', searchable="false"};
		arrayAppend(columns, z);
		z = {name = 'EndDate', searchable="false"};
		arrayAppend(columns, z);
		z = {name = 'Active'};
		arrayAppend(columns, z);
		z = {name = 'inUse', searchable="false"};
		arrayAppend(columns, z);		 						
	</cfscript>
	
	<cftry>
		<cf_util_datatablesresponse columns="#columns#" data="#data#" dataSource="#session.application_store.getAppDSN()#" result="dTableResponse">
			<cfoutput>
                SELECT termcode, semester, academicyear, 
                    date_format(beginDate,'%m/%d/%Y') as startDate, date_format(endDate,'%m/%d/%Y') as endDate, "yes" as inUse,		
                    CASE WHEN active = '1' THEN 'yes' ELSE 'no' END  AS active
                FROM und_finaid.academicterm term
                WHERE EXISTS 
                (
                    SELECT depositNumber
                    FROM und_finaid.checkDisbursement chkDisb
                    WHERE chkDisb.termCode = term.termCode
                )
                
                UNION
                
                SELECT termcode, semester, academicyear, 
                    date_format(beginDate,'%m/%d/%Y') as startDate, date_format(endDate,'%m/%d/%Y') as endDate, "no" as inUse,	
                    CASE WHEN active = '1' THEN 'yes' ELSE 'no' END  AS active
                FROM und_finaid.academicterm term
                WHERE NOT EXISTS 
                (
                    SELECT depositNumber
                    FROM und_finaid.checkDisbursement chkDisb
                    WHERE chkDisb.termCode = term.termCode
                )
			</cfoutput>
		</cf_util_datatablesresponse>
        
        <cf_util_dTablesResponseLoop data="#dTableResponse#">
			<cfscript>TermCode = ' #TermCode#';</cfscript>        	        	
        </cf_util_dTablesResponseLoop>        
        
		<cfoutput>true|#serializeJSON(dTableResponse)#</cfoutput>

		<cfcatch type="any">
			<cfoutput>false|<cfdump var="#cfcatch#"/></cfoutput>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction name="getStudentListings">
	<cfargument name="data" type="struct" required="yes" />
	<cfset var columns = arrayNew(1) />
	<cfset var z = structNew() />

	<cfscript>
		z = {name = 'idStudent'};
		arrayAppend(columns, z);
		z = {name = 'emplID'}; 
		arrayAppend(columns, z);
		z = {name = 'firstName'};
		arrayAppend(columns, z);
		z = {name = 'middleName'};
		arrayAppend(columns, z);
		z = {name = 'lastName'};
		arrayAppend(columns, z);
		z = {name = 'Active'};
		arrayAppend(columns, z);		
		z = {name = 'inUse', searchable="false"};
		arrayAppend(columns, z);								
	</cfscript>
	
	<cftry>
		<cf_util_datatablesresponse columns="#columns#" data="#data#" dataSource="#session.application_store.getAppDSN()#" result="dTableResponse">
			<cfoutput> 
                SELECT idStudent, emplID, firstName, middleName, lastName, 
                        CASE WHEN active = 1 THEN 'yes' ELSE 'no' END  AS active, 
                        CASE WHEN count(*) > 0 THEN 'yes' ELSE 'no' END AS inUse
                FROM und_finaid.student LEFT JOIN und_finaid.checkdisbursement USING (idStudent)
                WHERE true
                group by idStudent
            </cfoutput>      
		</cf_util_datatablesresponse>
        
        <cf_util_dTablesResponseLoop data="#dTableResponse#">
			<cfscript>emplID = ' #emplID#';</cfscript>        	        	
        </cf_util_dTablesResponseLoop>

		<cfoutput>true|#serializeJSON(dTableResponse)#</cfoutput> 

		<cfcatch type="any">
			<cfoutput>false|<cfdump var="#cfcatch#"/></cfoutput>
		</cfcatch>
	</cftry>
</cffunction>
