<!--- Check for Existence of Needed Session Variables --->
<cfif structKeyExists(session, "appConfig") and structKeyExists(session,"mailerObject")>    
	<cftry>		
		<!--- Instantiate TagManager CFC Object --->
        <cfset tagManagerObject = CreateObject("component",session.appConfig.getCFCFullPath("TagManager")).init(session.application_store.getAppDSN()) />
        <!--- Form Processing Result Struct --->
        <cfset variables.checkTagFormProcessResult = { type= "", title="", detail="" } />

        <!--- Process Form for Add/Save Actions if Required Fields Exist --->
        <cfif (FORM.action eq 'add' or FORM.action eq 'save') and StructKeyExists(FORM,"selectedTag") and StructKeyExists(FORM,"active")>
            <!--- Instantiate CheckTag Object --->
            <cfset checkTag = CreateObject("component",session.appConfig.getCFCFullPath("CheckTag")).init(selectedTag,active) />
            <!--- Add/Save Tag Data to DB --->
            <cfswitch expression="#FORM.action#">
                <cfcase value="add">
                	<!--- Create New Tag --->
                    <cfset tagManagerObject.createCheckTag(checkTag) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.checkTagFormProcessResult.type = "success" />
                    <cfset variables.checkTagFormProcessResult.title = "Record Created!" />
                    <cfset variables.checkTagFormProcessResult.detail = "New Check Tag Created Successfully In The Database" />
                </cfcase>
                <cfcase value="save">
                	<!--- Save Tag Data --->
                    <cfset tagManagerObject.editCheckTag(FORM.originalCheckTag,checkTag) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.checkTagFormProcessResult.type = "success" />
                    <cfset variables.checkTagFormProcessResult.title = "Record Saved!" />
                    <cfset variables.checkTagFormProcessResult.detail = "Check Tag Record Saved Successfully To The Database" />
                </cfcase>
            </cfswitch>
        <cfelseif (FORM.action eq 'deactivate' or FORM.action eq 'activate' or FORM.action eq 'delete') and StructKeyExists(FORM,"selectedTags")>
            <!--- Activate/Deactivate/Delete Tag Data --->   
            <cfswitch expression="#FORM.action#">
                <cfcase value="activate">	
                    <!--- Activate Tag(s) --->
					<cfset tagManagerObject.setCheckTagActive(FORM.selectedTags,true) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.checkTagFormProcessResult.type = "success" />
                    <cfset variables.checkTagFormProcessResult.title =  "Record(s) Activated!" />
                    <cfset variables.checkTagFormProcessResult.detail = "Check Tag Record(s) Activated Successfully" />
                </cfcase>
                <cfcase value="deactivate">
                    <!--- DeActivate Tags --->
					<cfset tagManagerObject.setCheckTagActive(FORM.selectedTags,false) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.checkTagFormProcessResult.type = "success" />
                    <cfset variables.checkTagFormProcessResult.title =  "Record(s) DeActivated!" />
                    <cfset variables.checkTagFormProcessResult.detail = "Check Tag Record(s) DeActivated Successfully" />
                </cfcase>
                <cfcase value="delete">
                	<!--- Delete Tag --->
                    <cfset tagManagerObject.deleteCheckTags(FORM.selectedTags) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.checkTagFormProcessResult.type = "success" />
                    <cfset variables.checkTagFormProcessResult.title =  "Record(s) Deleted!" />
                    <cfset variables.checkTagFormProcessResult.detail = "Check Tag Record(s) Deleted Successfully" />
                </cfcase>                        
            </cfswitch>
        </cfif>    
        <!--- Catch & Process Exceptions --->
        <cfcatch type="IllegalTagTextException">
            <!--- Set Result Struct --->
            <cfset variables.checkTagFormProcessResult.type = "error" />
            <cfset variables.checkTagFormProcessResult.title = cfcatch.Message />
            <cfset variables.checkTagFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
        <!--- Duplicate Check Tag --->
        <cfcatch type="DuplicateCheckTagException">
            <!--- Set Result Struct --->
            <cfset variables.checkTagFormProcessResult.type = "error" />
            <cfset variables.checkTagFormProcessResult.title = cfcatch.Message />
            <cfset variables.checkTagFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
        <!--- Cannot Delete Check Tag --->
        <cfcatch type="CannotDeleteCheckTagException">
            <!--- Set Result Struct --->
            <cfset variables.checkTagFormProcessResult.type = "error" />
            <cfset variables.checkTagFormProcessResult.title = cfcatch.Message />
            <cfset variables.checkTagFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
		<!--- Unknown Exceptions --->
        <cfcatch type="any"> 
            <!--- Inform Web Team --->
            <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
            <!--- Set Result Struct --->
            <cfset variables.checkTagFormProcessResult.type = "error" />
            <cfset variables.checkTagFormProcessResult.title = "Unknown Error!" />
            <cfset variables.checkTagFormProcessResult.detail = "Unknown error was encountered while processing your request. Web team has been notified of the issue. Please try again after sometime." />
        </cfcatch>
	</cftry>        
<!--- Handle Session Issue! --->
<cfelse>
	<cfset variables.checkTagFormProcessResult.type = "error" />
    <cfset variables.checkTagFormProcessResult.title = "Session Problem!" />
    <cfset variables.checkTagFormProcessResult.detail = "There was a problem encountered with session while processing your request. Please try again after logging out and logging back in the system. If the problem persists, please contact the web team for resolution." />
</cfif>
<!---
<cfdump var="#FORM#" />
<cfdump var="#variables#" />
<cfabort />
--->
