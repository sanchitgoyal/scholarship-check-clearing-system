<!--- Check for Existence of Needed Session Variables --->
<cfif structKeyExists(session, "appConfig") and structKeyExists(session,"mailerObject")>    
	<cftry>
        <!--- Instantiate Student Manager Object --->
        <cfset studentManagerObject = CreateObject("component",session.appConfig.getCFCFullPath("StudentManager")).init(session.application_store.getAppDSN()) />
        <!--- Instantiate Form Processing Result Struct --->
        <cfset variables.studentFormProcessResult = { type= "", title="", detail="" } />
        
        <!--- Save/Add Actions --->
        <cfif FORM.action eq 'add' or FORM.action eq 'save'>
			<cfset studentObject = CreateObject("component",session.appConfig.getCFCFullPath("Student"))
                                    .init(
                                        studentID=FORM.studentID,
                                        emplID=FORM.emplID,
                                        firstName=FORM.firstName,
                                        middleName=FORM.middleName,										
                                        lastName=FORM.lastName,
                                        active=FORM.active																		
                                    ) />
            <cfswitch expression="#FORM.action#">
                <cfcase value="add">
                	<!--- Add Action --->
                    <cfset studentManagerObject.createStudentRecord(studentObject) />
                    <cfset variables.studentFormProcessResult.type = "success" /> 
                    <cfset variables.studentFormProcessResult.title = "Record Created!" /> 
                    <cfset variables.studentFormProcessResult.detail = "Student Record Created Successfully" /> 
                </cfcase>
                <cfcase value="save">
                	<!--- Save Action --->
                    <cfset studentManagerObject.editStudentRecord(studentObject) />
                    <cfset variables.studentFormProcessResult.type = "success" /> 
                    <cfset variables.studentFormProcessResult.title = "Record Saved!" /> 
                    <cfset variables.studentFormProcessResult.detail = "Student Record Saved Successfully" /> 
                </cfcase>
            </cfswitch>
        <!--- Delete Action --->    
        <cfelseif FORM.action eq 'delete' and StructKeyExists(FORM,'selectedTableRows')>
			<cfset studentManagerObject.deleteStudentRecord(FORM.selectedTableRows) />
			<cfset variables.studentFormProcessResult.type = "success" /> 
            <cfset variables.studentFormProcessResult.title = "Record(s) Deleted!" /> 
            <cfset variables.studentFormProcessResult.detail = "Student Record(s) Deleted Successfully" />
        <!--- Import Action --->     
        <cfelseif FORM.action eq 'import'>
            <!--- Check for Coldfusion 9 --->
            <cfif listGetAt(server.coldfusion.productversion, 1) EQ '9'>
				<!--- Check for Valid File Upload --->
                <cfif StructKeyExists(FORM,"studentDataImportFile") and Len(FORM.studentDataImportFile) neq 0>
					<!--- Successful Upload --->
                    <cfset tempDirectory = GetTempDirectory() />
                    <cffile action="upload" destination="#tempDirectory#" filefield="studentDataImportFile" result="uploadResult" nameconflict="makeunique">
					<cfif uploadResult.fileWasSaved>
						<!--- Successful Save --->
                        <cfset importResult = studentManagerObject.importStudentRecords(importFile=uploadResult.serverDirectory & "/" & uploadResult.serverFile) />
						<!--- Set Result Struct --->
						<cfset variables.studentFormProcessResult.type = "success" /> 
                        <cfset variables.studentFormProcessResult.title = "#importResult.inserted + importResult.updated# Record(s) Imported!" /> 
                        <cfset variables.studentFormProcessResult.detail = "#importResult.inserted + importResult.updated# Student Record(s) Imported Successfully. #importResult.inserted# new record(s) were inserted and #importResult.updated# record(s) were updated." /> 
                    <cfelse>
						<!--- Unsuccessful Save --->
						<!--- Set Result Struct --->
						<cfset variables.studentFormProcessResult.type = "error" /> 
                        <cfset variables.studentFormProcessResult.title = "Fatal Error!" /> 
                        <cfset variables.studentFormProcessResult.detail = "An unexpected problem encountered by the system prevented it from successfully uploading the file." /> 
                    </cfif>
                <cfelse>
                	<!--- Unsuccessful Upload --->
					<cfset variables.studentFormProcessResult.type = "error" /> 
                    <cfset variables.studentFormProcessResult.title = "Missing Import File Information!" /> 
                    <cfset variables.studentFormProcessResult.detail = "Please upload a correctly formatted excel file (.xls) for student data import." /> 
                </cfif>
            <cfelse>
            	<!--- Unsupported Coldufusion Version --->
				<cfset variables.studentFormProcessResult.type = "error" /> 
                <cfset variables.studentFormProcessResult.title = "Unsupported Server Action!" /> 
                <cfset variables.studentFormProcessResult.detail = "Sorry this version of server technology does not support import functionality. Please Contact Web Team for resolution of the problem." /> 
            </cfif>         
        <cfelseif FORM.action eq 'export'>
        	<!--- Export Action --->
            <cfif listGetAt(server.coldfusion.productversion, 1) EQ '9'>
				<!--- Check for Coldfusion 9 --->
				<cfset exportSpreadSheet = studentManagerObject.exportStudentRecords() />
                <cfheader name="Content-Disposition" value="attachment; filename=StudentExport_#dateFormat(now(), 'short')#.xls">
                <cfcontent type="application/vnd.msexcel" variable="#SpreadSheetReadBinary(exportSpreadSheet)#">
            <cfelse>
            	<!--- Unsupported Coldufusion Version --->
				<cfset variables.studentFormProcessResult.type = "error" /> 
                <cfset variables.studentFormProcessResult.title = "Unsupported Server Action!" /> 
                <cfset variables.studentFormProcessResult.detail = "Sorry this version of server technology does not support student record export functionality. Please contact web team for resolution of the problem." /> 
            </cfif>         
        <cfelseif FORM.action eq 'merge'and StructKeyExists(FORM,'selectedTableRows')>
			<!--- Merge Action --->    
			<cfset studentManagerObject.mergeStudentRecords(FORM.selectedTableRows) />
            <!--- Set Result Set --->
			<cfset variables.studentFormProcessResult.type = "success" /> 
            <cfset variables.studentFormProcessResult.title = "Records Merged!" /> 
            <cfset variables.studentFormProcessResult.detail = "Student Records Merged Successfully" />
        </cfif>
        
        
		<!--- Catch & Process Exceptions --->  
        
        <!--- Illegal Student Record ID --->
        <cfcatch type="IllegalStudentRecordIDFormatException">
            <!--- Set Result Struct ---> 
            <cfset variables.studentFormProcessResult.type = "error" />
            <cfset variables.studentFormProcessResult.title = cfcatch.Message />
            <cfset variables.studentFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
        <!--- Illegal Student EmplID --->
        <cfcatch type="IllegalEmplIDFormatException">
            <!--- Set Result Struct ---> 
            <cfset variables.studentFormProcessResult.type = "error" />
            <cfset variables.studentFormProcessResult.title = cfcatch.Message />
            <cfset variables.studentFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
        <!--- Illegal Student First Name --->
        <cfcatch type="IllegalStudentFirstNameFormatException">
            <!--- Set Result Struct ---> 
            <cfset variables.studentFormProcessResult.type = "error" />
            <cfset variables.studentFormProcessResult.title = cfcatch.Message />
            <cfset variables.studentFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
        <!--- Illegal Student Middle Name --->
        <cfcatch type="IllegalStudentMiddleNameFormatException">
            <!--- Set Result Struct ---> 
            <cfset variables.studentFormProcessResult.type = "error" />
            <cfset variables.studentFormProcessResult.title = cfcatch.Message />
            <cfset variables.studentFormProcessResult.detail = cfcatch.detail />
        </cfcatch>

        <!--- Illegal Student Last Name --->
        <cfcatch type="IllegalStudentLastNameFormatException">
            <!--- Set Result Struct ---> 
            <cfset variables.studentFormProcessResult.type = "error" />
            <cfset variables.studentFormProcessResult.title = cfcatch.Message />
            <cfset variables.studentFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
        <!--- Cannot Edit Student Record --->
        <cfcatch type="CannotEditStudentRecordException">
            <!--- Set Result Struct ---> 
            <cfset variables.studentFormProcessResult.type = "error" />
            <cfset variables.studentFormProcessResult.title = cfcatch.Message />
            <cfset variables.studentFormProcessResult.detail = cfcatch.detail />
        </cfcatch>

        <!--- Cannot Delete Student Record --->
        <cfcatch type="CannotDeleteStudentRecordException">
            <!--- Set Result Struct ---> 
            <cfset variables.studentFormProcessResult.type = "error" />
            <cfset variables.studentFormProcessResult.title = cfcatch.Message />
            <cfset variables.studentFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
        <!--- Cannot Merge Student Records --->
        <cfcatch type="CannotMergeStudentRecordsException">
            <!--- Set Result Struct ---> 
            <cfset variables.studentFormProcessResult.type = "error" />
            <cfset variables.studentFormProcessResult.title = cfcatch.Message />
            <cfset variables.studentFormProcessResult.detail = cfcatch.detail />
        </cfcatch>

        <!--- Illegal Import File Format --->
        <cfcatch type="IllegalImportFileFormatException">
            <!--- Set Result Struct ---> 
            <cfset variables.studentFormProcessResult.type = "error" />
            <cfset variables.studentFormProcessResult.title = cfcatch.Message />
            <cfset variables.studentFormProcessResult.detail = cfcatch.detail />
        </cfcatch>
        
		<!--- Unknown Exceptions --->
        <cfcatch type="any"> 
            <!--- Inform Web Team --->
            <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
            <!--- Set Result Struct --->
            <cfset variables.studentFormProcessResult.type = "error" />
            <cfset variables.studentFormProcessResult.title = "Unknown Error!" />
            <cfset variables.studentFormProcessResult.detail = "Unknown error was encountered while processing your request. Web team has been notified of the issue. Please try again after sometime." />
        </cfcatch>
	</cftry>
<!--- Handle Session Issue! --->
<cfelse>
	<!--- Set Result Struct --->
	<cfset variables.studentFormProcessResult.type = "error" />
    <cfset variables.studentFormProcessResult.title = "Session Problem!" />
    <cfset variables.studentFormProcessResult.detail = "There was a problem encountered with session while processing your request. Please try again after logging out and logging back in the system. If the problem persists, please contact the web team for resolution." />
</cfif>