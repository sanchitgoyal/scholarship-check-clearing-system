<!--- Check for Existence of Needed Session Variables --->
<cfif structKeyExists(session, "appConfig") and structKeyExists(session,"mailerObject")>    
    <cftry>
        <!--- ItemTypeManager CFC Object --->
        <cfset itemTypeManagerObject = CreateObject("component",session.appConfig.getCFCFullPath("ItemTypeManager")).init(session.application_store.getAppDSN()) />
        <!--- Instantiate Form Processing Result Struct --->
        <cfset variables.itemTypeFormProcessResult = { type= "", title="", detail="" } />
        
        <!--- Process Form Based on Actions Chosen By User --->
        <cfif (FORM.action eq 'add' or FORM.action eq 'save') 
            and StructKeyExists(FORM,"code") 
            and StructKeyExists(FORM,"desc")
            and StructKeyExists(FORM,"dept")
            and StructKeyExists(FORM,"fund")
            and StructKeyExists(FORM,"acc")
            and StructKeyExists(FORM,"itemActive")>
        
            <!--- Instantiate Item Type Object --->
            <cfset itemType = CreateObject("component",session.appConfig.getCFCFullPath("ItemType")).init(FORM.code,FORM.desc,FORM.fund,FORM.dept,FORM.acc,FORM.itemActive) />
            <!--- Add/Save Item Type Data --->
            <cfswitch expression="#FORM.action#">
                <cfcase value="add">
                	<!--- Add Item Type --->
                    <cfset itemTypeManagerObject.createItemType(itemType) />
                    <!--- Set Result Struct ---> 
					<cfset variables.itemTypeFormProcessResult.type = "success" />
                    <cfset variables.itemTypeFormProcessResult.title = "Record Created!" />
                    <cfset variables.itemTypeFormProcessResult.detail = "Item Type Record Created Successfully" />
                </cfcase>
                <cfcase value="save">
                	<!--- Save Item Type --->
                    <cfset itemTypeManagerObject.editItemType(FORM.itemTypeOriginalCode,itemType) />
                    <!--- Set Result Struct ---> 
					<cfset variables.itemTypeFormProcessResult.type = "success" />
                    <cfset variables.itemTypeFormProcessResult.title = "Record Saved!" />
                    <cfset variables.itemTypeFormProcessResult.detail = "Item Type Record Saved Successfully" />
                </cfcase>
            </cfswitch>
        
        <cfelseif (FORM.action eq 'deactivate' or FORM.action eq 'activate' or FORM.action eq 'delete') and StructKeyExists(FORM,"selectedItemTypes")>
            <!--- Activate/Deactivate/Delete Item Types --->   
            <cfswitch expression="#FORM.action#">
                <cfcase value="activate">	
                	<!--- Activate Item Type(s) --->
                    <cfset itemTypeManagerObject.setItemTypeActive(FORM.selectedItemTypes,true) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.itemTypeFormProcessResult.type = "success" />
                    <cfset variables.itemTypeFormProcessResult.title = "Record(s) Activated!" />
                    <cfset variables.itemTypeFormProcessResult.detail = "Item Type Record(s) Activated Successfully" />
                </cfcase>
                <cfcase value="deactivate">
                	<!--- DeActivate Item Type(s) --->
                    <cfset itemTypeManagerObject.setItemTypeActive(FORM.selectedItemTypes,false) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.itemTypeFormProcessResult.type = "success" />
                    <cfset variables.itemTypeFormProcessResult.title = "Record(s) DeActivated!" />
                    <cfset variables.itemTypeFormProcessResult.detail = "Item Type Record(s) DeActivated Successfully" />
                </cfcase>
                <cfcase value="delete">
                	<!--- Delete Item Type(s) --->
                    <cfset itemTypeManagerObject.deleteItemType(FORM.selectedItemTypes) />
                    <!--- Set Result Struct ---> 
                    <cfset variables.itemTypeFormProcessResult.type = "success" />
                    <cfset variables.itemTypeFormProcessResult.title = "Record(s) Deleted!" />
                    <cfset variables.itemTypeFormProcessResult.detail = "Item Type Record(s) Deleted Successfully" />
                </cfcase>                        
            </cfswitch>
        </cfif>   
        
        <!--- Catch & Process Exceptions --->
        <cfcatch type="IllegalItemTypeCodeException">
			<!--- Set Result Struct ---> 
            <cfset variables.itemTypeFormProcessResult.type = "error" />
            <cfset variables.itemTypeFormProcessResult.title = cfcatch.Message />
            <cfset variables.itemTypeFormProcessResult.detail = cfcatch.Detail />
        </cfcatch>
        <cfcatch type="IllegalItemTypeDescriptionException">
			<!--- Set Result Struct ---> 
            <cfset variables.itemTypeFormProcessResult.type = "error" />
            <cfset variables.itemTypeFormProcessResult.title = cfcatch.Message />
            <cfset variables.itemTypeFormProcessResult.detail = cfcatch.Detail />
        </cfcatch>
        <cfcatch type="IllegalItemTypeFundException">
			<!--- Set Result Struct ---> 
            <cfset variables.itemTypeFormProcessResult.type = "error" />
            <cfset variables.itemTypeFormProcessResult.title = cfcatch.Message />
            <cfset variables.itemTypeFormProcessResult.detail = cfcatch.Detail />
        </cfcatch>
        <cfcatch type="IllegalItemTypeDepartmentException">
			<!--- Set Result Struct ---> 
            <cfset variables.itemTypeFormProcessResult.type = "error" />
            <cfset variables.itemTypeFormProcessResult.title = cfcatch.Message />
            <cfset variables.itemTypeFormProcessResult.detail = cfcatch.Detail />
        </cfcatch>
        <cfcatch type="IllegalItemTypeAccountException">
			<!--- Set Result Struct ---> 
            <cfset variables.itemTypeFormProcessResult.type = "error" />
            <cfset variables.itemTypeFormProcessResult.title = cfcatch.Message />
            <cfset variables.itemTypeFormProcessResult.detail = cfcatch.Detail />
        </cfcatch>
        <cfcatch type="CannotDeleteItemTypeException">
			<!--- Set Result Struct ---> 
            <cfset variables.itemTypeFormProcessResult.type = "error" />
            <cfset variables.itemTypeFormProcessResult.title = cfcatch.Message />
            <cfset variables.itemTypeFormProcessResult.detail = cfcatch.Detail />
        </cfcatch>
        <cfcatch type="DuplicateItemTypeParameterException">
			<!--- Set Result Struct ---> 
            <cfset variables.itemTypeFormProcessResult.type = "error" />
            <cfset variables.itemTypeFormProcessResult.title = cfcatch.Message />
            <cfset variables.itemTypeFormProcessResult.detail = cfcatch.Detail />
        </cfcatch>
        
		<!--- Unknown Exceptions --->
        <cfcatch type="any"> 
            <!--- Inform Web Team --->
            <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
            <!--- Set Result Struct --->
            <cfset variables.itemTypeFormProcessResult.type = "error" />
            <cfset variables.itemTypeFormProcessResult.title = "Unknown Error!" />
            <cfset variables.itemTypeFormProcessResult.detail = "Unknown error was encountered while processing your request. Web team has been notified of the issue. Please try again after sometime." />
        </cfcatch>
    </cftry>
</cfif>


<!-------- Workshop ----------

        <cfcatch type="application">   
            <cfswitch expression="#cfcatch.type#">	   
                <cfcase value="numeric">
                    <cfswitch expression="#cfcatch.arg#">	
                        <cfcase value="ITEMTYPECODE">	
                            <cfset variables.itemTypeFormMessageBanner = 
                                "<div class='error'>
                                    <h3>Item Code Should Be A Number</h3>
                                </div>" />   
                        </cfcase>
                        <cfcase value="FUND">	
                            <cfset variables.itemTypeFormMessageBanner = 
                                "<div class='error'>
                                    <h3>Fund Should Be A Number</h3>
                                </div>" />   
                        </cfcase>
                        <cfcase value="DEPARTMENT">	
                            <cfset variables.itemTypeFormMessageBanner = 
                                "<div class='error'>
                                    <h3>Department Should Be A Number</h3>
                                </div>" />   
                        </cfcase>
                        <cfcase value="ACCOUNT">	
                            <cfset variables.itemTypeFormMessageBanner = 
                                "<div class='error'>
                                    <h3>Account Should Be A Number</h3>
                                </div>" />   
                        </cfcase>
                        <cfdefaultcase>
                            <cfset session.mailerObject.mailExceptionToWebTeam(exceptionObject=cfcatch,userID='#session.name#(#session.userID#)') />   
                            <!--- Mail Details To Web Team --->
                            <cfset variables.itemTypeFormMessageBanner = 
                                "<div class='error'>
                                    Unknown error was encountered while processing your request. 
                                    Web team has been notified of the issue. Please try again after sometime.
                                </div>" />   
                        </cfdefaultcase>
                    </cfswitch>
                </cfcase>                    
            </cfswitch>                
        </cfcatch>



---------------------------->